-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: ilias
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abstraction_progress`
--

DROP TABLE IF EXISTS `abstraction_progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abstraction_progress` (
  `table_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `step` int(11) NOT NULL,
  PRIMARY KEY (`table_name`,`step`),
  KEY `t` (`table_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `acl_ws`
--

DROP TABLE IF EXISTS `acl_ws`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_ws` (
  `node_id` int(11) NOT NULL DEFAULT '0',
  `object_id` int(11) NOT NULL DEFAULT '0',
  `extended_data` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`node_id`,`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `addressbook_mlist`
--

DROP TABLE IF EXISTS `addressbook_mlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressbook_mlist` (
  `ml_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `changedate` datetime DEFAULT NULL,
  `lmode` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ml_id`),
  KEY `i1_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `addressbook_mlist_ass_seq`
--

DROP TABLE IF EXISTS `addressbook_mlist_ass_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressbook_mlist_ass_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `addressbook_mlist_seq`
--

DROP TABLE IF EXISTS `addressbook_mlist_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressbook_mlist_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `adm_settings_template_seq`
--

DROP TABLE IF EXISTS `adm_settings_template_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_settings_template_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adv_md_obj_rec_select`
--

DROP TABLE IF EXISTS `adv_md_obj_rec_select`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv_md_obj_rec_select` (
  `obj_id` int(11) NOT NULL,
  `sub_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `rec_id` int(11) NOT NULL,
  PRIMARY KEY (`obj_id`,`sub_type`,`rec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adv_md_record`
--

DROP TABLE IF EXISTS `adv_md_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv_md_record` (
  `record_id` int(11) NOT NULL DEFAULT '0',
  `import_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_obj` int(11) DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adv_md_record_objs`
--

DROP TABLE IF EXISTS `adv_md_record_objs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv_md_record_objs` (
  `record_id` int(11) NOT NULL DEFAULT '0',
  `obj_type` char(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sub_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `optional` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`record_id`,`obj_type`,`sub_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `adv_md_record_seq`
--

DROP TABLE IF EXISTS `adv_md_record_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv_md_record_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--
-- Table structure for table `adv_mdf_definition`
--

DROP TABLE IF EXISTS `adv_mdf_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv_mdf_definition` (
  `field_id` int(11) NOT NULL DEFAULT '0',
  `record_id` int(11) NOT NULL DEFAULT '0',
  `import_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `field_type` tinyint(4) NOT NULL DEFAULT '0',
  `field_values` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `searchable` tinyint(4) NOT NULL DEFAULT '0',
  `required` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adv_mdf_definition_seq`
--

DROP TABLE IF EXISTS `adv_mdf_definition_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv_mdf_definition_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--
-- Table structure for table `ass_log`
--

DROP TABLE IF EXISTS `ass_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ass_log` (
  `ass_log_id` int(11) NOT NULL DEFAULT '0',
  `user_fi` int(11) NOT NULL DEFAULT '0',
  `obj_fi` int(11) NOT NULL DEFAULT '0',
  `logtext` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_fi` int(11) DEFAULT NULL,
  `original_fi` int(11) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `test_only` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ass_log_id`),
  KEY `i1_idx` (`user_fi`,`obj_fi`),
  KEY `i2_idx` (`obj_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Logging of Test&Assessment object changes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ass_log_seq`
--

DROP TABLE IF EXISTS `ass_log_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ass_log_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2315 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--
-- Table structure for table `benchmark`
--

DROP TABLE IF EXISTS `benchmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benchmark` (
  `id` int(11) NOT NULL,
  `cdate` datetime DEFAULT NULL,
  `module` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benchmark` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` double DEFAULT NULL,
  `sql_stmt` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`module`,`benchmark`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `booking_entry`
--

DROP TABLE IF EXISTS `booking_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_entry` (
  `booking_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `deadline` int(11) NOT NULL,
  `num_bookings` int(11) NOT NULL,
  `target_obj_id` int(11) DEFAULT NULL,
  `booking_group` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`booking_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `booking_entry_seq`
--

DROP TABLE IF EXISTS `booking_entry_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_entry_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `booking_obj_assignment`
--

DROP TABLE IF EXISTS `booking_obj_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_obj_assignment` (
  `booking_id` int(11) NOT NULL,
  `target_obj_id` int(11) NOT NULL,
  PRIMARY KEY (`booking_id`,`target_obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `booking_object_seq`
--

DROP TABLE IF EXISTS `booking_object_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_object_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `booking_reservation_group_seq`
--

DROP TABLE IF EXISTS `booking_reservation_group_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_reservation_group_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `booking_reservation_seq`
--

DROP TABLE IF EXISTS `booking_reservation_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_reservation_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `booking_schedule_seq`
--

DROP TABLE IF EXISTS `booking_schedule_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_schedule_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `booking_schedule_slot`
--

DROP TABLE IF EXISTS `booking_schedule_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_schedule_slot` (
  `booking_schedule_id` int(11) NOT NULL,
  `day_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `slot_id` tinyint(4) NOT NULL,
  `times` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`booking_schedule_id`,`day_id`,`slot_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `booking_user`
--

DROP TABLE IF EXISTS `booking_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_user` (
  `entry_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tstamp` int(11) NOT NULL,
  `booking_message` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notification_sent` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookmark_data`
--

DROP TABLE IF EXISTS `bookmark_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark_data` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookmark_data_seq`
--

DROP TABLE IF EXISTS `bookmark_data_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark_data_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookmark_tree`
--

DROP TABLE IF EXISTS `bookmark_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookmark_tree` (
  `tree` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tree`,`child`),
  KEY `i1_idx` (`child`),
  KEY `i2_idx` (`parent`),
  KEY `i3_idx` (`child`,`tree`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `buddylist`
--

DROP TABLE IF EXISTS `buddylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buddylist` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `buddy_usr_id` int(11) NOT NULL DEFAULT '0',
  `ts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usr_id`,`buddy_usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `buddylist_requests`
--

DROP TABLE IF EXISTS `buddylist_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buddylist_requests` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `buddy_usr_id` int(11) NOT NULL DEFAULT '0',
  `ignored` tinyint(4) NOT NULL DEFAULT '0',
  `ts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usr_id`,`buddy_usr_id`),
  KEY `i1_idx` (`buddy_usr_id`,`ignored`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `cal_auth_token`
--

DROP TABLE IF EXISTS `cal_auth_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_auth_token` (
  `user_id` int(11) NOT NULL,
  `hash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `selection` int(11) NOT NULL,
  `calendar` int(11) NOT NULL,
  `ical` longtext COLLATE utf8_unicode_ci,
  `c_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`hash`),
  KEY `i1_idx` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_cat_assignments`
--

DROP TABLE IF EXISTS `cal_cat_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_cat_assignments` (
  `cal_id` int(11) NOT NULL DEFAULT '0',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cal_id`,`cat_id`),
  KEY `i2_idx` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_cat_visibility`
--

DROP TABLE IF EXISTS `cal_cat_visibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_cat_visibility` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`cat_id`,`obj_id`),
  KEY `i1_idx` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_categories`
--

DROP TABLE IF EXISTS `cal_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_categories` (
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `title` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `loc_type` tinyint(4) NOT NULL DEFAULT '1',
  `remote_url` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_pass` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_sync` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `i2_idx` (`obj_id`),
  KEY `i3_idx` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_categories_seq`
--

DROP TABLE IF EXISTS `cal_categories_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_categories_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=745 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_ch_group`
--

DROP TABLE IF EXISTS `cal_ch_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_ch_group` (
  `grp_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `multiple_assignments` tinyint(4) NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`grp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_ch_group_seq`
--

DROP TABLE IF EXISTS `cal_ch_group_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_ch_group_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `cal_entries`
--

DROP TABLE IF EXISTS `cal_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_entries` (
  `cal_id` int(11) NOT NULL DEFAULT '0',
  `last_update` datetime DEFAULT NULL,
  `title` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitle` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullday` tinyint(4) NOT NULL DEFAULT '0',
  `starta` datetime DEFAULT NULL,
  `enda` datetime DEFAULT NULL,
  `informations` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto_generated` tinyint(4) NOT NULL DEFAULT '0',
  `context_id` int(11) NOT NULL DEFAULT '0',
  `translation_type` tinyint(4) NOT NULL DEFAULT '0',
  `is_milestone` tinyint(4) NOT NULL DEFAULT '0',
  `completion` int(11) NOT NULL DEFAULT '0',
  `notification` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cal_id`),
  KEY `i1_idx` (`last_update`),
  KEY `i2_idx` (`context_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_entries_seq`
--

DROP TABLE IF EXISTS `cal_entries_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_entries_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3957 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `cal_notification`
--

DROP TABLE IF EXISTS `cal_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_notification` (
  `notification_id` int(11) NOT NULL,
  `cal_id` int(11) NOT NULL DEFAULT '0',
  `user_type` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`notification_id`),
  KEY `i1_idx` (`cal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_notification_seq`
--

DROP TABLE IF EXISTS `cal_notification_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_notification_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_rec_exclusion`
--

DROP TABLE IF EXISTS `cal_rec_exclusion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_rec_exclusion` (
  `excl_id` int(11) NOT NULL,
  `cal_id` int(11) NOT NULL,
  `excl_date` date DEFAULT NULL,
  PRIMARY KEY (`excl_id`),
  KEY `i1_idx` (`cal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_rec_exclusion_seq`
--

DROP TABLE IF EXISTS `cal_rec_exclusion_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_rec_exclusion_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_recurrence_rules`
--

DROP TABLE IF EXISTS `cal_recurrence_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_recurrence_rules` (
  `rule_id` int(11) NOT NULL DEFAULT '0',
  `cal_id` int(11) NOT NULL DEFAULT '0',
  `cal_recurrence` int(11) NOT NULL DEFAULT '0',
  `freq_type` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `freq_until_date` datetime DEFAULT NULL,
  `freq_until_count` int(11) NOT NULL DEFAULT '0',
  `intervall` int(11) NOT NULL DEFAULT '0',
  `byday` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `byweekno` char(64) COLLATE utf8_unicode_ci DEFAULT '0',
  `bymonth` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bymonthday` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `byyearday` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bysetpos` char(64) COLLATE utf8_unicode_ci DEFAULT '0',
  `weekstart` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`rule_id`),
  KEY `i1_idx` (`cal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cal_recurrence_rules_seq`
--

DROP TABLE IF EXISTS `cal_recurrence_rules_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cal_recurrence_rules_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `catch_write_events`
--

DROP TABLE IF EXISTS `catch_write_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catch_write_events` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `ts` datetime DEFAULT NULL,
  PRIMARY KEY (`obj_id`,`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatroom_admconfig`
--

DROP TABLE IF EXISTS `chatroom_admconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_admconfig` (
  `instance_id` int(11) NOT NULL,
  `server_settings` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `default_config` tinyint(4) NOT NULL DEFAULT '0',
  `client_settings` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`instance_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `chatroom_history`
--

DROP TABLE IF EXISTS `chatroom_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_history` (
  `hist_id` bigint(20) NOT NULL DEFAULT '0',
  `room_id` int(11) NOT NULL DEFAULT '0',
  `message` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `sub_room` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`hist_id`),
  KEY `i1_idx` (`room_id`,`sub_room`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `chatroom_proomaccess`
--

DROP TABLE IF EXISTS `chatroom_proomaccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_proomaccess` (
  `proom_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`proom_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatroom_prooms`
--

DROP TABLE IF EXISTS `chatroom_prooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_prooms` (
  `proom_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `owner` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `closed` int(11) DEFAULT '0',
  `is_public` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`proom_id`),
  KEY `i1_idx` (`parent_id`),
  KEY `i2_idx` (`owner`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatroom_prooms_seq`
--

DROP TABLE IF EXISTS `chatroom_prooms_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_prooms_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatroom_psessions`
--

DROP TABLE IF EXISTS `chatroom_psessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_psessions` (
  `psess_id` bigint(20) NOT NULL DEFAULT '0',
  `proom_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `connected` int(11) NOT NULL DEFAULT '0',
  `disconnected` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`psess_id`),
  KEY `i1_idx` (`proom_id`,`user_id`),
  KEY `i2_idx` (`disconnected`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `chatroom_sessions`
--

DROP TABLE IF EXISTS `chatroom_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_sessions` (
  `sess_id` bigint(20) NOT NULL DEFAULT '0',
  `room_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `userdata` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connected` int(11) NOT NULL DEFAULT '0',
  `disconnected` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sess_id`),
  KEY `i1_idx` (`room_id`,`user_id`),
  KEY `i2_idx` (`disconnected`),
  KEY `i3_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `chatroom_settings`
--

DROP TABLE IF EXISTS `chatroom_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_settings` (
  `room_id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT '0',
  `room_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `allow_anonymous` tinyint(4) DEFAULT '0',
  `allow_custom_usernames` tinyint(4) DEFAULT '0',
  `enable_history` tinyint(4) DEFAULT '0',
  `restrict_history` tinyint(4) DEFAULT '0',
  `autogen_usernames` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Anonymous #',
  `allow_private_rooms` tinyint(4) DEFAULT '0',
  `display_past_msgs` int(11) NOT NULL DEFAULT '0',
  `private_rooms_enabled` int(11) NOT NULL DEFAULT '0',
  `online_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`room_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chatroom_settings_seq`
--

DROP TABLE IF EXISTS `chatroom_settings_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chatroom_settings_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--


--


--


--


--


--


--


--


--
-- Table structure for table `conditions`
--

DROP TABLE IF EXISTS `conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conditions` (
  `condition_id` int(11) NOT NULL DEFAULT '0',
  `target_ref_id` int(11) NOT NULL DEFAULT '0',
  `target_obj_id` int(11) NOT NULL DEFAULT '0',
  `target_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trigger_ref_id` int(11) NOT NULL DEFAULT '0',
  `trigger_obj_id` int(11) NOT NULL DEFAULT '0',
  `trigger_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operator` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_handling` tinyint(4) NOT NULL DEFAULT '1',
  `obligatory` tinyint(4) NOT NULL DEFAULT '1',
  `num_obligatory` tinyint(4) NOT NULL DEFAULT '0',
  `hidden_status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`condition_id`),
  KEY `tot_idx` (`target_obj_id`,`target_type`),
  KEY `i1_idx` (`target_obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conditions_seq`
--

DROP TABLE IF EXISTS `conditions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conditions_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=374 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `container_reference`
--

DROP TABLE IF EXISTS `container_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_reference` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `target_obj_id` int(11) NOT NULL DEFAULT '0',
  `title_type` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`obj_id`,`target_obj_id`),
  KEY `i1_idx` (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_settings`
--

DROP TABLE IF EXISTS `container_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_settings` (
  `id` int(11) NOT NULL DEFAULT '0',
  `keyword` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_sorting`
--

DROP TABLE IF EXISTS `container_sorting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_sorting` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `child_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `parent_type` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`,`child_id`,`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_sorting_bl`
--

DROP TABLE IF EXISTS `container_sorting_bl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_sorting_bl` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `block_ids` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_sorting_set`
--

DROP TABLE IF EXISTS `container_sorting_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_sorting_set` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `sort_mode` tinyint(4) NOT NULL DEFAULT '0',
  `sort_direction` tinyint(4) NOT NULL DEFAULT '0',
  `new_items_position` tinyint(4) NOT NULL DEFAULT '1',
  `new_items_order` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_object`
--

DROP TABLE IF EXISTS `content_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_object` (
  `id` int(11) NOT NULL DEFAULT '0',
  `default_layout` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'toc2win',
  `stylesheet` int(11) NOT NULL DEFAULT '0',
  `page_header` char(8) COLLATE utf8_unicode_ci DEFAULT 'st_title',
  `is_online` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `toc_active` char(1) COLLATE utf8_unicode_ci DEFAULT 'y',
  `lm_menu_active` char(1) COLLATE utf8_unicode_ci DEFAULT 'y',
  `toc_mode` char(8) COLLATE utf8_unicode_ci DEFAULT 'chapters',
  `clean_frames` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `print_view_active` char(1) COLLATE utf8_unicode_ci DEFAULT 'y',
  `numbering` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `hist_user_comments` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `public_access_mode` char(8) COLLATE utf8_unicode_ci DEFAULT 'complete',
  `public_html_file` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_xml_file` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `downloads_active` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `downloads_public_active` char(1) COLLATE utf8_unicode_ci DEFAULT 'y',
  `header_page` int(11) NOT NULL DEFAULT '0',
  `footer_page` int(11) NOT NULL DEFAULT '0',
  `no_glo_appendix` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `layout_per_page` tinyint(4) DEFAULT NULL,
  `public_scorm_file` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` tinyint(4) NOT NULL DEFAULT '0',
  `hide_head_foot_print` tinyint(4) NOT NULL DEFAULT '0',
  `disable_def_feedback` int(11) NOT NULL DEFAULT '0',
  `rating_pages` tinyint(4) DEFAULT '0',
  `progr_icons` tinyint(4) NOT NULL DEFAULT '0',
  `store_tries` tinyint(4) NOT NULL DEFAULT '0',
  `restrict_forw_nav` tinyint(4) NOT NULL DEFAULT '0',
  `for_translation` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `copg_pc_def`
--

DROP TABLE IF EXISTS `copg_pc_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copg_pc_def` (
  `pc_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `directory` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `int_links` tinyint(4) NOT NULL DEFAULT '0',
  `style_classes` tinyint(4) NOT NULL DEFAULT '0',
  `xsl` tinyint(4) NOT NULL DEFAULT '0',
  `component` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `def_enabled` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`pc_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `copg_pobj_def`
--

DROP TABLE IF EXISTS `copg_pobj_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copg_pobj_def` (
  `parent_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `class_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `directory` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `component` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`parent_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `copy_wizard_options`
--

DROP TABLE IF EXISTS `copy_wizard_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copy_wizard_options` (
  `copy_id` int(11) NOT NULL DEFAULT '0',
  `source_id` int(11) NOT NULL DEFAULT '0',
  `options` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`copy_id`,`source_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `cp_file`
--

DROP TABLE IF EXISTS `cp_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_file` (
  `cp_node_id` int(11) NOT NULL DEFAULT '0',
  `href` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cp_node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `cp_item`
--

DROP TABLE IF EXISTS `cp_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_item` (
  `completionthreshold` varchar(50) COLLATE utf8_unicode_ci DEFAULT '1.0',
  `cp_node_id` int(11) NOT NULL DEFAULT '0',
  `datafromlms` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isvisible` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resourceid` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequencingid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timelimitaction` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `progressweight` varchar(50) COLLATE utf8_unicode_ci DEFAULT '1.0',
  `completedbymeasure` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`cp_node_id`),
  KEY `i1_idx` (`id`),
  KEY `i2_idx` (`sequencingid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cp_manifest`
--

DROP TABLE IF EXISTS `cp_manifest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_manifest` (
  `base` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_node_id` int(11) NOT NULL DEFAULT '0',
  `defaultorganization` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cp_node_id`),
  KEY `i1_idx` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `cp_node`
--

DROP TABLE IF EXISTS `cp_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_node` (
  `cp_node_id` int(11) NOT NULL DEFAULT '0',
  `nodename` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slm_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cp_node_id`),
  KEY `i2_idx` (`nodename`),
  KEY `i3_idx` (`slm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cp_node_seq`
--

DROP TABLE IF EXISTS `cp_node_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_node_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `cp_organization`
--

DROP TABLE IF EXISTS `cp_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_organization` (
  `cp_node_id` int(11) NOT NULL DEFAULT '0',
  `id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `objectivesglobtosys` tinyint(4) DEFAULT NULL,
  `sequencingid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `structure` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cp_node_id`),
  KEY `i1_idx` (`id`),
  KEY `i2_idx` (`sequencingid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cp_package`
--

DROP TABLE IF EXISTS `cp_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_package` (
  `created` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jsdata` longtext COLLATE utf8_unicode_ci,
  `modified` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `persistprevattempts` int(11) DEFAULT NULL,
  `c_settings` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xmldata` longtext COLLATE utf8_unicode_ci,
  `activitytree` longtext COLLATE utf8_unicode_ci,
  `global_to_system` tinyint(4) NOT NULL DEFAULT '1',
  `shared_data_global_to_system` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`obj_id`),
  KEY `i1_idx` (`c_identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cp_resource`
--

DROP TABLE IF EXISTS `cp_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_resource` (
  `base` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_node_id` int(11) NOT NULL DEFAULT '0',
  `href` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scormtype` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cp_node_id`),
  KEY `i1_idx` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `cp_sequencing`
--

DROP TABLE IF EXISTS `cp_sequencing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_sequencing` (
  `activityabsdurlimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activityexpdurlimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attemptabsdurlimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attemptexpdurlimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attemptlimit` int(11) DEFAULT NULL,
  `begintimelimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `choice` tinyint(4) DEFAULT NULL,
  `choiceexit` tinyint(4) DEFAULT NULL,
  `completionbycontent` tinyint(4) DEFAULT NULL,
  `constrainchoice` tinyint(4) DEFAULT NULL,
  `cp_node_id` int(11) NOT NULL DEFAULT '0',
  `endtimelimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flow` tinyint(4) DEFAULT NULL,
  `forwardonly` tinyint(4) DEFAULT NULL,
  `id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `measuresatisfactive` tinyint(4) DEFAULT NULL,
  `objectivemeasweight` double DEFAULT NULL,
  `objectivebycontent` tinyint(4) DEFAULT NULL,
  `preventactivation` tinyint(4) DEFAULT NULL,
  `randomizationtiming` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reorderchildren` tinyint(4) DEFAULT NULL,
  `requiredcompleted` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiredincomplete` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requirednotsatisfied` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiredforsatisfied` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rollupobjectivesatis` tinyint(4) DEFAULT NULL,
  `rollupprogcompletion` tinyint(4) DEFAULT NULL,
  `selectcount` int(11) DEFAULT NULL,
  `selectiontiming` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequencingid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracked` tinyint(4) DEFAULT NULL,
  `usecurattemptobjinfo` tinyint(4) DEFAULT NULL,
  `usecurattemptproginfo` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`cp_node_id`),
  KEY `i1_idx` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `cp_tree`
--

DROP TABLE IF EXISTS `cp_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_tree` (
  `child` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  PRIMARY KEY (`obj_id`,`child`),
  KEY `i1_idx` (`child`),
  KEY `i2_idx` (`obj_id`),
  KEY `i3_idx` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cron_job`
--

DROP TABLE IF EXISTS `cron_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cron_job` (
  `job_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `component` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schedule_type` tinyint(4) DEFAULT NULL,
  `schedule_value` int(11) DEFAULT NULL,
  `job_status` tinyint(4) DEFAULT NULL,
  `job_status_user_id` int(11) DEFAULT NULL,
  `job_status_type` tinyint(4) DEFAULT NULL,
  `job_status_ts` int(11) DEFAULT NULL,
  `job_result_status` tinyint(4) DEFAULT NULL,
  `job_result_user_id` int(11) DEFAULT NULL,
  `job_result_code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_result_message` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_result_type` tinyint(4) DEFAULT NULL,
  `job_result_ts` int(11) DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `running_ts` int(11) DEFAULT NULL,
  `job_result_dur` int(11) DEFAULT NULL,
  `alive_ts` int(11) DEFAULT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `crs_f_definitions`
--

DROP TABLE IF EXISTS `crs_f_definitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_f_definitions` (
  `field_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_type` tinyint(4) NOT NULL DEFAULT '0',
  `field_values` longtext COLLATE utf8_unicode_ci,
  `field_required` tinyint(4) NOT NULL DEFAULT '0',
  `field_values_opt` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_f_definitions_seq`
--

DROP TABLE IF EXISTS `crs_f_definitions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_f_definitions_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `crs_groupings`
--

DROP TABLE IF EXISTS `crs_groupings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_groupings` (
  `crs_grp_id` int(11) NOT NULL DEFAULT '0',
  `crs_ref_id` int(11) NOT NULL DEFAULT '0',
  `crs_id` int(11) NOT NULL DEFAULT '0',
  `unique_field` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`crs_grp_id`),
  KEY `i1_idx` (`crs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_items`
--

DROP TABLE IF EXISTS `crs_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_items` (
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `timing_type` tinyint(4) DEFAULT NULL,
  `timing_start` int(11) NOT NULL DEFAULT '0',
  `timing_end` int(11) NOT NULL DEFAULT '0',
  `suggestion_start` int(11) NOT NULL DEFAULT '0',
  `suggestion_end` int(11) NOT NULL DEFAULT '0',
  `changeable` tinyint(4) NOT NULL DEFAULT '0',
  `earliest_start` int(11) NOT NULL DEFAULT '0',
  `latest_end` int(11) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '0',
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`parent_id`,`obj_id`),
  KEY `ob_idx` (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_lm_history`
--

DROP TABLE IF EXISTS `crs_lm_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_lm_history` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `crs_ref_id` int(11) NOT NULL DEFAULT '0',
  `lm_ref_id` int(11) NOT NULL DEFAULT '0',
  `lm_page_id` int(11) NOT NULL DEFAULT '0',
  `last_access` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usr_id`,`crs_ref_id`,`lm_ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objective_lm`
--

DROP TABLE IF EXISTS `crs_objective_lm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objective_lm` (
  `lm_ass_id` int(11) NOT NULL DEFAULT '0',
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `ref_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `type` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`lm_ass_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objective_lm_seq`
--

DROP TABLE IF EXISTS `crs_objective_lm_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objective_lm_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2592 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objective_qst`
--

DROP TABLE IF EXISTS `crs_objective_qst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objective_qst` (
  `qst_ass_id` int(11) NOT NULL DEFAULT '0',
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `ref_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`qst_ass_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objective_qst_seq`
--

DROP TABLE IF EXISTS `crs_objective_qst_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objective_qst_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2027 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objective_status`
--

DROP TABLE IF EXISTS `crs_objective_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objective_status` (
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`objective_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objective_status_p`
--

DROP TABLE IF EXISTS `crs_objective_status_p`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objective_status_p` (
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`objective_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objective_tst`
--

DROP TABLE IF EXISTS `crs_objective_tst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objective_tst` (
  `test_objective_id` int(11) NOT NULL DEFAULT '0',
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `ref_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `tst_status` tinyint(4) DEFAULT NULL,
  `tst_limit` tinyint(4) DEFAULT NULL,
  `tst_limit_p` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`test_objective_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objective_tst_seq`
--

DROP TABLE IF EXISTS `crs_objective_tst_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objective_tst_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=464 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objectives`
--

DROP TABLE IF EXISTS `crs_objectives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objectives` (
  `crs_id` int(11) NOT NULL DEFAULT '0',
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `passes` smallint(6) DEFAULT '0',
  PRIMARY KEY (`objective_id`),
  KEY `i1_idx` (`crs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_objectives_seq`
--

DROP TABLE IF EXISTS `crs_objectives_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_objectives_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=865 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_settings`
--

DROP TABLE IF EXISTS `crs_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_settings` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `syllabus` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_responsibility` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_consultation` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_type` tinyint(4) NOT NULL DEFAULT '0',
  `activation_start` int(11) DEFAULT NULL,
  `activation_end` int(11) DEFAULT NULL,
  `sub_limitation_type` tinyint(4) NOT NULL DEFAULT '0',
  `sub_start` int(11) DEFAULT NULL,
  `sub_end` int(11) DEFAULT NULL,
  `sub_type` int(11) DEFAULT NULL,
  `sub_password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_mem_limit` tinyint(4) NOT NULL DEFAULT '0',
  `sub_max_members` int(11) DEFAULT NULL,
  `sub_notify` int(11) DEFAULT NULL,
  `view_mode` tinyint(4) NOT NULL DEFAULT '0',
  `sortorder` int(11) DEFAULT NULL,
  `archive_start` int(11) DEFAULT NULL,
  `archive_end` int(11) DEFAULT NULL,
  `archive_type` int(11) DEFAULT NULL,
  `abo` tinyint(4) DEFAULT '1',
  `waiting_list` tinyint(4) NOT NULL DEFAULT '1',
  `important` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_members` tinyint(4) NOT NULL DEFAULT '1',
  `latitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_zoom` int(11) NOT NULL DEFAULT '0',
  `enable_course_map` tinyint(4) NOT NULL DEFAULT '0',
  `session_limit` tinyint(4) NOT NULL DEFAULT '0',
  `session_prev` bigint(20) NOT NULL DEFAULT '-1',
  `session_next` bigint(20) NOT NULL DEFAULT '-1',
  `reg_ac_enabled` tinyint(4) NOT NULL,
  `reg_ac` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_dt` tinyint(4) DEFAULT '2',
  `auto_notification` tinyint(4) NOT NULL DEFAULT '1',
  `mail_members_type` tinyint(4) DEFAULT '1',
  `crs_start` int(11) DEFAULT NULL,
  `crs_end` int(11) DEFAULT NULL,
  `leave_end` int(11) DEFAULT NULL,
  `auto_wait` tinyint(4) NOT NULL DEFAULT '0',
  `min_members` smallint(6) DEFAULT NULL,
  `show_members_export` int(11) DEFAULT NULL,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_start`
--

DROP TABLE IF EXISTS `crs_start`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_start` (
  `crs_start_id` int(11) NOT NULL DEFAULT '0',
  `crs_id` int(11) NOT NULL DEFAULT '0',
  `item_ref_id` int(11) NOT NULL DEFAULT '0',
  `pos` int(11) DEFAULT NULL,
  PRIMARY KEY (`crs_start_id`),
  KEY `i1_idx` (`crs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crs_start_seq`
--

DROP TABLE IF EXISTS `crs_start_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_start_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `crs_user_data`
--

DROP TABLE IF EXISTS `crs_user_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crs_user_data` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `field_id` int(11) NOT NULL DEFAULT '0',
  `value` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usr_id`,`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `ctrl_calls`
--

DROP TABLE IF EXISTS `ctrl_calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctrl_calls` (
  `parent` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `child` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comp_prefix` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`parent`,`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ctrl_classfile`
--

DROP TABLE IF EXISTS `ctrl_classfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctrl_classfile` (
  `class` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `filename` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comp_prefix` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugin_path` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cid` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`class`),
  KEY `i1_idx` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ctrl_structure`
--

DROP TABLE IF EXISTS `ctrl_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctrl_structure` (
  `root_class` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `call_node` longtext COLLATE utf8_unicode_ci,
  `forward` longtext COLLATE utf8_unicode_ci,
  `parent` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`root_class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `desktop_item`
--

DROP TABLE IF EXISTS `desktop_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desktop_item` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameters` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didactic_tpl_a`
--

DROP TABLE IF EXISTS `didactic_tpl_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_a` (
  `id` int(11) NOT NULL,
  `tpl_id` int(11) NOT NULL,
  `type_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didactic_tpl_a_seq`
--

DROP TABLE IF EXISTS `didactic_tpl_a_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_a_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `didactic_tpl_alp`
--

DROP TABLE IF EXISTS `didactic_tpl_alp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_alp` (
  `action_id` int(11) NOT NULL,
  `filter_type` tinyint(4) NOT NULL,
  `template_type` tinyint(4) NOT NULL,
  `template_id` int(11) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `didactic_tpl_fp`
--

DROP TABLE IF EXISTS `didactic_tpl_fp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_fp` (
  `pattern_id` int(11) NOT NULL,
  `pattern_type` tinyint(4) NOT NULL,
  `pattern_sub_type` tinyint(4) NOT NULL,
  `pattern` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pattern_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didactic_tpl_fp_seq`
--

DROP TABLE IF EXISTS `didactic_tpl_fp_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_fp_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didactic_tpl_objs`
--

DROP TABLE IF EXISTS `didactic_tpl_objs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_objs` (
  `obj_id` int(11) NOT NULL,
  `tpl_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ref_id`,`tpl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didactic_tpl_sa`
--

DROP TABLE IF EXISTS `didactic_tpl_sa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_sa` (
  `id` int(11) NOT NULL,
  `obj_type` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`obj_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didactic_tpl_settings`
--

DROP TABLE IF EXISTS `didactic_tpl_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_settings` (
  `id` int(11) NOT NULL,
  `enabled` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto_generated` tinyint(4) NOT NULL DEFAULT '0',
  `exclusive_tpl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `didactic_tpl_settings_seq`
--

DROP TABLE IF EXISTS `didactic_tpl_settings_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `didactic_tpl_settings_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--
-- Table structure for table `ecs_community`
--

DROP TABLE IF EXISTS `ecs_community`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_community` (
  `sid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `own_id` int(11) NOT NULL,
  `cname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mids` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sid`,`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ecs_container_mapping`
--

DROP TABLE IF EXISTS `ecs_container_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_container_mapping` (
  `mapping_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mapping_type` tinyint(4) NOT NULL DEFAULT '0',
  `mapping_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_range_start` int(11) NOT NULL DEFAULT '0',
  `date_range_end` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mapping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ecs_container_mapping_seq`
--

DROP TABLE IF EXISTS `ecs_container_mapping_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_container_mapping_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--
-- Table structure for table `ecs_events`
--

DROP TABLE IF EXISTS `ecs_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_events` (
  `event_id` int(11) NOT NULL DEFAULT '0',
  `type` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  `op` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `server_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ecs_events_seq`
--

DROP TABLE IF EXISTS `ecs_events_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_events_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `ecs_import`
--

DROP TABLE IF EXISTS `ecs_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_import` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `mid` int(11) NOT NULL DEFAULT '0',
  `server_id` int(11) NOT NULL DEFAULT '0',
  `sub_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecs_id` int(11) DEFAULT '0',
  `content_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `econtent_id` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`server_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `ecs_part_settings`
--

DROP TABLE IF EXISTS `ecs_part_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_part_settings` (
  `sid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `export` tinyint(4) NOT NULL,
  `import` tinyint(4) NOT NULL,
  `import_type` tinyint(4) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` tinyint(4) DEFAULT '1',
  `export_types` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `import_types` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtoken` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sid`,`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `ecs_server`
--

DROP TABLE IF EXISTS `ecs_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_server` (
  `server_id` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) DEFAULT '0',
  `protocol` tinyint(4) DEFAULT '1',
  `server` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` smallint(6) DEFAULT '1',
  `auth_type` tinyint(4) DEFAULT '1',
  `client_cert_path` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ca_cert_path` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key_path` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key_password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cert_serial` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `polling_time` int(11) DEFAULT '0',
  `import_id` int(11) DEFAULT '0',
  `global_role` int(11) DEFAULT '0',
  `econtent_rcp` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_rcp` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approval_rcp` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` int(11) DEFAULT '0',
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_user` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_pass` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`server_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ecs_server_seq`
--

DROP TABLE IF EXISTS `ecs_server_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecs_server_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `event_appointment_seq`
--

DROP TABLE IF EXISTS `event_appointment_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_appointment_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `event_seq`
--

DROP TABLE IF EXISTS `event_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_ass_file_order`
--

DROP TABLE IF EXISTS `exc_ass_file_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_ass_file_order` (
  `id` int(11) NOT NULL DEFAULT '0',
  `assignment_id` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(150) NOT NULL,
  `order_nr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_ass_file_order_seq`
--

DROP TABLE IF EXISTS `exc_ass_file_order_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_ass_file_order_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_assignment`
--

DROP TABLE IF EXISTS `exc_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_assignment` (
  `id` int(11) NOT NULL,
  `exc_id` int(11) NOT NULL,
  `time_stamp` int(11) DEFAULT NULL,
  `instruction` longtext COLLATE utf8_unicode_ci,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time` int(11) DEFAULT NULL,
  `mandatory` tinyint(4) DEFAULT '1',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `peer` tinyint(4) NOT NULL DEFAULT '0',
  `peer_min` smallint(6) NOT NULL DEFAULT '0',
  `fb_file` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_cron` tinyint(4) NOT NULL DEFAULT '0',
  `fb_cron_done` tinyint(4) NOT NULL DEFAULT '0',
  `peer_dl` int(11) DEFAULT '0',
  `peer_file` tinyint(4) DEFAULT '0',
  `peer_prsl` tinyint(4) DEFAULT '0',
  `fb_date` tinyint(4) NOT NULL DEFAULT '1',
  `peer_char` smallint(6) DEFAULT NULL,
  `peer_unlock` tinyint(4) NOT NULL DEFAULT '0',
  `peer_valid` tinyint(4) NOT NULL DEFAULT '1',
  `team_tutor` tinyint(4) NOT NULL DEFAULT '0',
  `max_file` tinyint(4) DEFAULT NULL,
  `deadline2` int(11) DEFAULT NULL,
  `peer_text` tinyint(4) NOT NULL DEFAULT '1',
  `peer_rating` tinyint(4) NOT NULL DEFAULT '1',
  `peer_crit_cat` int(11) DEFAULT NULL,
  `portfolio_template` int(11) DEFAULT NULL,
  `min_char_limit` int(11) DEFAULT NULL,
  `max_char_limit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `exc_assignment_seq`
--

DROP TABLE IF EXISTS `exc_assignment_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_assignment_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=293 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `exc_data`
--

DROP TABLE IF EXISTS `exc_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_data` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `instruction` longtext COLLATE utf8_unicode_ci,
  `time_stamp` int(11) DEFAULT NULL,
  `pass_mode` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'all',
  `pass_nr` int(11) DEFAULT NULL,
  `show_submissions` tinyint(4) NOT NULL DEFAULT '0',
  `compl_by_submission` tinyint(4) NOT NULL DEFAULT '0',
  `certificate_visibility` tinyint(4) NOT NULL DEFAULT '0',
  `tfeedback` tinyint(4) NOT NULL DEFAULT '7',
  PRIMARY KEY (`obj_id`),
  FULLTEXT KEY `i1_idx` (`instruction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_idl`
--

DROP TABLE IF EXISTS `exc_idl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_idl` (
  `ass_id` int(11) NOT NULL DEFAULT '0',
  `member_id` int(11) NOT NULL DEFAULT '0',
  `is_team` tinyint(4) NOT NULL DEFAULT '0',
  `tstamp` int(11) DEFAULT '0',
  PRIMARY KEY (`ass_id`,`member_id`,`is_team`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_mem_ass_status`
--

DROP TABLE IF EXISTS `exc_mem_ass_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_mem_ass_status` (
  `ass_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `notice` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `returned` tinyint(4) NOT NULL DEFAULT '0',
  `solved` tinyint(4) DEFAULT NULL,
  `status_time` datetime DEFAULT NULL,
  `sent` tinyint(4) DEFAULT NULL,
  `sent_time` datetime DEFAULT NULL,
  `feedback_time` datetime DEFAULT NULL,
  `feedback` tinyint(4) NOT NULL DEFAULT '0',
  `status` char(9) COLLATE utf8_unicode_ci DEFAULT 'notgraded',
  `mark` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_comment` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ass_id`,`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_members`
--

DROP TABLE IF EXISTS `exc_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_members` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `notice` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `returned` tinyint(4) NOT NULL DEFAULT '0',
  `solved` tinyint(4) DEFAULT NULL,
  `status_time` datetime DEFAULT NULL,
  `sent` tinyint(4) DEFAULT NULL,
  `sent_time` datetime DEFAULT NULL,
  `feedback_time` datetime DEFAULT NULL,
  `feedback` tinyint(4) NOT NULL DEFAULT '0',
  `status` char(9) COLLATE utf8_unicode_ci DEFAULT 'notgraded',
  PRIMARY KEY (`obj_id`,`usr_id`),
  KEY `ob_idx` (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_returned`
--

DROP TABLE IF EXISTS `exc_returned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_returned` (
  `returned_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filetitle` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mimetype` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `ass_id` int(11) DEFAULT NULL,
  `atext` longtext COLLATE utf8_unicode_ci,
  `late` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`returned_id`),
  KEY `i1_idx` (`obj_id`),
  KEY `i2_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_returned_seq`
--

DROP TABLE IF EXISTS `exc_returned_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_returned_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=614 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exc_usr_tutor`
--

DROP TABLE IF EXISTS `exc_usr_tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exc_usr_tutor` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `tutor_id` int(11) NOT NULL DEFAULT '0',
  `download_time` datetime DEFAULT NULL,
  `ass_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ass_id`,`usr_id`,`tutor_id`),
  KEY `ob_idx` (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `export_file_info`
--

DROP TABLE IF EXISTS `export_file_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_file_info` (
  `obj_id` int(11) NOT NULL,
  `export_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `version` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `filename` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`obj_id`,`export_type`,`filename`),
  KEY `i1_idx` (`create_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `export_options`
--

DROP TABLE IF EXISTS `export_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_options` (
  `export_id` smallint(6) NOT NULL,
  `keyword` smallint(6) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `value` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`export_id`,`keyword`,`ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `file_data`
--

DROP TABLE IF EXISTS `file_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_data` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `file_name` char(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_type` char(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_size` int(11) NOT NULL DEFAULT '0',
  `version` int(11) DEFAULT NULL,
  `f_mode` char(8) COLLATE utf8_unicode_ci DEFAULT 'object',
  `rating` tinyint(4) NOT NULL DEFAULT '0',
  `page_count` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `file_usage`
--

DROP TABLE IF EXISTS `file_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_usage` (
  `id` int(11) NOT NULL DEFAULT '0',
  `usage_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `usage_id` int(11) NOT NULL DEFAULT '0',
  `usage_hist_nr` int(11) NOT NULL DEFAULT '0',
  `usage_lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`id`,`usage_type`,`usage_id`,`usage_hist_nr`,`usage_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_data`
--

DROP TABLE IF EXISTS `frm_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_data` (
  `top_pk` bigint(20) NOT NULL DEFAULT '0',
  `top_frm_fk` bigint(20) NOT NULL DEFAULT '0',
  `top_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top_num_posts` int(11) NOT NULL DEFAULT '0',
  `top_num_threads` int(11) NOT NULL DEFAULT '0',
  `top_last_post` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top_mods` int(11) NOT NULL DEFAULT '0',
  `top_date` datetime DEFAULT NULL,
  `visits` int(11) NOT NULL DEFAULT '0',
  `top_update` datetime DEFAULT NULL,
  `update_user` int(11) NOT NULL DEFAULT '0',
  `top_usr_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`top_pk`),
  KEY `i1_idx` (`top_frm_fk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_data_seq`
--

DROP TABLE IF EXISTS `frm_data_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_data_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=221 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `frm_notification`
--

DROP TABLE IF EXISTS `frm_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_notification` (
  `notification_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `frm_id` int(11) NOT NULL DEFAULT '0',
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `admin_force_noti` tinyint(4) NOT NULL DEFAULT '0',
  `user_toggle_noti` tinyint(4) NOT NULL DEFAULT '0',
  `user_id_noti` int(11) DEFAULT NULL,
  PRIMARY KEY (`notification_id`),
  KEY `i1_idx` (`user_id`,`thread_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_notification_seq`
--

DROP TABLE IF EXISTS `frm_notification_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_notification_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5684 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_posts`
--

DROP TABLE IF EXISTS `frm_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_posts` (
  `pos_pk` bigint(20) NOT NULL DEFAULT '0',
  `pos_top_fk` bigint(20) NOT NULL DEFAULT '0',
  `pos_thr_fk` bigint(20) NOT NULL DEFAULT '0',
  `pos_usr_alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_subject` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_date` datetime DEFAULT NULL,
  `pos_update` datetime DEFAULT NULL,
  `update_user` int(11) NOT NULL DEFAULT '0',
  `pos_cens` tinyint(4) NOT NULL DEFAULT '0',
  `pos_cens_com` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notify` tinyint(4) NOT NULL DEFAULT '0',
  `import_name` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_status` tinyint(4) NOT NULL DEFAULT '1',
  `pos_message` longtext COLLATE utf8_unicode_ci,
  `pos_author_id` int(11) NOT NULL DEFAULT '0',
  `pos_display_user_id` int(11) NOT NULL DEFAULT '0',
  `is_author_moderator` tinyint(4) DEFAULT NULL,
  `pos_cens_date` datetime DEFAULT NULL,
  `pos_activation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`pos_pk`),
  KEY `i1_idx` (`pos_thr_fk`),
  KEY `i2_idx` (`pos_top_fk`),
  KEY `i3_idx` (`pos_date`),
  FULLTEXT KEY `i4_idx` (`pos_subject`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `frm_posts_seq`
--

DROP TABLE IF EXISTS `frm_posts_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_posts_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1527 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_posts_tree`
--

DROP TABLE IF EXISTS `frm_posts_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_posts_tree` (
  `fpt_pk` bigint(20) NOT NULL DEFAULT '0',
  `thr_fk` bigint(20) NOT NULL DEFAULT '0',
  `pos_fk` bigint(20) NOT NULL DEFAULT '0',
  `parent_pos` bigint(20) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '0',
  `fpt_date` datetime DEFAULT NULL,
  PRIMARY KEY (`fpt_pk`),
  KEY `i1_idx` (`thr_fk`),
  KEY `i2_idx` (`pos_fk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_posts_tree_seq`
--

DROP TABLE IF EXISTS `frm_posts_tree_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_posts_tree_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1527 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_settings`
--

DROP TABLE IF EXISTS `frm_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_settings` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `default_view` int(11) NOT NULL DEFAULT '0',
  `anonymized` tinyint(4) NOT NULL DEFAULT '0',
  `statistics_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `post_activation` tinyint(4) NOT NULL DEFAULT '0',
  `admin_force_noti` tinyint(4) NOT NULL DEFAULT '0',
  `user_toggle_noti` tinyint(4) NOT NULL DEFAULT '0',
  `preset_subject` tinyint(4) NOT NULL DEFAULT '1',
  `notification_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_re_subject` tinyint(4) NOT NULL DEFAULT '0',
  `mark_mod_posts` tinyint(4) NOT NULL DEFAULT '0',
  `thread_sorting` int(11) NOT NULL DEFAULT '0',
  `thread_rating` tinyint(4) NOT NULL DEFAULT '0',
  `file_upload_allowed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_thread_access`
--

DROP TABLE IF EXISTS `frm_thread_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_thread_access` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `access_old` int(11) NOT NULL DEFAULT '0',
  `access_last` int(11) NOT NULL DEFAULT '0',
  `access_old_ts` datetime DEFAULT NULL,
  PRIMARY KEY (`usr_id`,`obj_id`,`thread_id`),
  KEY `i1_idx` (`access_last`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_threads`
--

DROP TABLE IF EXISTS `frm_threads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_threads` (
  `thr_pk` bigint(20) NOT NULL DEFAULT '0',
  `thr_top_fk` bigint(20) NOT NULL DEFAULT '0',
  `thr_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thr_usr_alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thr_num_posts` int(11) NOT NULL DEFAULT '0',
  `thr_last_post` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thr_date` datetime DEFAULT NULL,
  `thr_update` datetime DEFAULT NULL,
  `visits` int(11) NOT NULL DEFAULT '0',
  `import_name` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_sticky` tinyint(4) NOT NULL DEFAULT '0',
  `is_closed` tinyint(4) NOT NULL DEFAULT '0',
  `thread_sorting` int(11) NOT NULL DEFAULT '0',
  `avg_rating` double NOT NULL DEFAULT '0',
  `thr_author_id` int(11) NOT NULL DEFAULT '0',
  `thr_display_user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`thr_pk`),
  KEY `i2_idx` (`thr_top_fk`),
  FULLTEXT KEY `i1_idx` (`thr_subject`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_threads_seq`
--

DROP TABLE IF EXISTS `frm_threads_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_threads_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=832 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frm_user_read`
--

DROP TABLE IF EXISTS `frm_user_read`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_user_read` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usr_id`,`obj_id`,`thread_id`,`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `glo_glossaries`
--

DROP TABLE IF EXISTS `glo_glossaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glo_glossaries` (
  `id` int(11) NOT NULL DEFAULT '0',
  `glo_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`glo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `glossary`
--

DROP TABLE IF EXISTS `glossary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glossary` (
  `id` int(11) NOT NULL DEFAULT '0',
  `is_online` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `virtual` char(7) COLLATE utf8_unicode_ci DEFAULT 'none',
  `public_html_file` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_xml_file` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `glo_menu_active` char(1) COLLATE utf8_unicode_ci DEFAULT 'y',
  `downloads_active` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `pres_mode` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'table',
  `snippet_length` int(11) NOT NULL DEFAULT '200',
  `show_tax` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `glossary_definition`
--

DROP TABLE IF EXISTS `glossary_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glossary_definition` (
  `id` int(11) NOT NULL DEFAULT '0',
  `term_id` int(11) NOT NULL DEFAULT '0',
  `short_text` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nr` int(11) NOT NULL DEFAULT '0',
  `short_text_dirty` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `glossary_definition_seq`
--

DROP TABLE IF EXISTS `glossary_definition_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glossary_definition_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1815 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `glossary_term`
--

DROP TABLE IF EXISTS `glossary_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glossary_term` (
  `id` int(11) NOT NULL DEFAULT '0',
  `glo_id` int(11) NOT NULL DEFAULT '0',
  `term` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `import_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`glo_id`),
  FULLTEXT KEY `i2_idx` (`term`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `glossary_term_seq`
--

DROP TABLE IF EXISTS `glossary_term_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glossary_term_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1797 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grp_settings`
--

DROP TABLE IF EXISTS `grp_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grp_settings` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `information` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grp_type` tinyint(4) NOT NULL DEFAULT '0',
  `registration_type` tinyint(4) NOT NULL DEFAULT '0',
  `registration_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `registration_unlimited` tinyint(4) NOT NULL DEFAULT '0',
  `registration_start` datetime DEFAULT NULL,
  `registration_end` datetime DEFAULT NULL,
  `registration_password` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_mem_limit` tinyint(4) NOT NULL DEFAULT '0',
  `registration_max_members` int(11) NOT NULL DEFAULT '0',
  `waiting_list` tinyint(4) NOT NULL DEFAULT '0',
  `latitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_zoom` int(11) NOT NULL DEFAULT '0',
  `enablemap` tinyint(4) NOT NULL DEFAULT '0',
  `reg_ac_enabled` tinyint(4) NOT NULL,
  `reg_ac` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_mode` tinyint(4) NOT NULL DEFAULT '6',
  `mail_members_type` tinyint(4) DEFAULT '1',
  `registration_min_members` smallint(6) DEFAULT NULL,
  `leave_end` int(11) DEFAULT NULL,
  `auto_wait` tinyint(4) NOT NULL DEFAULT '0',
  `show_members` tinyint(4) NOT NULL DEFAULT '1',
  `grp_start` int(11) DEFAULT NULL,
  `grp_end` int(11) DEFAULT NULL,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_map`
--

DROP TABLE IF EXISTS `help_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_map` (
  `chap` int(11) NOT NULL,
  `component` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `screen_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `screen_sub_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `perm` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`component`,`screen_id`,`screen_sub_id`,`chap`,`perm`,`module_id`),
  KEY `sc_idx` (`screen_id`),
  KEY `ch_idx` (`chap`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_module`
--

DROP TABLE IF EXISTS `help_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_module` (
  `id` int(11) NOT NULL DEFAULT '0',
  `lm_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_module_seq`
--

DROP TABLE IF EXISTS `help_module_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_module_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_tooltip`
--

DROP TABLE IF EXISTS `help_tooltip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_tooltip` (
  `id` int(11) NOT NULL,
  `tt_text` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tt_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `comp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lang` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'de',
  `module_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`tt_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_tooltip_seq`
--

DROP TABLE IF EXISTS `help_tooltip_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_tooltip_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=974 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `obj_type` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hdate` datetime DEFAULT NULL,
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `info_params` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_comment` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`obj_id`,`obj_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `history_seq`
--

DROP TABLE IF EXISTS `history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=34651 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `il_bibl_attribute_seq`
--

DROP TABLE IF EXISTS `il_bibl_attribute_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_bibl_attribute_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5738 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `il_bibl_entry_seq`
--

DROP TABLE IF EXISTS `il_bibl_entry_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_bibl_entry_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=672 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_bibl_overview_model`
--

DROP TABLE IF EXISTS `il_bibl_overview_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_bibl_overview_model` (
  `ovm_id` int(11) NOT NULL DEFAULT '0',
  `filetype` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `literature_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pattern` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ovm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `il_bibl_settings_seq`
--

DROP TABLE IF EXISTS `il_bibl_settings_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_bibl_settings_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_block_setting`
--

DROP TABLE IF EXISTS `il_block_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_block_setting` (
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `block_id` int(11) NOT NULL DEFAULT '0',
  `setting` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `value` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`type`,`user_id`,`block_id`,`setting`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_blog`
--

DROP TABLE IF EXISTS `il_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_blog` (
  `id` int(11) NOT NULL DEFAULT '0',
  `bg_color` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `font_color` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppic` tinyint(4) DEFAULT NULL,
  `rss_active` tinyint(4) DEFAULT '0',
  `approval` tinyint(4) DEFAULT '0',
  `abs_shorten` tinyint(4) DEFAULT '0',
  `abs_shorten_len` smallint(6) DEFAULT '0',
  `abs_image` tinyint(4) DEFAULT '0',
  `abs_img_width` smallint(6) DEFAULT '0',
  `abs_img_height` smallint(6) DEFAULT '0',
  `keywords` tinyint(4) NOT NULL DEFAULT '1',
  `authors` tinyint(4) NOT NULL DEFAULT '1',
  `nav_mode` tinyint(4) NOT NULL DEFAULT '1',
  `nav_list_post` smallint(6) NOT NULL DEFAULT '10',
  `nav_list_mon` smallint(6) DEFAULT '0',
  `ov_post` smallint(6) DEFAULT '0',
  `nav_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nav_list_mon_with_post` int(11) DEFAULT '3',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_blog_posting`
--

DROP TABLE IF EXISTS `il_blog_posting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_blog_posting` (
  `id` int(11) NOT NULL DEFAULT '0',
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_blog_posting_seq`
--

DROP TABLE IF EXISTS `il_blog_posting_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_blog_posting_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=803 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--
-- Table structure for table `il_certificate`
--

DROP TABLE IF EXISTS `il_certificate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_certificate` (
  `obj_id` int(11) NOT NULL,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `il_component`
--

DROP TABLE IF EXISTS `il_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_component` (
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`type`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_custom_block`
--

DROP TABLE IF EXISTS `il_custom_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_custom_block` (
  `id` int(11) NOT NULL DEFAULT '0',
  `context_obj_id` int(11) DEFAULT NULL,
  `context_obj_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context_sub_obj_id` int(11) DEFAULT NULL,
  `context_sub_obj_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_custom_block_seq`
--

DROP TABLE IF EXISTS `il_custom_block_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_custom_block_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_data`
--

DROP TABLE IF EXISTS `il_dcl_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_data` (
  `id` int(11) NOT NULL,
  `is_online` tinyint(4) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `public_notes` tinyint(4) DEFAULT NULL,
  `approval` tinyint(4) DEFAULT NULL,
  `notification` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `il_dcl_datatype`
--

DROP TABLE IF EXISTS `il_dcl_datatype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_datatype` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ildb_type` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `storage_location` int(11) NOT NULL,
  `sort` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_datatype_prop`
--

DROP TABLE IF EXISTS `il_dcl_datatype_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_datatype_prop` (
  `id` int(11) NOT NULL,
  `datatype_id` int(11) DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inputformat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_field`
--

DROP TABLE IF EXISTS `il_dcl_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_field` (
  `id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `datatype_id` int(11) NOT NULL,
  `required` tinyint(4) NOT NULL,
  `is_unique` tinyint(4) NOT NULL DEFAULT '0',
  `is_locked` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`datatype_id`),
  KEY `i2_idx` (`table_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_field_prop`
--

DROP TABLE IF EXISTS `il_dcl_field_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_field_prop` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `field_id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_field_prop_b`
--

DROP TABLE IF EXISTS `il_dcl_field_prop_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_field_prop_b` (
  `id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `datatype_prop_id` int(11) NOT NULL,
  `value` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`field_id`),
  KEY `i2_idx` (`datatype_prop_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_field_prop_s_b`
--

DROP TABLE IF EXISTS `il_dcl_field_prop_s_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_field_prop_s_b` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=853 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `il_dcl_field_seq`
--

DROP TABLE IF EXISTS `il_dcl_field_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_field_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=392 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_record`
--

DROP TABLE IF EXISTS `il_dcl_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_record` (
  `id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `owner` int(11) NOT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`table_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_record_field`
--

DROP TABLE IF EXISTS `il_dcl_record_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_record_field` (
  `id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`record_id`),
  KEY `i2_idx` (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_record_field_seq`
--

DROP TABLE IF EXISTS `il_dcl_record_field_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_record_field_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=7303 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_record_seq`
--

DROP TABLE IF EXISTS `il_dcl_record_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_record_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1198 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `il_dcl_stloc1_value`
--

DROP TABLE IF EXISTS `il_dcl_stloc1_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_stloc1_value` (
  `id` int(11) NOT NULL,
  `record_field_id` int(11) NOT NULL,
  `value` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`record_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_stloc1_value_seq`
--

DROP TABLE IF EXISTS `il_dcl_stloc1_value_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_stloc1_value_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=10229 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_stloc2_value`
--

DROP TABLE IF EXISTS `il_dcl_stloc2_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_stloc2_value` (
  `id` int(11) NOT NULL,
  `record_field_id` int(11) NOT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`record_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_stloc2_value_seq`
--

DROP TABLE IF EXISTS `il_dcl_stloc2_value_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_stloc2_value_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3397 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_stloc3_value`
--

DROP TABLE IF EXISTS `il_dcl_stloc3_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_stloc3_value` (
  `id` int(11) NOT NULL,
  `record_field_id` int(11) NOT NULL,
  `value` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`record_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_stloc3_value_seq`
--

DROP TABLE IF EXISTS `il_dcl_stloc3_value_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_stloc3_value_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1606 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_table`
--

DROP TABLE IF EXISTS `il_dcl_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_table` (
  `id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_perm` tinyint(4) NOT NULL DEFAULT '1',
  `edit_perm` tinyint(4) NOT NULL DEFAULT '1',
  `delete_perm` tinyint(4) NOT NULL DEFAULT '1',
  `edit_by_owner` tinyint(4) NOT NULL DEFAULT '1',
  `limited` tinyint(4) NOT NULL DEFAULT '0',
  `limit_start` datetime DEFAULT NULL,
  `limit_end` datetime DEFAULT NULL,
  `is_visible` tinyint(4) NOT NULL DEFAULT '1',
  `export_enabled` tinyint(4) DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_sort_field_id` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `default_sort_field_order` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'asc',
  `public_comments` int(11) NOT NULL DEFAULT '0',
  `view_own_records_perm` int(11) NOT NULL DEFAULT '0',
  `delete_by_owner` tinyint(4) NOT NULL DEFAULT '0',
  `save_confirmation` tinyint(4) NOT NULL DEFAULT '0',
  `import_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `table_order` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_table_seq`
--

DROP TABLE IF EXISTS `il_dcl_table_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_table_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_tableview`
--

DROP TABLE IF EXISTS `il_dcl_tableview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_tableview` (
  `id` bigint(20) NOT NULL,
  `table_id` bigint(20) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `roles` text COLLATE utf8_unicode_ci,
  `description` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tableview_order` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t1_idx` (`table_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_tableview_seq`
--

DROP TABLE IF EXISTS `il_dcl_tableview_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_tableview_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_tfield_set`
--

DROP TABLE IF EXISTS `il_dcl_tfield_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_tfield_set` (
  `id` bigint(20) NOT NULL,
  `table_id` bigint(20) NOT NULL,
  `field` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `field_order` bigint(20) DEFAULT NULL,
  `exportable` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t2_idx` (`table_id`,`field`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_tfield_set_seq`
--

DROP TABLE IF EXISTS `il_dcl_tfield_set_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_tfield_set_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=784 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_tview_set`
--

DROP TABLE IF EXISTS `il_dcl_tview_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_tview_set` (
  `id` bigint(20) NOT NULL,
  `tableview_id` bigint(20) NOT NULL,
  `field` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(4) DEFAULT NULL,
  `in_filter` tinyint(4) DEFAULT NULL,
  `filter_value` longtext COLLATE utf8_unicode_ci,
  `filter_changeable` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_tview_set_seq`
--

DROP TABLE IF EXISTS `il_dcl_tview_set_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_tview_set_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=774 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_dcl_view_seq`
--

DROP TABLE IF EXISTS `il_dcl_view_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_dcl_view_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=526 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_disk_quota`
--

DROP TABLE IF EXISTS `il_disk_quota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_disk_quota` (
  `owner_id` int(11) NOT NULL,
  `src_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `src_obj_id` int(11) NOT NULL,
  `src_size` int(11) NOT NULL,
  PRIMARY KEY (`owner_id`,`src_type`,`src_obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_event_handling`
--

DROP TABLE IF EXISTS `il_event_handling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_event_handling` (
  `component` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`component`,`type`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--
-- Table structure for table `il_md_cpr_selections`
--

DROP TABLE IF EXISTS `il_md_cpr_selections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_md_cpr_selections` (
  `entry_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `copyright` longtext COLLATE utf8_unicode_ci,
  `language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `costs` tinyint(4) NOT NULL DEFAULT '0',
  `cpr_restrictions` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_md_cpr_selections_seq`
--

DROP TABLE IF EXISTS `il_md_cpr_selections_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_md_cpr_selections_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `il_meta_annotation_seq`
--

DROP TABLE IF EXISTS `il_meta_annotation_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_annotation_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_classification`
--

DROP TABLE IF EXISTS `il_meta_classification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_classification` (
  `meta_classification_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purpose` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_classification_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_classification_seq`
--

DROP TABLE IF EXISTS `il_meta_classification_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_classification_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_contribute`
--

DROP TABLE IF EXISTS `il_meta_contribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_contribute` (
  `meta_contribute_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `role` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_date` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_contribute_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_contribute_seq`
--

DROP TABLE IF EXISTS `il_meta_contribute_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_contribute_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3173 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_description`
--

DROP TABLE IF EXISTS `il_meta_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_description` (
  `meta_description_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `description_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_description_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`),
  FULLTEXT KEY `i2_idx` (`description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_description_seq`
--

DROP TABLE IF EXISTS `il_meta_description_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_description_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=67353 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_educational`
--

DROP TABLE IF EXISTS `il_meta_educational`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_educational` (
  `meta_educational_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interactivity_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `learning_resource_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interactivity_level` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `semantic_density` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intended_end_user_role` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `difficulty` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typical_learning_time` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_educational_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_educational_seq`
--

DROP TABLE IF EXISTS `il_meta_educational_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_educational_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_entity`
--

DROP TABLE IF EXISTS `il_meta_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_entity` (
  `meta_entity_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `entity` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_entity_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`),
  FULLTEXT KEY `i2_idx` (`entity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_entity_seq`
--

DROP TABLE IF EXISTS `il_meta_entity_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_entity_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=4832 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_format`
--

DROP TABLE IF EXISTS `il_meta_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_format` (
  `meta_format_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_format_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`),
  KEY `i2_idx` (`format`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_format_seq`
--

DROP TABLE IF EXISTS `il_meta_format_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_format_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2081 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_general`
--

DROP TABLE IF EXISTS `il_meta_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_general` (
  `meta_general_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `general_structure` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coverage` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coverage_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_general_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`),
  FULLTEXT KEY `i2_idx` (`title`,`coverage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_general_seq`
--

DROP TABLE IF EXISTS `il_meta_general_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_general_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=67325 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_identifier`
--

DROP TABLE IF EXISTS `il_meta_identifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_identifier` (
  `meta_identifier_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `catalog` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_identifier_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_identifier_`
--

DROP TABLE IF EXISTS `il_meta_identifier_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_identifier_` (
  `meta_identifier__id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `catalog` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_identifier__id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_identifier__seq`
--

DROP TABLE IF EXISTS `il_meta_identifier__seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_identifier__seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_identifier_seq`
--

DROP TABLE IF EXISTS `il_meta_identifier_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_identifier_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=67252 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_keyword`
--

DROP TABLE IF EXISTS `il_meta_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_keyword` (
  `meta_keyword_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `keyword` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_keyword_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`),
  FULLTEXT KEY `i2_idx` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_keyword_seq`
--

DROP TABLE IF EXISTS `il_meta_keyword_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_keyword_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=68152 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_language`
--

DROP TABLE IF EXISTS `il_meta_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_language` (
  `meta_language_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_language_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_language_seq`
--

DROP TABLE IF EXISTS `il_meta_language_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_language_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=67243 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_lifecycle`
--

DROP TABLE IF EXISTS `il_meta_lifecycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_lifecycle` (
  `meta_lifecycle_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lifecycle_status` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_version` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_lifecycle_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_lifecycle_seq`
--

DROP TABLE IF EXISTS `il_meta_lifecycle_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_lifecycle_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3165 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `il_meta_meta_data`
--

DROP TABLE IF EXISTS `il_meta_meta_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_meta_data` (
  `meta_meta_data_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_data_scheme` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_meta_data_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_meta_data_seq`
--

DROP TABLE IF EXISTS `il_meta_meta_data_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_meta_data_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_relation`
--

DROP TABLE IF EXISTS `il_meta_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_relation` (
  `meta_relation_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kind` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_relation_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_relation_seq`
--

DROP TABLE IF EXISTS `il_meta_relation_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_relation_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `il_meta_rights`
--

DROP TABLE IF EXISTS `il_meta_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_rights` (
  `meta_rights_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `costs` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpr_and_or` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_rights_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_rights_seq`
--

DROP TABLE IF EXISTS `il_meta_rights_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_rights_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `il_meta_tar_seq`
--

DROP TABLE IF EXISTS `il_meta_tar_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_tar_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_taxon`
--

DROP TABLE IF EXISTS `il_meta_taxon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_taxon` (
  `meta_taxon_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `taxon` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taxon_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taxon_id` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_taxon_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_taxon_path`
--

DROP TABLE IF EXISTS `il_meta_taxon_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_taxon_path` (
  `meta_taxon_path_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `source` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_taxon_path_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_taxon_path_seq`
--

DROP TABLE IF EXISTS `il_meta_taxon_path_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_taxon_path_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_taxon_seq`
--

DROP TABLE IF EXISTS `il_meta_taxon_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_taxon_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_technical`
--

DROP TABLE IF EXISTS `il_meta_technical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_technical` (
  `meta_technical_id` int(11) NOT NULL DEFAULT '0',
  `rbac_id` int(11) DEFAULT NULL,
  `obj_id` int(11) DEFAULT NULL,
  `obj_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `t_size` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ir` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ir_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opr` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opr_language` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_technical_id`),
  KEY `i1_idx` (`rbac_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_meta_technical_seq`
--

DROP TABLE IF EXISTS `il_meta_technical_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_meta_technical_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2067 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `il_news_item`
--

DROP TABLE IF EXISTS `il_news_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_news_item` (
  `id` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) DEFAULT '1',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `context_obj_id` int(11) DEFAULT NULL,
  `context_obj_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context_sub_obj_id` int(11) DEFAULT NULL,
  `context_sub_obj_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_type` char(5) COLLATE utf8_unicode_ci DEFAULT 'text',
  `creation_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `visibility` char(6) COLLATE utf8_unicode_ci DEFAULT 'users',
  `content_long` longtext COLLATE utf8_unicode_ci,
  `content_is_lang_var` tinyint(4) DEFAULT '0',
  `mob_id` int(11) DEFAULT NULL,
  `playtime` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `content_text_is_lang_var` tinyint(4) NOT NULL DEFAULT '0',
  `mob_cnt_download` int(11) NOT NULL DEFAULT '0',
  `mob_cnt_play` int(11) NOT NULL DEFAULT '0',
  `content_html` tinyint(4) NOT NULL DEFAULT '0',
  `update_user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`context_obj_id`),
  KEY `i2_idx` (`creation_date`),
  KEY `mo_idx` (`mob_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_news_item_seq`
--

DROP TABLE IF EXISTS `il_news_item_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_news_item_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=12404 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `il_object_def`
--

DROP TABLE IF EXISTS `il_object_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_object_def` (
  `id` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `component` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkbox` tinyint(4) NOT NULL DEFAULT '0',
  `inherit` tinyint(4) NOT NULL DEFAULT '0',
  `translate` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `devmode` tinyint(4) NOT NULL DEFAULT '0',
  `allow_link` tinyint(4) NOT NULL DEFAULT '0',
  `allow_copy` tinyint(4) NOT NULL DEFAULT '0',
  `rbac` tinyint(4) NOT NULL DEFAULT '0',
  `system` tinyint(4) NOT NULL DEFAULT '0',
  `sideblock` tinyint(4) NOT NULL DEFAULT '0',
  `default_pos` int(11) NOT NULL DEFAULT '0',
  `grp` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_pres_pos` int(11) NOT NULL DEFAULT '0',
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `repository` tinyint(4) NOT NULL DEFAULT '1',
  `workspace` tinyint(4) NOT NULL DEFAULT '0',
  `administration` tinyint(4) NOT NULL DEFAULT '0',
  `amet` tinyint(4) NOT NULL DEFAULT '0',
  `orgunit_permissions` tinyint(4) NOT NULL DEFAULT '0',
  `lti_provider` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_object_group`
--

DROP TABLE IF EXISTS `il_object_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_object_group` (
  `id` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_pres_pos` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_object_sub_type`
--

DROP TABLE IF EXISTS `il_object_sub_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_object_sub_type` (
  `obj_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `sub_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `amet` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_type`,`sub_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_object_subobj`
--

DROP TABLE IF EXISTS `il_object_subobj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_object_subobj` (
  `parent` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subobj` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mmax` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`parent`,`subobj`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_orgu_authority`
--

DROP TABLE IF EXISTS `il_orgu_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_orgu_authority` (
  `id` bigint(20) NOT NULL,
  `over` tinyint(4) DEFAULT NULL,
  `scope` tinyint(4) DEFAULT NULL,
  `position_id` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `il_orgu_op_contexts`
--

DROP TABLE IF EXISTS `il_orgu_op_contexts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_orgu_op_contexts` (
  `id` bigint(20) NOT NULL,
  `context` varchar(16) DEFAULT NULL,
  `parent_context_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `il_orgu_operations`
--

DROP TABLE IF EXISTS `il_orgu_operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_orgu_operations` (
  `operation_id` bigint(20) NOT NULL,
  `operation_string` varchar(127) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `list_order` bigint(20) DEFAULT NULL,
  `context_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`operation_id`),
  KEY `i1_idx` (`operation_string`),
  KEY `i3_idx` (`list_order`),
  KEY `i4_idx` (`context_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `il_orgu_positions`
--

DROP TABLE IF EXISTS `il_orgu_positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_orgu_positions` (
  `id` bigint(20) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `core_position` tinyint(4) DEFAULT NULL,
  `core_identifier` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `il_plugin`
--

DROP TABLE IF EXISTS `il_plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_plugin` (
  `component_type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `component_name` varchar(90) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `slot_id` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `last_update_version` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `db_version` int(11) NOT NULL DEFAULT '0',
  `plugin_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`component_type`,`component_name`,`slot_id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_pluginslot`
--

DROP TABLE IF EXISTS `il_pluginslot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_pluginslot` (
  `component` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `id` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`component`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `il_poll_answer_seq`
--

DROP TABLE IF EXISTS `il_poll_answer_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_poll_answer_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=236 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `il_qpl_qst_fq_res`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_res`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_res` (
  `result_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `result` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `range_min` double DEFAULT NULL,
  `range_max` double DEFAULT NULL,
  `tolerance` double DEFAULT NULL,
  `unit_fi` int(11) DEFAULT NULL,
  `formula` longtext COLLATE utf8_unicode_ci,
  `rating_simple` int(11) NOT NULL DEFAULT '1',
  `rating_sign` double NOT NULL DEFAULT '0.25',
  `rating_value` double NOT NULL DEFAULT '0.25',
  `rating_unit` double NOT NULL DEFAULT '0.25',
  `points` double NOT NULL DEFAULT '0',
  `resprecision` int(11) NOT NULL DEFAULT '0',
  `result_type` bigint(20) DEFAULT '0',
  `range_min_txt` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `range_max_txt` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`result_id`),
  KEY `i1_idx` (`question_fi`,`result`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_qpl_qst_fq_res_seq`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_res_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_res_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=4467 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `il_qpl_qst_fq_res_unit_seq`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_res_unit_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_res_unit_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_qpl_qst_fq_ucat`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_ucat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_ucat` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_fi` bigint(20) DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_qpl_qst_fq_ucat_seq`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_ucat_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_ucat_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_qpl_qst_fq_unit`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_unit` (
  `unit_id` int(11) NOT NULL DEFAULT '0',
  `unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` double NOT NULL DEFAULT '0',
  `baseunit_fi` int(11) DEFAULT NULL,
  `category_fi` int(11) NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL DEFAULT '0',
  `question_fi` bigint(20) DEFAULT '0',
  PRIMARY KEY (`unit_id`),
  KEY `i1_idx` (`baseunit_fi`,`category_fi`),
  KEY `i2_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_qpl_qst_fq_unit_seq`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_unit_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_unit_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_qpl_qst_fq_var`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_var`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_var` (
  `variable_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `variable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `range_min` double NOT NULL DEFAULT '0',
  `range_max` double NOT NULL DEFAULT '0',
  `unit_fi` int(11) DEFAULT NULL,
  `step_dim_min` int(11) NOT NULL DEFAULT '0',
  `step_dim_max` int(11) NOT NULL DEFAULT '0',
  `varprecision` int(11) NOT NULL DEFAULT '0',
  `intprecision` int(11) NOT NULL DEFAULT '1',
  `range_min_txt` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `range_max_txt` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `i1_idx` (`question_fi`,`variable`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_qpl_qst_fq_var_seq`
--

DROP TABLE IF EXISTS `il_qpl_qst_fq_var_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_qpl_qst_fq_var_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2021 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_rating`
--

DROP TABLE IF EXISTS `il_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_rating` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `obj_type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sub_obj_id` int(11) NOT NULL DEFAULT '0',
  `sub_obj_type` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`obj_id`,`obj_type`,`sub_obj_id`,`sub_obj_type`,`user_id`,`category_id`),
  KEY `obj_idx` (`obj_id`,`obj_type`,`sub_obj_id`,`sub_obj_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_rating_cat`
--

DROP TABLE IF EXISTS `il_rating_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_rating_cat` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_rating_cat_seq`
--

DROP TABLE IF EXISTS `il_rating_cat_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_rating_cat_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_request_token`
--

DROP TABLE IF EXISTS `il_request_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_request_token` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `token` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `stamp` datetime DEFAULT NULL,
  `session_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `i1_idx` (`user_id`,`session_id`),
  KEY `i2_idx` (`user_id`,`stamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `il_wac_secure_path`
--

DROP TABLE IF EXISTS `il_wac_secure_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_wac_secure_path` (
  `path` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `component_directory` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checking_class` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `in_sec_folder` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`path`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_wiki_contributor`
--

DROP TABLE IF EXISTS `il_wiki_contributor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_wiki_contributor` (
  `wiki_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `status_time` datetime DEFAULT NULL,
  PRIMARY KEY (`wiki_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_wiki_data`
--

DROP TABLE IF EXISTS `il_wiki_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_wiki_data` (
  `id` int(11) NOT NULL DEFAULT '0',
  `startpage` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_online` tinyint(4) DEFAULT '0',
  `rating` tinyint(4) DEFAULT '0',
  `introduction` longtext COLLATE utf8_unicode_ci,
  `public_notes` tinyint(4) DEFAULT '1',
  `imp_pages` tinyint(4) DEFAULT NULL,
  `page_toc` tinyint(4) DEFAULT NULL,
  `rating_side` tinyint(4) NOT NULL DEFAULT '0',
  `rating_new` tinyint(4) NOT NULL DEFAULT '0',
  `rating_ext` tinyint(4) NOT NULL DEFAULT '0',
  `rating_overall` tinyint(4) DEFAULT '0',
  `empty_page_templ` tinyint(4) NOT NULL DEFAULT '1',
  `link_md_values` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_wiki_imp_pages`
--

DROP TABLE IF EXISTS `il_wiki_imp_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_wiki_imp_pages` (
  `wiki_id` int(11) NOT NULL,
  `ord` int(11) NOT NULL,
  `indent` tinyint(4) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`wiki_id`,`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_wiki_missing_page`
--

DROP TABLE IF EXISTS `il_wiki_missing_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_wiki_missing_page` (
  `wiki_id` int(11) NOT NULL DEFAULT '0',
  `source_id` int(11) NOT NULL DEFAULT '0',
  `target_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`wiki_id`,`source_id`,`target_name`),
  KEY `i1_idx` (`wiki_id`,`target_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_wiki_page`
--

DROP TABLE IF EXISTS `il_wiki_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_wiki_page` (
  `id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wiki_id` int(11) NOT NULL DEFAULT '0',
  `blocked` tinyint(4) DEFAULT NULL,
  `rating` tinyint(4) NOT NULL DEFAULT '0',
  `hide_adv_md` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `il_wiki_page_seq`
--

DROP TABLE IF EXISTS `il_wiki_page_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `il_wiki_page_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1773 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `int_link`
--

DROP TABLE IF EXISTS `int_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `int_link` (
  `source_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `source_id` int(11) NOT NULL DEFAULT '0',
  `target_type` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `target_id` int(11) NOT NULL DEFAULT '0',
  `target_inst` int(11) NOT NULL DEFAULT '0',
  `source_lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`source_type`,`source_id`,`source_lang`,`target_type`,`target_id`,`target_inst`),
  KEY `ta_idx` (`target_type`,`target_id`,`target_inst`),
  KEY `so_idx` (`source_type`,`source_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_group_item`
--

DROP TABLE IF EXISTS `item_group_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_group_item` (
  `item_group_id` int(11) NOT NULL DEFAULT '0',
  `item_ref_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_group_id`,`item_ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `itgr_data`
--

DROP TABLE IF EXISTS `itgr_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itgr_data` (
  `id` int(11) NOT NULL,
  `hide_title` tinyint(4) NOT NULL DEFAULT '0',
  `behaviour` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--
-- Table structure for table `lf_optes_sm_col`
--

DROP TABLE IF EXISTS `lf_optes_sm_col`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lf_optes_sm_col` (
  `id` int(11) NOT NULL,
  `nr` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `lf_optes_sm_row`
--

DROP TABLE IF EXISTS `lf_optes_sm_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lf_optes_sm_row` (
  `id` int(11) NOT NULL,
  `nr` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `lf_optes_sm_skill`
--

DROP TABLE IF EXISTS `lf_optes_sm_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lf_optes_sm_skill` (
  `col_id` int(11) NOT NULL,
  `row_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `tref_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`col_id`,`row_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `link_check_seq`
--

DROP TABLE IF EXISTS `link_check_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_check_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=10713 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lm_data`
--

DROP TABLE IF EXISTS `lm_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lm_data` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lm_id` int(11) NOT NULL DEFAULT '0',
  `import_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_access` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `create_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `active` char(1) COLLATE utf8_unicode_ci DEFAULT 'y',
  `layout` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_id`),
  KEY `i1_idx` (`lm_id`),
  KEY `i2_idx` (`type`),
  KEY `im_idx` (`import_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lm_data_seq`
--

DROP TABLE IF EXISTS `lm_data_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lm_data_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=27415 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `lm_menu`
--

DROP TABLE IF EXISTS `lm_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lm_menu` (
  `id` int(11) NOT NULL DEFAULT '0',
  `lm_id` int(11) NOT NULL DEFAULT '0',
  `link_type` char(6) COLLATE utf8_unicode_ci DEFAULT 'extern',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_ref_id` int(11) DEFAULT NULL,
  `active` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`link_type`),
  KEY `i2_idx` (`lm_id`),
  KEY `i3_idx` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lm_menu_seq`
--

DROP TABLE IF EXISTS `lm_menu_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lm_menu_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lm_read_event`
--

DROP TABLE IF EXISTS `lm_read_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lm_read_event` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `read_count` int(11) NOT NULL DEFAULT '0',
  `spent_seconds` int(11) NOT NULL DEFAULT '0',
  `last_access` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`,`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lm_tree`
--

DROP TABLE IF EXISTS `lm_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lm_tree` (
  `lm_id` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lm_id`,`child`),
  KEY `i1_idx` (`child`),
  KEY `i2_idx` (`parent`),
  KEY `i3_idx` (`lm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lng_data`
--

DROP TABLE IF EXISTS `lng_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lng_data` (
  `module` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `identifier` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `lang_key` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `local_change` datetime DEFAULT NULL,
  `remarks` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`module`,`identifier`,`lang_key`),
  KEY `i1_idx` (`module`),
  KEY `i2_idx` (`lang_key`),
  KEY `i3_idx` (`local_change`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `lng_modules`
--

DROP TABLE IF EXISTS `lng_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lng_modules` (
  `module` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `lang_key` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lang_array` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`module`,`lang_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lo_access`
--

DROP TABLE IF EXISTS `lo_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lo_access` (
  `timestamp` datetime DEFAULT NULL,
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `lm_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `lm_title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usr_id`,`lm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loc_rnd_qpl`
--

DROP TABLE IF EXISTS `loc_rnd_qpl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loc_rnd_qpl` (
  `container_id` int(11) NOT NULL DEFAULT '0',
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `tst_type` tinyint(4) NOT NULL DEFAULT '0',
  `tst_id` int(11) NOT NULL DEFAULT '0',
  `qp_seq` int(11) NOT NULL DEFAULT '0',
  `percentage` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`container_id`,`objective_id`,`tst_type`,`tst_id`,`qp_seq`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loc_settings`
--

DROP TABLE IF EXISTS `loc_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loc_settings` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `itest` int(11) DEFAULT NULL,
  `qtest` int(11) DEFAULT NULL,
  `qt_vis_all` tinyint(4) DEFAULT '1',
  `qt_vis_obj` tinyint(4) DEFAULT '0',
  `reset_results` tinyint(4) DEFAULT '0',
  `it_type` tinyint(4) DEFAULT '5',
  `qt_type` tinyint(4) DEFAULT '1',
  `it_start` tinyint(4) DEFAULT '1',
  `qt_start` tinyint(4) DEFAULT '1',
  `passed_obj_mode` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loc_tst_assignments`
--

DROP TABLE IF EXISTS `loc_tst_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loc_tst_assignments` (
  `assignment_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL DEFAULT '0',
  `assignment_type` tinyint(4) NOT NULL DEFAULT '0',
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `tst_ref_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`assignment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loc_tst_assignments_seq`
--

DROP TABLE IF EXISTS `loc_tst_assignments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loc_tst_assignments_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loc_tst_run`
--

DROP TABLE IF EXISTS `loc_tst_run`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loc_tst_run` (
  `container_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `test_id` int(11) NOT NULL DEFAULT '0',
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `max_points` int(11) DEFAULT '0',
  `questions` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`container_id`,`user_id`,`test_id`,`objective_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loc_user_results`
--

DROP TABLE IF EXISTS `loc_user_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loc_user_results` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL DEFAULT '0',
  `objective_id` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `result_perc` tinyint(4) DEFAULT '0',
  `limit_perc` tinyint(4) DEFAULT '0',
  `tries` tinyint(4) DEFAULT '0',
  `is_final` tinyint(4) DEFAULT '0',
  `tstamp` int(11) DEFAULT '0',
  PRIMARY KEY (`user_id`,`course_id`,`objective_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_components`
--

DROP TABLE IF EXISTS `log_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_components` (
  `component_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `log_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`component_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loginname_history`
--

DROP TABLE IF EXISTS `loginname_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loginname_history` (
  `usr_id` int(11) NOT NULL,
  `login` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `history_date` int(11) NOT NULL,
  PRIMARY KEY (`usr_id`,`login`,`history_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--


--


--


--


--


--


--


--
-- Table structure for table `mail`
--

DROP TABLE IF EXISTS `mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail` (
  `mail_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `folder_id` int(11) NOT NULL DEFAULT '0',
  `sender_id` int(11) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `m_status` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_email` tinyint(4) DEFAULT NULL,
  `m_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `import_name` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use_placeholders` tinyint(4) NOT NULL DEFAULT '0',
  `m_message` longtext COLLATE utf8_unicode_ci,
  `rcp_to` longtext COLLATE utf8_unicode_ci,
  `rcp_cc` longtext COLLATE utf8_unicode_ci,
  `rcp_bcc` longtext COLLATE utf8_unicode_ci,
  `attachments` longtext COLLATE utf8_unicode_ci,
  `tpl_ctx_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpl_ctx_params` longblob,
  PRIMARY KEY (`mail_id`),
  KEY `i1_idx` (`user_id`),
  KEY `i2_idx` (`folder_id`),
  KEY `i3_idx` (`m_status`),
  KEY `i4_idx` (`sender_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_attachment`
--

DROP TABLE IF EXISTS `mail_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_attachment` (
  `mail_id` int(11) NOT NULL DEFAULT '0',
  `path` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`mail_id`),
  KEY `i1_idx` (`path`(333))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_cron_orphaned`
--

DROP TABLE IF EXISTS `mail_cron_orphaned`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_cron_orphaned` (
  `mail_id` int(11) NOT NULL,
  `folder_id` int(11) NOT NULL,
  `ts_do_delete` int(11) NOT NULL,
  PRIMARY KEY (`mail_id`,`folder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `mail_obj_data`
--

DROP TABLE IF EXISTS `mail_obj_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_obj_data` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` char(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_type` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_id`),
  KEY `i1_idx` (`user_id`,`m_type`),
  KEY `i2_idx` (`obj_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_obj_data_seq`
--

DROP TABLE IF EXISTS `mail_obj_data_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_obj_data_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=18184 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_options`
--

DROP TABLE IF EXISTS `mail_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_options` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `linebreak` tinyint(4) NOT NULL DEFAULT '0',
  `signature` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `incoming_type` tinyint(4) DEFAULT NULL,
  `cronjob_notification` tinyint(4) NOT NULL DEFAULT '0',
  `mail_address_option` tinyint(4) NOT NULL DEFAULT '3',
  PRIMARY KEY (`user_id`),
  KEY `i1_idx` (`user_id`,`linebreak`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_saved`
--

DROP TABLE IF EXISTS `mail_saved`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_saved` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `m_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_email` tinyint(4) DEFAULT NULL,
  `m_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use_placeholders` tinyint(4) NOT NULL DEFAULT '0',
  `m_message` longtext COLLATE utf8_unicode_ci,
  `rcp_to` longtext COLLATE utf8_unicode_ci,
  `rcp_cc` longtext COLLATE utf8_unicode_ci,
  `rcp_bcc` longtext COLLATE utf8_unicode_ci,
  `attachments` longtext COLLATE utf8_unicode_ci,
  `tpl_ctx_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpl_ctx_params` longblob,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_seq`
--

DROP TABLE IF EXISTS `mail_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=223554 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_template`
--

DROP TABLE IF EXISTS `mail_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_template` (
  `lang` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `subject` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci,
  `sal_f` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sal_m` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sal_g` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `att_file` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`type`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_tpl_ctx`
--

DROP TABLE IF EXISTS `mail_tpl_ctx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_tpl_ctx` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `component` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail_tree`
--

DROP TABLE IF EXISTS `mail_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_tree` (
  `tree` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`child`),
  KEY `i2_idx` (`parent`),
  KEY `i3_idx` (`tree`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `map_area`
--

DROP TABLE IF EXISTS `map_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_area` (
  `item_id` int(11) NOT NULL DEFAULT '0',
  `nr` int(11) NOT NULL DEFAULT '0',
  `shape` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coords` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_type` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `href` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target_frame` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `highlight_mode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `highlight_class` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`item_id`,`nr`),
  KEY `lt_idx` (`link_type`,`target`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `media_item`
--

DROP TABLE IF EXISTS `media_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_item` (
  `id` int(11) NOT NULL DEFAULT '0',
  `width` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `height` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `halign` char(10) COLLATE utf8_unicode_ci DEFAULT 'Left',
  `caption` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nr` int(11) NOT NULL DEFAULT '0',
  `purpose` char(20) COLLATE utf8_unicode_ci DEFAULT 'Standard',
  `mob_id` int(11) NOT NULL DEFAULT '0',
  `location` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_type` char(10) COLLATE utf8_unicode_ci DEFAULT 'LocalFile',
  `format` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `param` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tried_thumb` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `text_representation` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `media_item_seq`
--

DROP TABLE IF EXISTS `media_item_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_item_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=25346 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_agreement`
--

DROP TABLE IF EXISTS `member_agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_agreement` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `accepted` tinyint(4) NOT NULL DEFAULT '0',
  `acceptance_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usr_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `mep_data`
--

DROP TABLE IF EXISTS `mep_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mep_data` (
  `id` int(11) NOT NULL,
  `default_width` int(11) DEFAULT NULL,
  `default_height` int(11) DEFAULT NULL,
  `for_translation` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mep_item`
--

DROP TABLE IF EXISTS `mep_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mep_item` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foreign_id` int(11) DEFAULT NULL,
  `import_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_id`),
  KEY `ft_idx` (`foreign_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mep_item_seq`
--

DROP TABLE IF EXISTS `mep_item_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mep_item_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1368 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mep_tree`
--

DROP TABLE IF EXISTS `mep_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mep_tree` (
  `mep_id` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mep_id`,`child`),
  KEY `ch_idx` (`child`),
  KEY `pa_idx` (`parent`),
  KEY `me_idx` (`mep_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_parameter`
--

DROP TABLE IF EXISTS `mob_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mob_parameter` (
  `med_item_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`med_item_id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mob_usage`
--

DROP TABLE IF EXISTS `mob_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mob_usage` (
  `id` int(11) NOT NULL DEFAULT '0',
  `usage_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `usage_id` int(11) NOT NULL DEFAULT '0',
  `usage_hist_nr` int(11) NOT NULL DEFAULT '0',
  `usage_lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`id`,`usage_type`,`usage_id`,`usage_hist_nr`,`usage_lang`),
  KEY `mi_idx` (`id`),
  KEY `i1_idx` (`usage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `module_class`
--

DROP TABLE IF EXISTS `module_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_class` (
  `class` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `module` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dir` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Class information of ILIAS Modules';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `id` int(11) NOT NULL DEFAULT '0',
  `rep_obj_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `obj_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `author` int(11) NOT NULL DEFAULT '0',
  `note_text` longtext COLLATE utf8_unicode_ci,
  `label` int(11) NOT NULL DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `subject` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_repository` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`author`),
  KEY `i2_idx` (`rep_obj_id`,`obj_id`,`obj_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `note_seq`
--

DROP TABLE IF EXISTS `note_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1005 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `note_settings`
--

DROP TABLE IF EXISTS `note_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note_settings` (
  `rep_obj_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `obj_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `activated` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rep_obj_id`,`obj_id`,`obj_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `type` tinyint(4) NOT NULL,
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_mail` datetime DEFAULT NULL,
  `page_id` int(11) DEFAULT '0',
  `activated` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`type`,`id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_channels`
--

DROP TABLE IF EXISTS `notification_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_channels` (
  `channel_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `include` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `config_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`channel_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `notification_osd_seq`
--

DROP TABLE IF EXISTS `notification_osd_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_osd_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `notification_types`
--

DROP TABLE IF EXISTS `notification_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_types` (
  `type_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notification_group` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `config_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`type_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_usercfg`
--

DROP TABLE IF EXISTS `notification_usercfg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_usercfg` (
  `usr_id` int(11) NOT NULL,
  `module` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `channel` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`usr_id`,`module`,`channel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `obj_lp_stat`
--

DROP TABLE IF EXISTS `obj_lp_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_lp_stat` (
  `type` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `obj_id` int(11) NOT NULL,
  `yyyy` smallint(6) NOT NULL,
  `mm` tinyint(4) NOT NULL,
  `dd` tinyint(4) NOT NULL,
  `fulldate` int(11) NOT NULL,
  `mem_cnt` int(11) DEFAULT NULL,
  `in_progress` int(11) DEFAULT NULL,
  `completed` int(11) DEFAULT NULL,
  `failed` int(11) DEFAULT NULL,
  `not_attempted` int(11) DEFAULT NULL,
  PRIMARY KEY (`obj_id`,`fulldate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obj_members`
--

DROP TABLE IF EXISTS `obj_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_members` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `blocked` tinyint(4) NOT NULL DEFAULT '0',
  `notification` tinyint(4) NOT NULL DEFAULT '0',
  `passed` tinyint(4) DEFAULT NULL,
  `origin` int(11) DEFAULT '0',
  `origin_ts` int(11) DEFAULT '0',
  `contact` tinyint(4) DEFAULT '0',
  `admin` tinyint(4) DEFAULT '0',
  `tutor` tinyint(4) DEFAULT '0',
  `member` smallint(6) DEFAULT '0',
  PRIMARY KEY (`obj_id`,`usr_id`),
  KEY `i1_idx` (`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obj_noti_settings`
--

DROP TABLE IF EXISTS `obj_noti_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_noti_settings` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `noti_mode` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obj_stat`
--

DROP TABLE IF EXISTS `obj_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_stat` (
  `obj_id` int(11) NOT NULL,
  `obj_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `yyyy` smallint(6) NOT NULL DEFAULT '0',
  `mm` tinyint(4) NOT NULL DEFAULT '0',
  `dd` tinyint(4) NOT NULL DEFAULT '0',
  `hh` tinyint(4) NOT NULL DEFAULT '0',
  `read_count` int(11) DEFAULT NULL,
  `childs_read_count` int(11) DEFAULT NULL,
  `spent_seconds` int(11) DEFAULT NULL,
  `childs_spent_seconds` int(11) DEFAULT NULL,
  PRIMARY KEY (`obj_id`,`yyyy`,`mm`,`dd`,`hh`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obj_stat_log`
--

DROP TABLE IF EXISTS `obj_stat_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_stat_log` (
  `log_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `obj_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tstamp` int(11) DEFAULT NULL,
  `yyyy` smallint(6) DEFAULT NULL,
  `mm` tinyint(4) DEFAULT NULL,
  `dd` tinyint(4) DEFAULT NULL,
  `hh` tinyint(4) DEFAULT NULL,
  `read_count` int(11) DEFAULT NULL,
  `childs_read_count` int(11) DEFAULT NULL,
  `spent_seconds` int(11) DEFAULT NULL,
  `childs_spent_seconds` int(11) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `i1_idx` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obj_stat_log_seq`
--

DROP TABLE IF EXISTS `obj_stat_log_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_stat_log_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=1931375 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `obj_type_stat`
--

DROP TABLE IF EXISTS `obj_type_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_type_stat` (
  `type` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `yyyy` smallint(6) NOT NULL,
  `mm` tinyint(4) NOT NULL,
  `dd` tinyint(4) NOT NULL,
  `fulldate` int(11) NOT NULL,
  `cnt_references` int(11) DEFAULT NULL,
  `cnt_objects` int(11) DEFAULT NULL,
  `cnt_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`type`,`fulldate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obj_user_data_hist`
--

DROP TABLE IF EXISTS `obj_user_data_hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_user_data_hist` (
  `obj_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `update_user` int(11) NOT NULL,
  `editing_time` datetime DEFAULT NULL,
  PRIMARY KEY (`obj_id`,`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obj_user_stat`
--

DROP TABLE IF EXISTS `obj_user_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_user_stat` (
  `obj_id` int(11) NOT NULL,
  `yyyy` smallint(6) NOT NULL,
  `mm` tinyint(4) NOT NULL,
  `dd` tinyint(4) NOT NULL,
  `fulldate` int(11) NOT NULL,
  `counter` int(11) DEFAULT NULL,
  PRIMARY KEY (`obj_id`,`fulldate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_data`
--

DROP TABLE IF EXISTS `object_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_data` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `type` char(4) COLLATE utf8_unicode_ci DEFAULT 'none',
  `title` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` char(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `import_id` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_id`),
  KEY `i1_idx` (`type`),
  KEY `i2_idx` (`title`),
  KEY `i4_idx` (`import_id`),
  FULLTEXT KEY `i3_idx` (`title`,`description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_data_del`
--

DROP TABLE IF EXISTS `object_data_del`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_data_del` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `type` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_data_seq`
--

DROP TABLE IF EXISTS `object_data_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_data_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=47008 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_description`
--

DROP TABLE IF EXISTS `object_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_description` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_reference`
--

DROP TABLE IF EXISTS `object_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_reference` (
  `ref_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`ref_id`),
  KEY `i1_idx` (`obj_id`),
  KEY `i2_idx` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_reference_seq`
--

DROP TABLE IF EXISTS `object_reference_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_reference_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=10882 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_reference_ws`
--

DROP TABLE IF EXISTS `object_reference_ws`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_reference_ws` (
  `wsp_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`wsp_id`),
  KEY `i1_idx` (`obj_id`),
  KEY `i2_idx` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_reference_ws_seq`
--

DROP TABLE IF EXISTS `object_reference_ws_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_reference_ws_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2420 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `object_translation`
--

DROP TABLE IF EXISTS `object_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_translation` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang_code` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lang_default` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `openid_provider`
--

DROP TABLE IF EXISTS `openid_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `openid_provider` (
  `provider_id` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(4) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`provider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `openid_provider_seq`
--

DROP TABLE IF EXISTS `openid_provider_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `openid_provider_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `orgu_obj_pos_settings`
--

DROP TABLE IF EXISTS `orgu_obj_pos_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orgu_obj_pos_settings` (
  `obj_id` int(11) NOT NULL,
  `active` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--
-- Table structure for table `page_anchor`
--

DROP TABLE IF EXISTS `page_anchor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_anchor` (
  `page_parent_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `anchor_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `page_lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`page_parent_type`,`page_id`,`page_lang`,`anchor_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_editor_settings`
--

DROP TABLE IF EXISTS `page_editor_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_editor_settings` (
  `settings_grp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`settings_grp`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_history`
--

DROP TABLE IF EXISTS `page_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_history` (
  `page_id` int(11) NOT NULL DEFAULT '0',
  `parent_type` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `hdate` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `nr` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `ilias_version` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`page_id`,`parent_type`,`hdate`,`lang`),
  KEY `i1_idx` (`page_id`),
  KEY `i2_idx` (`parent_id`,`parent_type`,`hdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_layout`
--

DROP TABLE IF EXISTS `page_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_layout` (
  `layout_id` int(11) NOT NULL DEFAULT '0',
  `content` longtext COLLATE utf8_unicode_ci,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0',
  `style_id` int(11) DEFAULT '0',
  `special_page` tinyint(4) DEFAULT '0',
  `mod_scorm` tinyint(4) DEFAULT '1',
  `mod_portfolio` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_layout_seq`
--

DROP TABLE IF EXISTS `page_layout_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_layout_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_object`
--

DROP TABLE IF EXISTS `page_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_object` (
  `page_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `parent_type` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'lm',
  `last_change_user` int(11) DEFAULT NULL,
  `view_cnt` int(11) DEFAULT '0',
  `last_change` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_user` int(11) DEFAULT NULL,
  `render_md5` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rendered_content` longtext COLLATE utf8_unicode_ci,
  `rendered_time` datetime DEFAULT NULL,
  `activation_start` datetime DEFAULT NULL,
  `activation_end` datetime DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `is_empty` tinyint(4) NOT NULL DEFAULT '0',
  `inactive_elements` tinyint(4) DEFAULT '0',
  `int_links` tinyint(4) DEFAULT '0',
  `show_activation_info` tinyint(4) NOT NULL DEFAULT '0',
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `edit_lock_user` int(11) DEFAULT NULL,
  `edit_lock_ts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`,`parent_type`,`lang`),
  KEY `i3_idx` (`parent_id`,`parent_type`,`last_change`),
  FULLTEXT KEY `i1_idx` (`content`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_pc_usage`
--

DROP TABLE IF EXISTS `page_pc_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_pc_usage` (
  `pc_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pc_id` int(11) NOT NULL,
  `usage_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `usage_id` int(11) NOT NULL,
  `usage_hist_nr` int(11) NOT NULL,
  `usage_lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`pc_type`,`pc_id`,`usage_type`,`usage_id`,`usage_hist_nr`,`usage_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_qst_answer`
--

DROP TABLE IF EXISTS `page_qst_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_qst_answer` (
  `qst_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `try` tinyint(4) NOT NULL,
  `passed` tinyint(4) NOT NULL,
  `points` double NOT NULL,
  `unlocked` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`qst_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_question`
--

DROP TABLE IF EXISTS `page_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_question` (
  `page_parent_type` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL DEFAULT '0',
  `page_lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`page_parent_type`,`page_id`,`question_id`,`page_lang`),
  KEY `i1_idx` (`page_parent_type`,`page_id`,`page_lang`),
  KEY `i2_idx` (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_style_usage`
--

DROP TABLE IF EXISTS `page_style_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_style_usage` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `page_type` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `page_nr` int(11) NOT NULL,
  `template` tinyint(4) NOT NULL DEFAULT '0',
  `stype` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sname` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`page_id`,`page_type`,`page_lang`,`page_nr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_style_usage_seq`
--

DROP TABLE IF EXISTS `page_style_usage_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_style_usage_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=1165201 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `pdfgen_map`
--

DROP TABLE IF EXISTS `pdfgen_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfgen_map` (
  `map_id` int(11) NOT NULL,
  `service` varchar(255) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `preferred` varchar(255) NOT NULL,
  `selected` varchar(255) NOT NULL,
  PRIMARY KEY (`map_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `pdfgen_purposes`
--

DROP TABLE IF EXISTS `pdfgen_purposes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfgen_purposes` (
  `purpose_id` int(11) NOT NULL,
  `service` varchar(255) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  PRIMARY KEY (`purpose_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `pdfgen_renderer`
--

DROP TABLE IF EXISTS `pdfgen_renderer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfgen_renderer` (
  `renderer_id` int(11) NOT NULL,
  `renderer` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`renderer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pdfgen_renderer_avail`
--

DROP TABLE IF EXISTS `pdfgen_renderer_avail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfgen_renderer_avail` (
  `availability_id` int(11) NOT NULL,
  `service` varchar(255) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `renderer` varchar(255) NOT NULL,
  PRIMARY KEY (`availability_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `personal_clipboard`
--

DROP TABLE IF EXISTS `personal_clipboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_clipboard` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `type` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` char(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insert_time` datetime DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`item_id`,`type`),
  KEY `it_idx` (`item_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `personal_pc_clipboard`
--

DROP TABLE IF EXISTS `personal_pc_clipboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_pc_clipboard` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `content` longtext COLLATE utf8_unicode_ci,
  `insert_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`insert_time`,`order_nr`),
  KEY `i1_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pg_amd_page_list`
--

DROP TABLE IF EXISTS `pg_amd_page_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pg_amd_page_list` (
  `id` int(11) NOT NULL DEFAULT '0',
  `field_id` int(11) NOT NULL DEFAULT '0',
  `data` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pg_amd_page_list_seq`
--

DROP TABLE IF EXISTS `pg_amd_page_list_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pg_amd_page_list_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `preview_data`
--

DROP TABLE IF EXISTS `preview_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preview_data` (
  `obj_id` int(11) NOT NULL,
  `render_date` datetime NOT NULL,
  `render_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--


--


--


--
-- Table structure for table `qpl_a_cloze`
--

DROP TABLE IF EXISTS `qpl_a_cloze`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_cloze` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `shuffle` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `answertext` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` double NOT NULL DEFAULT '0',
  `aorder` smallint(6) NOT NULL DEFAULT '0',
  `gap_id` smallint(6) NOT NULL DEFAULT '0',
  `cloze_type` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `lowerlimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `upperlimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `gap_size` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `qpl_a_cloze_seq`
--

DROP TABLE IF EXISTS `qpl_a_cloze_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_cloze_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=290176 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_errortext`
--

DROP TABLE IF EXISTS `qpl_a_errortext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_errortext` (
  `answer_id` int(11) NOT NULL,
  `question_fi` int(11) NOT NULL,
  `text_wrong` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `text_correct` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` double NOT NULL DEFAULT '0',
  `sequence` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_errortext_seq`
--

DROP TABLE IF EXISTS `qpl_a_errortext_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_errortext_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=187 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_essay`
--

DROP TABLE IF EXISTS `qpl_a_essay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_essay` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) DEFAULT NULL,
  `answertext` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` double DEFAULT NULL,
  PRIMARY KEY (`answer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_essay_seq`
--

DROP TABLE IF EXISTS `qpl_a_essay_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_essay_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_imagemap`
--

DROP TABLE IF EXISTS `qpl_a_imagemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_imagemap` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `answertext` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` double NOT NULL DEFAULT '0',
  `aorder` smallint(6) NOT NULL DEFAULT '0',
  `coords` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `points_unchecked` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_imagemap_seq`
--

DROP TABLE IF EXISTS `qpl_a_imagemap_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_imagemap_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=7509 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_kprim`
--

DROP TABLE IF EXISTS `qpl_a_kprim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_kprim` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `answertext` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagefile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correctness` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_fi`,`position`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `qpl_a_matching`
--

DROP TABLE IF EXISTS `qpl_a_matching`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_matching` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `term_fi` int(11) NOT NULL DEFAULT '0',
  `points` double NOT NULL DEFAULT '0',
  `definition_fi` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`),
  KEY `i2_idx` (`term_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_matching_seq`
--

DROP TABLE IF EXISTS `qpl_a_matching_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_matching_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5572 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_mc`
--

DROP TABLE IF EXISTS `qpl_a_mc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_mc` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `answertext` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagefile` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` double NOT NULL DEFAULT '0',
  `points_unchecked` double NOT NULL DEFAULT '0',
  `aorder` smallint(6) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_mc_seq`
--

DROP TABLE IF EXISTS `qpl_a_mc_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_mc_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=14120 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_mdef`
--

DROP TABLE IF EXISTS `qpl_a_mdef`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_mdef` (
  `def_id` int(11) NOT NULL,
  `question_fi` int(11) NOT NULL,
  `definition` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ident` int(11) NOT NULL DEFAULT '0',
  `picture` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`def_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_mdef_seq`
--

DROP TABLE IF EXISTS `qpl_a_mdef_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_mdef_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5580 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_mterm`
--

DROP TABLE IF EXISTS `qpl_a_mterm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_mterm` (
  `term_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `term` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ident` int(11) DEFAULT NULL,
  PRIMARY KEY (`term_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_mterm_seq`
--

DROP TABLE IF EXISTS `qpl_a_mterm_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_mterm_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5457 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_ordering`
--

DROP TABLE IF EXISTS `qpl_a_ordering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_ordering` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `answertext` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `solution_key` smallint(6) NOT NULL DEFAULT '0',
  `random_id` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '0',
  `position` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_ordering_seq`
--

DROP TABLE IF EXISTS `qpl_a_ordering_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_ordering_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=351 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_sc`
--

DROP TABLE IF EXISTS `qpl_a_sc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_sc` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `answertext` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagefile` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` double NOT NULL DEFAULT '0',
  `aorder` smallint(6) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_sc_seq`
--

DROP TABLE IF EXISTS `qpl_a_sc_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_sc_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=292108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_textsubset`
--

DROP TABLE IF EXISTS `qpl_a_textsubset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_textsubset` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `answertext` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` double NOT NULL DEFAULT '0',
  `aorder` smallint(6) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_a_textsubset_seq`
--

DROP TABLE IF EXISTS `qpl_a_textsubset_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_a_textsubset_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_fb_generic`
--

DROP TABLE IF EXISTS `qpl_fb_generic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_fb_generic` (
  `feedback_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `correctness` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `feedback` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`feedback_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_fb_generic_seq`
--

DROP TABLE IF EXISTS `qpl_fb_generic_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_fb_generic_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=152761 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_fb_specific`
--

DROP TABLE IF EXISTS `qpl_fb_specific`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_fb_specific` (
  `feedback_id` int(11) NOT NULL,
  `question_fi` int(11) NOT NULL,
  `answer` int(11) NOT NULL,
  `tstamp` int(11) NOT NULL,
  `feedback` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`feedback_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_fb_specific_seq`
--

DROP TABLE IF EXISTS `qpl_fb_specific_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_fb_specific_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=250542 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `qpl_hint_tracking_seq`
--

DROP TABLE IF EXISTS `qpl_hint_tracking_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_hint_tracking_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_hints`
--

DROP TABLE IF EXISTS `qpl_hints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_hints` (
  `qht_hint_id` int(11) NOT NULL,
  `qht_question_fi` int(11) NOT NULL,
  `qht_hint_index` int(11) NOT NULL,
  `qht_hint_points` double NOT NULL DEFAULT '0',
  `qht_hint_text` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`qht_hint_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_hints_seq`
--

DROP TABLE IF EXISTS `qpl_hints_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_hints_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_num_range`
--

DROP TABLE IF EXISTS `qpl_num_range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_num_range` (
  `range_id` int(11) NOT NULL DEFAULT '0',
  `lowerlimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `upperlimit` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `points` double NOT NULL DEFAULT '0',
  `aorder` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`range_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_num_range_seq`
--

DROP TABLE IF EXISTS `qpl_num_range_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_num_range_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=641 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_cloze`
--

DROP TABLE IF EXISTS `qpl_qst_cloze`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_cloze` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `textgap_rating` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identical_scoring` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `fixed_textlen` int(11) DEFAULT NULL,
  `cloze_text` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_errortext`
--

DROP TABLE IF EXISTS `qpl_qst_errortext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_errortext` (
  `question_fi` int(11) NOT NULL,
  `errortext` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `textsize` double NOT NULL DEFAULT '100',
  `points_wrong` double NOT NULL DEFAULT '-1',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_essay`
--

DROP TABLE IF EXISTS `qpl_qst_essay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_essay` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `maxnumofchars` int(11) NOT NULL DEFAULT '0',
  `keywords` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `textgap_rating` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matchcondition` smallint(6) NOT NULL DEFAULT '0',
  `keyword_relation` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'any',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_fileupload`
--

DROP TABLE IF EXISTS `qpl_qst_fileupload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_fileupload` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `allowedextensions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maxsize` double DEFAULT NULL,
  `compl_by_submission` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `qpl_qst_horder`
--

DROP TABLE IF EXISTS `qpl_qst_horder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_horder` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `ordertext` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `textsize` double DEFAULT NULL,
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_imagemap`
--

DROP TABLE IF EXISTS `qpl_qst_imagemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_imagemap` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `image_file` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_multiple_choice` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_javaapplet`
--

DROP TABLE IF EXISTS `qpl_qst_javaapplet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_javaapplet` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `image_file` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_kprim`
--

DROP TABLE IF EXISTS `qpl_qst_kprim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_kprim` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `shuffle_answers` tinyint(4) NOT NULL DEFAULT '0',
  `answer_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'singleLine',
  `thumb_size` int(11) DEFAULT NULL,
  `opt_label` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'right/wrong',
  `custom_true` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_false` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score_partsol` tinyint(4) NOT NULL DEFAULT '0',
  `feedback_setting` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `qpl_qst_matching`
--

DROP TABLE IF EXISTS `qpl_qst_matching`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_matching` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `shuffle` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `matching_type` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `thumb_geometry` int(11) NOT NULL DEFAULT '100',
  `matching_mode` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_mc`
--

DROP TABLE IF EXISTS `qpl_qst_mc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_mc` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `shuffle` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `allow_images` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `resize_images` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `thumb_size` smallint(6) DEFAULT NULL,
  `feedback_setting` tinyint(4) NOT NULL DEFAULT '1',
  `selection_limit` int(11) DEFAULT NULL,
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_numeric`
--

DROP TABLE IF EXISTS `qpl_qst_numeric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_numeric` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `maxnumofchars` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_ordering`
--

DROP TABLE IF EXISTS `qpl_qst_ordering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_ordering` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `ordering_type` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `thumb_geometry` int(11) NOT NULL DEFAULT '100',
  `element_height` int(11) DEFAULT NULL,
  `scoring_type` mediumint(9) NOT NULL DEFAULT '0',
  `reduced_points` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_sc`
--

DROP TABLE IF EXISTS `qpl_qst_sc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_sc` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `shuffle` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `allow_images` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `resize_images` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `thumb_size` smallint(6) DEFAULT NULL,
  `feedback_setting` tinyint(4) DEFAULT '2',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_skl_assigns`
--

DROP TABLE IF EXISTS `qpl_qst_skl_assigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_skl_assigns` (
  `obj_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `skill_base_fi` int(11) NOT NULL DEFAULT '0',
  `skill_tref_fi` int(11) NOT NULL DEFAULT '0',
  `skill_points` int(11) NOT NULL DEFAULT '0',
  `eval_mode` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_fi`,`question_fi`,`skill_base_fi`,`skill_tref_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `qpl_qst_textsubset`
--

DROP TABLE IF EXISTS `qpl_qst_textsubset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_textsubset` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `textgap_rating` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correctanswers` int(11) DEFAULT '0',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_qst_type`
--

DROP TABLE IF EXISTS `qpl_qst_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_qst_type` (
  `question_type_id` int(11) NOT NULL DEFAULT '0',
  `type_tag` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugin` tinyint(4) NOT NULL DEFAULT '0',
  `plugin_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`question_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_questionpool`
--

DROP TABLE IF EXISTS `qpl_questionpool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_questionpool` (
  `id_questionpool` int(11) NOT NULL DEFAULT '0',
  `obj_fi` int(11) NOT NULL DEFAULT '0',
  `isonline` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `questioncount` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `show_taxonomies` tinyint(4) NOT NULL DEFAULT '0',
  `nav_taxonomy` int(11) DEFAULT NULL,
  `skill_service` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_questionpool`),
  KEY `i1_idx` (`obj_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_questionpool_seq`
--

DROP TABLE IF EXISTS `qpl_questionpool_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_questionpool_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1530 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_questions`
--

DROP TABLE IF EXISTS `qpl_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_questions` (
  `question_id` int(11) NOT NULL DEFAULT '0',
  `question_type_fi` int(11) NOT NULL DEFAULT '0',
  `obj_fi` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner` int(11) NOT NULL DEFAULT '0',
  `working_time` varchar(8) COLLATE utf8_unicode_ci DEFAULT '00:00:00',
  `points` double DEFAULT NULL,
  `complete` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `original_id` int(11) DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `nr_of_tries` int(11) NOT NULL DEFAULT '0',
  `question_text` longtext COLLATE utf8_unicode_ci,
  `add_cont_edit_mode` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`question_id`),
  KEY `i1_idx` (`question_type_fi`),
  KEY `i2_idx` (`original_id`),
  KEY `i3_idx` (`obj_fi`),
  KEY `i4_idx` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_questions_seq`
--

DROP TABLE IF EXISTS `qpl_questions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_questions_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=94444 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_sol_sug`
--

DROP TABLE IF EXISTS `qpl_sol_sug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_sol_sug` (
  `suggested_solution_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `internal_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `import_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subquestion_index` int(11) NOT NULL DEFAULT '0',
  `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`suggested_solution_id`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qpl_sol_sug_seq`
--

DROP TABLE IF EXISTS `qpl_sol_sug_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qpl_sol_sug_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=490 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_fa`
--

DROP TABLE IF EXISTS `rbac_fa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_fa` (
  `rol_id` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `assign` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protected` char(1) COLLATE utf8_unicode_ci DEFAULT 'n',
  `blocked` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rol_id`,`parent`),
  KEY `i1_idx` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_log`
--

DROP TABLE IF EXISTS `rbac_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_log` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`log_id`),
  KEY `i1_idx` (`ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_log_seq`
--

DROP TABLE IF EXISTS `rbac_log_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_log_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=16185 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_operations`
--

DROP TABLE IF EXISTS `rbac_operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_operations` (
  `ops_id` int(11) NOT NULL DEFAULT '0',
  `operation` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_order` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`ops_id`),
  KEY `i1_idx` (`operation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_operations_seq`
--

DROP TABLE IF EXISTS `rbac_operations_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_operations_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_pa`
--

DROP TABLE IF EXISTS `rbac_pa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_pa` (
  `rol_id` int(11) NOT NULL DEFAULT '0',
  `ops_id` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rol_id`,`ref_id`),
  KEY `i1_idx` (`ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_ta`
--

DROP TABLE IF EXISTS `rbac_ta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_ta` (
  `typ_id` int(11) NOT NULL DEFAULT '0',
  `ops_id` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`typ_id`,`ops_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_templates`
--

DROP TABLE IF EXISTS `rbac_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_templates` (
  `rol_id` int(11) NOT NULL DEFAULT '0',
  `type` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ops_id` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rol_id`,`parent`,`type`,`ops_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rbac_ua`
--

DROP TABLE IF EXISTS `rbac_ua`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rbac_ua` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `rol_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usr_id`,`rol_id`),
  KEY `i1_idx` (`usr_id`),
  KEY `i2_idx` (`rol_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `read_event`
--

DROP TABLE IF EXISTS `read_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `read_event` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `last_access` int(11) DEFAULT NULL,
  `read_count` int(11) NOT NULL DEFAULT '0',
  `spent_seconds` int(11) NOT NULL DEFAULT '0',
  `first_access` datetime DEFAULT NULL,
  `childs_read_count` int(11) NOT NULL DEFAULT '0',
  `childs_spent_seconds` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`,`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reg_access_limit`
--

DROP TABLE IF EXISTS `reg_access_limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_access_limit` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `limit_absolute` int(11) DEFAULT NULL,
  `limit_relative_d` int(11) DEFAULT NULL,
  `limit_relative_m` int(11) DEFAULT NULL,
  `limit_relative_y` int(11) DEFAULT NULL,
  `limit_mode` char(16) COLLATE utf8_unicode_ci DEFAULT 'absolute',
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reg_er_assignments`
--

DROP TABLE IF EXISTS `reg_er_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_er_assignments` (
  `assignment_id` int(11) NOT NULL DEFAULT '0',
  `domain` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`assignment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reg_er_assignments_seq`
--

DROP TABLE IF EXISTS `reg_er_assignments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_er_assignments_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reg_registration_codes`
--

DROP TABLE IF EXISTS `reg_registration_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_registration_codes` (
  `code_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) DEFAULT '0',
  `generated_on` int(11) DEFAULT '0',
  `used` int(11) NOT NULL DEFAULT '0',
  `role_local` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alimit` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alimitdt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reg_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `ext_enabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code_id`),
  KEY `i1_idx` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reg_registration_codes_seq`
--

DROP TABLE IF EXISTS `reg_registration_codes_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_registration_codes_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=237 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `rep_robj_xdht_partic`
--

DROP TABLE IF EXISTS `rep_robj_xdht_partic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xdht_partic` (
  `id` bigint(20) NOT NULL,
  `training_obj_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `status` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_status` datetime DEFAULT NULL,
  `last_access` datetime DEFAULT NULL,
  `created_usr_id` bigint(20) DEFAULT NULL,
  `updated_usr_id` bigint(20) DEFAULT NULL,
  `full_name` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_robj_xdht_partic_seq`
--

DROP TABLE IF EXISTS `rep_robj_xdht_partic_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xdht_partic_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_robj_xdht_settings`
--

DROP TABLE IF EXISTS `rep_robj_xdht_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xdht_settings` (
  `id` bigint(20) NOT NULL,
  `dhbw_training_object_id` int(11) NOT NULL,
  `question_pool_id` int(11) DEFAULT NULL,
  `is_online` tinyint(4) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `installation_key` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `log` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_robj_xdht_settings_seq`
--

DROP TABLE IF EXISTS `rep_robj_xdht_settings_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xdht_settings_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_robj_xpdl_adm_set`
--

DROP TABLE IF EXISTS `rep_robj_xpdl_adm_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xpdl_adm_set` (
  `epkey` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `epvalue` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`epkey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_robj_xpdl_data`
--

DROP TABLE IF EXISTS `rep_robj_xpdl_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xpdl_data` (
  `id` int(11) NOT NULL,
  `is_online` int(11) DEFAULT NULL,
  `epadl_id` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_controls` tinyint(1) DEFAULT NULL,
  `line_numbers` tinyint(1) DEFAULT NULL,
  `show_colors` tinyint(1) DEFAULT NULL,
  `show_chat` tinyint(1) DEFAULT NULL,
  `monospace_font` tinyint(1) DEFAULT NULL,
  `show_style` tinyint(1) DEFAULT NULL,
  `show_list` tinyint(1) DEFAULT NULL,
  `show_redo` tinyint(1) DEFAULT NULL,
  `show_coloring` tinyint(1) DEFAULT NULL,
  `show_heading` tinyint(1) DEFAULT NULL,
  `show_import_export` tinyint(1) DEFAULT NULL,
  `show_timeline` tinyint(1) DEFAULT NULL,
  `old_pad` tinyint(1) DEFAULT NULL,
  `read_only_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `read_only` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_robj_xtdo_data`
--

DROP TABLE IF EXISTS `rep_robj_xtdo_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xtdo_data` (
  `id` bigint(20) NOT NULL,
  `is_online` tinyint(4) DEFAULT NULL,
  `are_finished_shown` tinyint(4) DEFAULT '0',
  `before_startdate_shown` tinyint(4) DEFAULT '0',
  `collectlist` bigint(20) DEFAULT '0',
  `namen` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `get_collect` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_startdate` tinyint(4) DEFAULT '0',
  `show_createdby` tinyint(4) DEFAULT '0',
  `show_updatedby` tinyint(4) DEFAULT '0',
  `show_percent_bar` tinyint(4) DEFAULT '0',
  `show_edit_status_button` tinyint(4) DEFAULT '0',
  `edit_status_permission` tinyint(4) DEFAULT '0',
  `enddate_warning` tinyint(4) DEFAULT '1',
  `enddate_cursive` tinyint(4) DEFAULT '0',
  `enddate_fat` tinyint(4) DEFAULT '0',
  `enddate_color` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `path` text COLLATE utf8_unicode_ci,
  `status_position` int(11) DEFAULT '0',
  `show_description` int(11) DEFAULT '1',
  `show_enddate` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_robj_xtdo_milsto`
--

DROP TABLE IF EXISTS `rep_robj_xtdo_milsto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xtdo_milsto` (
  `objectid` bigint(20) DEFAULT NULL,
  `milestone` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET latin1,
  `progress` tinyint(4) DEFAULT '0',
  `created_by` int(200) DEFAULT NULL,
  `updated_by` int(200) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rep_robj_xtdo_tasks`
--

DROP TABLE IF EXISTS `rep_robj_xtdo_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rep_robj_xtdo_tasks` (
  `objectid` bigint(20) DEFAULT NULL,
  `tasks` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `description` longtext CHARACTER SET latin1,
  `edit_status` tinyint(4) DEFAULT '0',
  `created_by` int(200) DEFAULT NULL,
  `updated_by` int(200) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `milestone_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--
-- Table structure for table `role_data`
--

DROP TABLE IF EXISTS `role_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_data` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `allow_register` tinyint(4) NOT NULL DEFAULT '0',
  `assign_users` tinyint(4) DEFAULT '0',
  `auth_mode` char(16) COLLATE utf8_unicode_ci DEFAULT 'default',
  `disk_quota` bigint(20) NOT NULL DEFAULT '0',
  `wsp_disk_quota` int(11) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  KEY `i1_idx` (`auth_mode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_desktop_items`
--

DROP TABLE IF EXISTS `role_desktop_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_desktop_items` (
  `role_item_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `item_type` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`role_item_id`),
  KEY `i1_idx` (`role_item_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_desktop_items_seq`
--

DROP TABLE IF EXISTS `role_desktop_items_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_desktop_items_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `sahs_sc13_sco`
--

DROP TABLE IF EXISTS `sahs_sc13_sco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sahs_sc13_sco` (
  `id` int(11) NOT NULL,
  `hide_obj_page` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `sahs_sc13_seq_item`
--

DROP TABLE IF EXISTS `sahs_sc13_seq_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sahs_sc13_seq_item` (
  `importid` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seqnodeid` int(11) NOT NULL DEFAULT '0',
  `sahs_sc13_tree_node_id` int(11) NOT NULL DEFAULT '0',
  `sequencingid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nocopy` tinyint(4) DEFAULT NULL,
  `nodelete` tinyint(4) DEFAULT NULL,
  `nomove` tinyint(4) DEFAULT NULL,
  `seqxml` longtext COLLATE utf8_unicode_ci,
  `rootlevel` tinyint(4) NOT NULL DEFAULT '0',
  `importseqxml` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`sahs_sc13_tree_node_id`,`rootlevel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--
-- Table structure for table `sahs_sc13_seq_templts`
--

DROP TABLE IF EXISTS `sahs_sc13_seq_templts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sahs_sc13_seq_templts` (
  `identifier` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filename` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sahs_sc13_seq_templts_seq`
--

DROP TABLE IF EXISTS `sahs_sc13_seq_templts_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sahs_sc13_seq_templts_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `sahs_sc13_tree`
--

DROP TABLE IF EXISTS `sahs_sc13_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sahs_sc13_tree` (
  `slm_id` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`child`,`parent`,`slm_id`),
  KEY `i1_idx` (`child`),
  KEY `i2_idx` (`parent`),
  KEY `i3_idx` (`slm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sahs_sc13_tree_node`
--

DROP TABLE IF EXISTS `sahs_sc13_tree_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sahs_sc13_tree_node` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slm_id` int(11) NOT NULL DEFAULT '0',
  `import_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`obj_id`),
  KEY `i1_idx` (`slm_id`),
  KEY `i2_idx` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sahs_sc13_tree_node_seq`
--

DROP TABLE IF EXISTS `sahs_sc13_tree_node_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sahs_sc13_tree_node_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--


--


--


--


--


--


--


--


--
-- Table structure for table `search_command_queue`
--

DROP TABLE IF EXISTS `search_command_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_command_queue` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `obj_type` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sub_id` int(11) NOT NULL DEFAULT '0',
  `sub_type` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `command` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `finished` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`,`obj_type`,`sub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `service_class`
--

DROP TABLE IF EXISTS `service_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_class` (
  `class` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `service` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dir` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Class information of ILIAS Modules';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'common',
  `keyword` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`module`,`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings_deactivated_s`
--

DROP TABLE IF EXISTS `settings_deactivated_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings_deactivated_s` (
  `skin` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `style` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  PRIMARY KEY (`skin`,`style`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `skl_assigned_material`
--

DROP TABLE IF EXISTS `skl_assigned_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_assigned_material` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `top_skill_id` int(11) NOT NULL DEFAULT '0',
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `level_id` int(11) NOT NULL DEFAULT '0',
  `wsp_id` int(11) NOT NULL DEFAULT '0',
  `tref_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`top_skill_id`,`tref_id`,`skill_id`,`level_id`,`wsp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_level`
--

DROP TABLE IF EXISTS `skl_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_level` (
  `id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `nr` smallint(6) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `trigger_ref_id` int(11) NOT NULL DEFAULT '0',
  `trigger_obj_id` int(11) NOT NULL DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `import_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_level_seq`
--

DROP TABLE IF EXISTS `skl_level_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_level_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=233 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_personal_skill`
--

DROP TABLE IF EXISTS `skl_personal_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_personal_skill` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `skill_node_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`skill_node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_profile`
--

DROP TABLE IF EXISTS `skl_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_profile` (
  `id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_profile_level`
--

DROP TABLE IF EXISTS `skl_profile_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_profile_level` (
  `profile_id` int(11) NOT NULL,
  `base_skill_id` int(11) NOT NULL,
  `tref_id` int(11) NOT NULL DEFAULT '0',
  `level_id` int(11) NOT NULL,
  PRIMARY KEY (`profile_id`,`tref_id`,`base_skill_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_profile_seq`
--

DROP TABLE IF EXISTS `skl_profile_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_profile_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `skl_self_eval_level`
--

DROP TABLE IF EXISTS `skl_self_eval_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_self_eval_level` (
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `level_id` int(11) NOT NULL DEFAULT '0',
  `tref_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `top_skill_id` int(11) NOT NULL DEFAULT '0',
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`top_skill_id`,`tref_id`,`skill_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `skl_templ_ref`
--

DROP TABLE IF EXISTS `skl_templ_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_templ_ref` (
  `skl_node_id` int(11) NOT NULL DEFAULT '0',
  `templ_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`skl_node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_tree`
--

DROP TABLE IF EXISTS `skl_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_tree` (
  `skl_tree_id` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`skl_tree_id`,`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_tree_node`
--

DROP TABLE IF EXISTS `skl_tree_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_tree_node` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `self_eval` tinyint(4) NOT NULL DEFAULT '0',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `creation_date` datetime DEFAULT NULL,
  `import_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_tree_node_seq`
--

DROP TABLE IF EXISTS `skl_tree_node_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_tree_node_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_usage`
--

DROP TABLE IF EXISTS `skl_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_usage` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `tref_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`,`skill_id`,`tref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_user_has_level`
--

DROP TABLE IF EXISTS `skl_user_has_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_user_has_level` (
  `level_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_date` datetime NOT NULL,
  `skill_id` int(11) NOT NULL,
  `trigger_ref_id` int(11) NOT NULL,
  `trigger_obj_id` int(11) NOT NULL,
  `trigger_title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tref_id` int(11) NOT NULL DEFAULT '0',
  `trigger_obj_type` varchar(4) COLLATE utf8_unicode_ci DEFAULT 'crs',
  `self_eval` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`level_id`,`user_id`,`trigger_obj_id`,`tref_id`,`self_eval`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skl_user_skill_level`
--

DROP TABLE IF EXISTS `skl_user_skill_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skl_user_skill_level` (
  `level_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_date` datetime NOT NULL,
  `skill_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '0',
  `trigger_ref_id` int(11) NOT NULL,
  `trigger_obj_id` int(11) NOT NULL,
  `trigger_title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tref_id` int(11) NOT NULL DEFAULT '0',
  `trigger_obj_type` varchar(4) COLLATE utf8_unicode_ci DEFAULT 'crs',
  `self_eval` tinyint(4) NOT NULL DEFAULT '0',
  `unique_identifier` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`skill_id`,`tref_id`,`user_id`,`status_date`,`status`,`trigger_obj_id`,`self_eval`),
  KEY `isk_idx` (`skill_id`),
  KEY `ilv_idx` (`level_id`),
  KEY `ius_idx` (`user_id`),
  KEY `isd_idx` (`status_date`),
  KEY `ist_idx` (`status`),
  KEY `ivl_idx` (`valid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sty_media_query`
--

DROP TABLE IF EXISTS `sty_media_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sty_media_query` (
  `id` int(11) NOT NULL DEFAULT '0',
  `style_id` int(11) NOT NULL DEFAULT '0',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  `mquery` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sty_media_query_seq`
--

DROP TABLE IF EXISTS `sty_media_query_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sty_media_query_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_char`
--

DROP TABLE IF EXISTS `style_char`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_char` (
  `style_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `characteristic` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `hide` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`style_id`,`type`,`characteristic`),
  KEY `i1_idx` (`style_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_color`
--

DROP TABLE IF EXISTS `style_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_color` (
  `style_id` int(11) NOT NULL,
  `color_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '.',
  `color_code` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`style_id`,`color_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_data`
--

DROP TABLE IF EXISTS `style_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_data` (
  `id` int(11) NOT NULL DEFAULT '0',
  `uptodate` tinyint(4) DEFAULT '0',
  `standard` tinyint(4) DEFAULT '0',
  `category` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_folder_styles`
--

DROP TABLE IF EXISTS `style_folder_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_folder_styles` (
  `folder_id` int(11) NOT NULL DEFAULT '0',
  `style_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`folder_id`,`style_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_parameter`
--

DROP TABLE IF EXISTS `style_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_parameter` (
  `id` int(11) NOT NULL DEFAULT '0',
  `style_id` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameter` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mq_id` int(11) NOT NULL DEFAULT '0',
  `custom` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`style_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_parameter_seq`
--

DROP TABLE IF EXISTS `style_parameter_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_parameter_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=363649 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_setting`
--

DROP TABLE IF EXISTS `style_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_setting` (
  `style_id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`style_id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_template`
--

DROP TABLE IF EXISTS `style_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_template` (
  `id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `preview` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temp_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_template_class`
--

DROP TABLE IF EXISTS `style_template_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_template_class` (
  `template_id` int(11) NOT NULL,
  `class_type` char(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class` char(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`template_id`,`class_type`,`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_template_seq`
--

DROP TABLE IF EXISTS `style_template_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_template_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1133 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `style_usage`
--

DROP TABLE IF EXISTS `style_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style_usage` (
  `obj_id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_360_appr`
--

DROP TABLE IF EXISTS `svy_360_appr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_360_appr` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `has_closed` int(11) DEFAULT '0',
  PRIMARY KEY (`obj_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_360_rater`
--

DROP TABLE IF EXISTS `svy_360_rater`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_360_rater` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `appr_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `anonymous_id` int(11) NOT NULL DEFAULT '0',
  `mail_sent` int(11) DEFAULT '0',
  PRIMARY KEY (`obj_id`,`appr_id`,`user_id`,`anonymous_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_anonymous`
--

DROP TABLE IF EXISTS `svy_anonymous`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_anonymous` (
  `anonymous_id` int(11) NOT NULL DEFAULT '0',
  `survey_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey_fi` int(11) NOT NULL DEFAULT '0',
  `user_key` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `externaldata` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sent` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`anonymous_id`),
  KEY `i1_idx` (`survey_key`,`survey_fi`),
  KEY `i2_idx` (`survey_fi`),
  KEY `i3_idx` (`sent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_anonymous_seq`
--

DROP TABLE IF EXISTS `svy_anonymous_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_anonymous_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2497 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_answer`
--

DROP TABLE IF EXISTS `svy_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_answer` (
  `answer_id` int(11) NOT NULL DEFAULT '0',
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `value` double DEFAULT NULL,
  `textanswer` longtext COLLATE utf8_unicode_ci,
  `rowvalue` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `i1_idx` (`question_fi`),
  KEY `i2_idx` (`active_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_answer_seq`
--

DROP TABLE IF EXISTS `svy_answer_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_answer_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=62435 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_category`
--

DROP TABLE IF EXISTS `svy_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_category` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defaultvalue` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `owner_fi` int(11) NOT NULL DEFAULT '0',
  `neutral` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  KEY `i1_idx` (`owner_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_category_seq`
--

DROP TABLE IF EXISTS `svy_category_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_category_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2038 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_constraint`
--

DROP TABLE IF EXISTS `svy_constraint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_constraint` (
  `constraint_id` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `relation_fi` int(11) NOT NULL DEFAULT '0',
  `value` double NOT NULL DEFAULT '0',
  `conjunction` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`constraint_id`),
  KEY `i1_idx` (`question_fi`),
  KEY `i2_idx` (`relation_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_constraint_seq`
--

DROP TABLE IF EXISTS `svy_constraint_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_constraint_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1012 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_finished`
--

DROP TABLE IF EXISTS `svy_finished`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_finished` (
  `finished_id` int(11) NOT NULL DEFAULT '0',
  `survey_fi` int(11) NOT NULL DEFAULT '0',
  `user_fi` int(11) NOT NULL DEFAULT '0',
  `anonymous_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `lastpage` int(11) NOT NULL DEFAULT '0',
  `appr_id` int(11) DEFAULT '0',
  PRIMARY KEY (`finished_id`),
  KEY `i1_idx` (`survey_fi`),
  KEY `i2_idx` (`user_fi`),
  KEY `i3_idx` (`anonymous_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_finished_seq`
--

DROP TABLE IF EXISTS `svy_finished_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_finished_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2136 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_inv_usr`
--

DROP TABLE IF EXISTS `svy_inv_usr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_inv_usr` (
  `invited_user_id` int(11) NOT NULL DEFAULT '0',
  `survey_fi` int(11) NOT NULL DEFAULT '0',
  `user_fi` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invited_user_id`),
  KEY `i1_idx` (`survey_fi`),
  KEY `i2_idx` (`user_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_inv_usr_seq`
--

DROP TABLE IF EXISTS `svy_inv_usr_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_inv_usr_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=963 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `svy_phrase`
--

DROP TABLE IF EXISTS `svy_phrase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_phrase` (
  `phrase_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defaultvalue` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `owner_fi` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`phrase_id`),
  KEY `i1_idx` (`owner_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_phrase_cat`
--

DROP TABLE IF EXISTS `svy_phrase_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_phrase_cat` (
  `phrase_category_id` int(11) NOT NULL DEFAULT '0',
  `phrase_fi` int(11) NOT NULL DEFAULT '0',
  `category_fi` int(11) NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL DEFAULT '0',
  `other` smallint(6) NOT NULL DEFAULT '0',
  `scale` int(11) DEFAULT NULL,
  PRIMARY KEY (`phrase_category_id`),
  KEY `i1_idx` (`phrase_fi`),
  KEY `i2_idx` (`category_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_phrase_cat_seq`
--

DROP TABLE IF EXISTS `svy_phrase_cat_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_phrase_cat_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_phrase_seq`
--

DROP TABLE IF EXISTS `svy_phrase_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_phrase_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qblk`
--

DROP TABLE IF EXISTS `svy_qblk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qblk` (
  `questionblock_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_questiontext` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `owner_fi` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `show_blocktitle` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`questionblock_id`),
  KEY `i1_idx` (`owner_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qblk_qst`
--

DROP TABLE IF EXISTS `svy_qblk_qst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qblk_qst` (
  `qblk_qst_id` int(11) NOT NULL DEFAULT '0',
  `survey_fi` int(11) NOT NULL DEFAULT '0',
  `questionblock_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`qblk_qst_id`),
  KEY `i1_idx` (`survey_fi`),
  KEY `i2_idx` (`questionblock_fi`),
  KEY `i3_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qblk_qst_seq`
--

DROP TABLE IF EXISTS `svy_qblk_qst_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qblk_qst_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1027 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qblk_seq`
--

DROP TABLE IF EXISTS `svy_qblk_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qblk_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=291 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qpl`
--

DROP TABLE IF EXISTS `svy_qpl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qpl` (
  `id_questionpool` int(11) NOT NULL DEFAULT '0',
  `obj_fi` int(11) NOT NULL DEFAULT '0',
  `isonline` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_questionpool`),
  KEY `i1_idx` (`obj_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qpl_seq`
--

DROP TABLE IF EXISTS `svy_qpl_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qpl_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_constraint`
--

DROP TABLE IF EXISTS `svy_qst_constraint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_constraint` (
  `question_constraint_id` int(11) NOT NULL DEFAULT '0',
  `survey_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `constraint_fi` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_constraint_id`),
  KEY `i1_idx` (`survey_fi`),
  KEY `i2_idx` (`question_fi`),
  KEY `i3_idx` (`constraint_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_constraint_seq`
--

DROP TABLE IF EXISTS `svy_qst_constraint_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_constraint_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1191 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_matrix`
--

DROP TABLE IF EXISTS `svy_qst_matrix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_matrix` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `subtype` int(11) NOT NULL DEFAULT '0',
  `column_separators` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `row_separators` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `neutral_column_separator` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `column_placeholders` int(11) NOT NULL DEFAULT '0',
  `legend` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `singleline_row_caption` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `repeat_column_header` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `column_header_position` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `random_rows` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `column_order` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `column_images` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `row_images` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `bipolar_adjective1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bipolar_adjective2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `layout` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_matrixrows`
--

DROP TABLE IF EXISTS `svy_qst_matrixrows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_matrixrows` (
  `id_svy_qst_matrixrows` int(11) NOT NULL DEFAULT '0',
  `title` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `other` tinyint(4) NOT NULL DEFAULT '0',
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_svy_qst_matrixrows`),
  KEY `i1_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_matrixrows_seq`
--

DROP TABLE IF EXISTS `svy_qst_matrixrows_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_matrixrows_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=5378 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_mc`
--

DROP TABLE IF EXISTS `svy_qst_mc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_mc` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `orientation` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `use_min_answers` tinyint(4) NOT NULL DEFAULT '0',
  `nr_min_answers` smallint(6) DEFAULT NULL,
  `nr_max_answers` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `svy_qst_oblig`
--

DROP TABLE IF EXISTS `svy_qst_oblig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_oblig` (
  `question_obligatory_id` int(11) NOT NULL DEFAULT '0',
  `survey_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `obligatory` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_obligatory_id`),
  KEY `i1_idx` (`survey_fi`,`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the obligatory state of questions in a survey';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_oblig_seq`
--

DROP TABLE IF EXISTS `svy_qst_oblig_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_oblig_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2216 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_sc`
--

DROP TABLE IF EXISTS `svy_qst_sc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_sc` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `orientation` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qst_text`
--

DROP TABLE IF EXISTS `svy_qst_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qst_text` (
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `maxchars` int(11) DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '50',
  `height` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qtype`
--

DROP TABLE IF EXISTS `svy_qtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qtype` (
  `questiontype_id` int(11) NOT NULL DEFAULT '0',
  `type_tag` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugin` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`questiontype_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_qtype_seq`
--

DROP TABLE IF EXISTS `svy_qtype_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_qtype_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `svy_question`
--

DROP TABLE IF EXISTS `svy_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_question` (
  `question_id` int(11) NOT NULL DEFAULT '0',
  `questiontype_fi` int(11) NOT NULL DEFAULT '0',
  `obj_fi` int(11) NOT NULL DEFAULT '0',
  `owner_fi` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `obligatory` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `complete` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `created` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_id` int(11) DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `questiontext` longtext COLLATE utf8_unicode_ci,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`question_id`),
  KEY `i1_idx` (`obj_fi`),
  KEY `i2_idx` (`owner_fi`),
  FULLTEXT KEY `i3_idx` (`title`,`description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_question_seq`
--

DROP TABLE IF EXISTS `svy_question_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_question_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2898 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_relation`
--

DROP TABLE IF EXISTS `svy_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_relation` (
  `relation_id` int(11) NOT NULL DEFAULT '0',
  `longname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shortname` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`relation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_relation_seq`
--

DROP TABLE IF EXISTS `svy_relation_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_relation_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `svy_skill_threshold`
--

DROP TABLE IF EXISTS `svy_skill_threshold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_skill_threshold` (
  `survey_id` int(11) NOT NULL DEFAULT '0',
  `base_skill_id` int(11) NOT NULL,
  `tref_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `threshold` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`survey_id`,`base_skill_id`,`tref_id`,`level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_svy`
--

DROP TABLE IF EXISTS `svy_svy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_svy` (
  `survey_id` int(11) NOT NULL DEFAULT '0',
  `obj_fi` int(11) NOT NULL DEFAULT '0',
  `author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction` longtext COLLATE utf8_unicode_ci,
  `outro` longtext COLLATE utf8_unicode_ci,
  `status` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `evaluation_access` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `invitation` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `invitation_mode` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `complete` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `anonymize` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `show_question_titles` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `mailnotification` tinyint(4) DEFAULT NULL,
  `startdate` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enddate` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mailaddresses` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mailparticipantdata` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `pool_usage` tinyint(4) DEFAULT NULL,
  `mode_360` tinyint(4) NOT NULL DEFAULT '0',
  `mode_360_self_eval` tinyint(4) NOT NULL DEFAULT '0',
  `mode_360_self_rate` tinyint(4) NOT NULL DEFAULT '0',
  `mode_360_self_appr` tinyint(4) NOT NULL DEFAULT '0',
  `mode_360_results` tinyint(4) NOT NULL DEFAULT '0',
  `mode_360_skill_service` tinyint(4) NOT NULL DEFAULT '0',
  `reminder_status` tinyint(4) NOT NULL DEFAULT '0',
  `reminder_start` datetime DEFAULT NULL,
  `reminder_end` datetime DEFAULT NULL,
  `reminder_frequency` smallint(6) NOT NULL DEFAULT '0',
  `reminder_target` tinyint(4) NOT NULL DEFAULT '0',
  `tutor_ntf_status` tinyint(4) NOT NULL DEFAULT '0',
  `tutor_ntf_reci` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tutor_ntf_target` tinyint(4) NOT NULL DEFAULT '0',
  `reminder_last_sent` datetime DEFAULT NULL,
  `own_results_view` tinyint(4) DEFAULT '0',
  `own_results_mail` tinyint(4) DEFAULT '0',
  `confirmation_mail` tinyint(4) DEFAULT NULL,
  `anon_user_list` tinyint(4) DEFAULT '0',
  `reminder_tmpl` int(11) DEFAULT NULL,
  PRIMARY KEY (`survey_id`),
  KEY `i1_idx` (`obj_fi`),
  FULLTEXT KEY `i2_idx` (`introduction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_svy_qst`
--

DROP TABLE IF EXISTS `svy_svy_qst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_svy_qst` (
  `survey_question_id` int(11) NOT NULL DEFAULT '0',
  `survey_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL DEFAULT '0',
  `heading` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`survey_question_id`),
  KEY `i1_idx` (`survey_fi`),
  KEY `i2_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_svy_qst_seq`
--

DROP TABLE IF EXISTS `svy_svy_qst_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_svy_qst_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=17472 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_svy_seq`
--

DROP TABLE IF EXISTS `svy_svy_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_svy_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=117 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_times`
--

DROP TABLE IF EXISTS `svy_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_times` (
  `id` int(11) NOT NULL,
  `finished_fi` int(11) NOT NULL,
  `entered_page` int(11) DEFAULT NULL,
  `left_page` int(11) DEFAULT NULL,
  `first_question` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`finished_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_times_seq`
--

DROP TABLE IF EXISTS `svy_times_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_times_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=22249 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_variable`
--

DROP TABLE IF EXISTS `svy_variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_variable` (
  `variable_id` int(11) NOT NULL DEFAULT '0',
  `category_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `value1` double DEFAULT NULL,
  `value2` double DEFAULT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `other` smallint(6) NOT NULL DEFAULT '0',
  `scale` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `i1_idx` (`category_fi`),
  KEY `i2_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svy_variable_seq`
--

DROP TABLE IF EXISTS `svy_variable_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svy_variable_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=16779 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sysc_groups`
--

DROP TABLE IF EXISTS `sysc_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sysc_groups` (
  `id` int(11) NOT NULL,
  `component` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `sysc_tasks`
--

DROP TABLE IF EXISTS `sysc_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sysc_tasks` (
  `id` int(11) NOT NULL,
  `grp_id` int(11) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `identifier` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sysc_tasks_seq`
--

DROP TABLE IF EXISTS `sysc_tasks_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sysc_tasks_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `table_properties`
--

DROP TABLE IF EXISTS `table_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_properties` (
  `table_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `property` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(4000) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  PRIMARY KEY (`table_id`,`user_id`,`property`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `table_templates`
--

DROP TABLE IF EXISTS `table_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_templates` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `context` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`name`,`user_id`,`context`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tax_data`
--

DROP TABLE IF EXISTS `tax_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_data` (
  `id` int(11) NOT NULL DEFAULT '0',
  `sorting_mode` int(11) NOT NULL DEFAULT '0',
  `item_sorting` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tax_node`
--

DROP TABLE IF EXISTS `tax_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_node` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `tax_id` int(11) NOT NULL DEFAULT '0',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tax_node_assignment`
--

DROP TABLE IF EXISTS `tax_node_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_node_assignment` (
  `node_id` int(11) NOT NULL DEFAULT '0',
  `component` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `item_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `tax_id` int(11) NOT NULL DEFAULT '0',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`node_id`,`component`,`obj_id`,`item_type`,`item_id`),
  KEY `i1_idx` (`component`,`item_type`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tax_node_seq`
--

DROP TABLE IF EXISTS `tax_node_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_node_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=33104 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tax_tree`
--

DROP TABLE IF EXISTS `tax_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_tree` (
  `tax_tree_id` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tax_tree_id`,`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tax_usage`
--

DROP TABLE IF EXISTS `tax_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_usage` (
  `tax_id` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tax_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tos_acceptance_track`
--

DROP TABLE IF EXISTS `tos_acceptance_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tos_acceptance_track` (
  `tosv_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `ts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tosv_id`,`usr_id`,`ts`),
  KEY `i1_idx` (`usr_id`,`ts`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `tos_versions`
--

DROP TABLE IF EXISTS `tos_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tos_versions` (
  `id` int(11) NOT NULL DEFAULT '0',
  `lng` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci,
  `hash` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ts` int(11) NOT NULL DEFAULT '0',
  `src_type` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`hash`,`lng`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tos_versions_seq`
--

DROP TABLE IF EXISTS `tos_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tos_versions_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tree`
--

DROP TABLE IF EXISTS `tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tree` (
  `tree` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  `path` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`child`),
  KEY `i1_idx` (`child`),
  KEY `i2_idx` (`parent`),
  KEY `i3_idx` (`tree`),
  KEY `i4_idx` (`lft`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tree_workspace`
--

DROP TABLE IF EXISTS `tree_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tree_workspace` (
  `tree` int(11) NOT NULL DEFAULT '0',
  `child` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`child`),
  KEY `i2_idx` (`parent`),
  KEY `i3_idx` (`tree`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_active`
--

DROP TABLE IF EXISTS `tst_active`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_active` (
  `active_id` int(11) NOT NULL DEFAULT '0',
  `user_fi` int(11) NOT NULL DEFAULT '0',
  `anonymous_id` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test_fi` int(11) NOT NULL DEFAULT '0',
  `tries` int(11) NOT NULL DEFAULT '0',
  `submitted` tinyint(4) NOT NULL DEFAULT '0',
  `submittimestamp` datetime DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `importname` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taxfilter` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastindex` int(11) NOT NULL DEFAULT '0',
  `last_finished_pass` int(11) DEFAULT NULL,
  `objective_container` int(11) DEFAULT NULL,
  `answerstatusfilter` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_lock` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_pmode` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_started_pass` int(11) DEFAULT NULL,
  PRIMARY KEY (`active_id`),
  UNIQUE KEY `uc1_idx` (`user_fi`,`test_fi`,`anonymous_id`),
  KEY `i1_idx` (`user_fi`),
  KEY `i2_idx` (`test_fi`),
  KEY `i3_idx` (`anonymous_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_active_seq`
--

DROP TABLE IF EXISTS `tst_active_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_active_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=12927 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `tst_invited_user`
--

DROP TABLE IF EXISTS `tst_invited_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_invited_user` (
  `test_fi` int(11) NOT NULL DEFAULT '0',
  `user_fi` int(11) NOT NULL DEFAULT '0',
  `clientip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`test_fi`,`user_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `tst_mark`
--

DROP TABLE IF EXISTS `tst_mark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_mark` (
  `mark_id` int(11) NOT NULL DEFAULT '0',
  `test_fi` int(11) NOT NULL DEFAULT '0',
  `short_name` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `official_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minimum_level` double NOT NULL DEFAULT '0',
  `passed` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mark_id`),
  KEY `i1_idx` (`test_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Mark steps of mark schemas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_mark_seq`
--

DROP TABLE IF EXISTS `tst_mark_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_mark_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6713 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_pass_result`
--

DROP TABLE IF EXISTS `tst_pass_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_pass_result` (
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `pass` int(11) NOT NULL DEFAULT '0',
  `points` double NOT NULL DEFAULT '0',
  `maxpoints` double NOT NULL DEFAULT '0',
  `questioncount` int(11) NOT NULL DEFAULT '0',
  `answeredquestions` int(11) NOT NULL DEFAULT '0',
  `workingtime` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `hint_count` int(11) DEFAULT '0',
  `hint_points` double DEFAULT '0',
  `obligations_answered` tinyint(4) NOT NULL DEFAULT '1',
  `exam_id` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`active_fi`,`pass`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `tst_result_cache`
--

DROP TABLE IF EXISTS `tst_result_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_result_cache` (
  `active_fi` int(11) NOT NULL,
  `pass` int(11) NOT NULL,
  `max_points` double NOT NULL DEFAULT '0',
  `reached_points` double NOT NULL DEFAULT '0',
  `mark_short` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `mark_official` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `passed` int(11) NOT NULL,
  `failed` int(11) NOT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `hint_count` int(11) DEFAULT '0',
  `hint_points` double DEFAULT '0',
  `obligations_answered` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`active_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_rnd_cpy`
--

DROP TABLE IF EXISTS `tst_rnd_cpy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_rnd_cpy` (
  `copy_id` int(11) NOT NULL,
  `tst_fi` int(11) NOT NULL,
  `qst_fi` int(11) NOT NULL,
  `qpl_fi` int(11) NOT NULL,
  PRIMARY KEY (`copy_id`),
  KEY `i1_idx` (`qst_fi`),
  KEY `i2_idx` (`qpl_fi`),
  KEY `i3_idx` (`tst_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_rnd_cpy_seq`
--

DROP TABLE IF EXISTS `tst_rnd_cpy_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_rnd_cpy_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=15667 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_rnd_qpl_title`
--

DROP TABLE IF EXISTS `tst_rnd_qpl_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_rnd_qpl_title` (
  `title_id` int(11) NOT NULL,
  `qpl_fi` int(11) NOT NULL,
  `tst_fi` int(11) NOT NULL,
  `qpl_title` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`title_id`),
  KEY `i1_idx` (`qpl_fi`),
  KEY `i2_idx` (`tst_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_rnd_qpl_title_seq`
--

DROP TABLE IF EXISTS `tst_rnd_qpl_title_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_rnd_qpl_title_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_rnd_quest_set_cfg`
--

DROP TABLE IF EXISTS `tst_rnd_quest_set_cfg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_rnd_quest_set_cfg` (
  `test_fi` int(11) NOT NULL DEFAULT '0',
  `req_pools_homo_scored` tinyint(4) NOT NULL DEFAULT '0',
  `quest_amount_cfg_mode` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quest_amount_per_test` int(11) DEFAULT NULL,
  `quest_sync_timestamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`test_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_rnd_quest_set_qpls`
--

DROP TABLE IF EXISTS `tst_rnd_quest_set_qpls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_rnd_quest_set_qpls` (
  `def_id` int(11) NOT NULL DEFAULT '0',
  `test_fi` int(11) NOT NULL DEFAULT '0',
  `pool_fi` int(11) NOT NULL DEFAULT '0',
  `pool_title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pool_path` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pool_quest_count` int(11) DEFAULT NULL,
  `origin_tax_fi` int(11) DEFAULT NULL,
  `origin_node_fi` int(11) DEFAULT NULL,
  `mapped_tax_fi` int(11) DEFAULT NULL,
  `mapped_node_fi` int(11) DEFAULT NULL,
  `quest_amount` int(11) DEFAULT NULL,
  `sequence_pos` int(11) DEFAULT NULL,
  `origin_tax_filter` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mapped_tax_filter` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_filter` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`def_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_rnd_quest_set_qpls_seq`
--

DROP TABLE IF EXISTS `tst_rnd_quest_set_qpls_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_rnd_quest_set_qpls_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1357 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `tst_seq_qst_checked`
--

DROP TABLE IF EXISTS `tst_seq_qst_checked`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_seq_qst_checked` (
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `pass` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`active_fi`,`pass`,`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_seq_qst_optional`
--

DROP TABLE IF EXISTS `tst_seq_qst_optional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_seq_qst_optional` (
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `pass` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `tst_sequence`
--

DROP TABLE IF EXISTS `tst_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_sequence` (
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `pass` int(11) NOT NULL DEFAULT '0',
  `sequence` longtext COLLATE utf8_unicode_ci,
  `postponed` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hidden` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `ans_opt_confirmed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`active_fi`,`pass`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `tst_skl_thresholds`
--

DROP TABLE IF EXISTS `tst_skl_thresholds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_skl_thresholds` (
  `test_fi` int(11) NOT NULL DEFAULT '0',
  `skill_base_fi` int(11) NOT NULL DEFAULT '0',
  `skill_tref_fi` int(11) NOT NULL DEFAULT '0',
  `skill_level_fi` int(11) NOT NULL DEFAULT '0',
  `threshold` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`test_fi`,`skill_base_fi`,`skill_tref_fi`,`skill_level_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_solutions`
--

DROP TABLE IF EXISTS `tst_solutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_solutions` (
  `solution_id` int(11) NOT NULL DEFAULT '0',
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `points` double DEFAULT NULL,
  `pass` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `value1` longtext COLLATE utf8_unicode_ci,
  `value2` longtext COLLATE utf8_unicode_ci,
  `step` int(11) DEFAULT NULL,
  `authorized` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`solution_id`),
  KEY `i1_idx` (`question_fi`),
  KEY `i2_idx` (`active_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Test and Assessment solutions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_solutions_seq`
--

DROP TABLE IF EXISTS `tst_solutions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_solutions_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1583300 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_test_defaults`
--

DROP TABLE IF EXISTS `tst_test_defaults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_test_defaults` (
  `test_defaults_id` int(11) NOT NULL DEFAULT '0',
  `user_fi` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `marks` longtext COLLATE utf8_unicode_ci,
  `defaults` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`test_defaults_id`),
  KEY `i1_idx` (`user_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_test_defaults_seq`
--

DROP TABLE IF EXISTS `tst_test_defaults_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_test_defaults_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_test_question`
--

DROP TABLE IF EXISTS `tst_test_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_test_question` (
  `test_question_id` int(11) NOT NULL DEFAULT '0',
  `test_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `sequence` smallint(6) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `obligatory` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`test_question_id`),
  KEY `i1_idx` (`test_fi`),
  KEY `i2_idx` (`question_fi`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Relation table for questions in tests';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_test_question_seq`
--

DROP TABLE IF EXISTS `tst_test_question_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_test_question_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=84993 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_test_result`
--

DROP TABLE IF EXISTS `tst_test_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_test_result` (
  `test_result_id` int(11) NOT NULL DEFAULT '0',
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `points` double NOT NULL DEFAULT '0',
  `pass` int(11) NOT NULL DEFAULT '0',
  `manual` tinyint(4) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `hint_count` int(11) DEFAULT '0',
  `hint_points` double DEFAULT '0',
  `answered` tinyint(4) NOT NULL DEFAULT '1',
  `step` int(11) DEFAULT NULL,
  PRIMARY KEY (`test_result_id`),
  KEY `i1_idx` (`active_fi`),
  KEY `i2_idx` (`question_fi`),
  KEY `i3_idx` (`pass`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Test and Assessment user results for test questions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_test_result_seq`
--

DROP TABLE IF EXISTS `tst_test_result_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_test_result_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=312810 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_test_rnd_qst`
--

DROP TABLE IF EXISTS `tst_test_rnd_qst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_test_rnd_qst` (
  `test_random_question_id` int(11) NOT NULL DEFAULT '0',
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `question_fi` int(11) NOT NULL DEFAULT '0',
  `sequence` smallint(6) NOT NULL DEFAULT '0',
  `pass` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `src_pool_def_fi` int(11) DEFAULT NULL,
  PRIMARY KEY (`test_random_question_id`),
  KEY `i1_idx` (`question_fi`),
  KEY `i2_idx` (`active_fi`),
  KEY `i3_idx` (`pass`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Relation table for random questions in tests';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_test_rnd_qst_seq`
--

DROP TABLE IF EXISTS `tst_test_rnd_qst_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_test_rnd_qst_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=8547 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_tests`
--

DROP TABLE IF EXISTS `tst_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_tests` (
  `test_id` int(11) NOT NULL DEFAULT '0',
  `obj_fi` int(11) NOT NULL DEFAULT '0',
  `author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence_settings` tinyint(4) NOT NULL DEFAULT '0',
  `score_reporting` tinyint(4) NOT NULL DEFAULT '0',
  `instant_verification` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `answer_feedback` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `answer_feedback_points` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `fixed_participants` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `show_cancel` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `anonymity` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `nr_of_tries` smallint(6) NOT NULL DEFAULT '0',
  `use_previous_answers` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `title_output` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `processing_time` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enable_processing_time` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `reset_processing_time` tinyint(4) NOT NULL DEFAULT '0',
  `reporting_date` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shuffle_questions` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `ects_output` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `ects_fx` double DEFAULT NULL,
  `complete` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `ects_a` double NOT NULL DEFAULT '90',
  `ects_b` double NOT NULL DEFAULT '65',
  `ects_c` double NOT NULL DEFAULT '35',
  `ects_d` double NOT NULL DEFAULT '10',
  `ects_e` double NOT NULL DEFAULT '0',
  `keep_questions` tinyint(4) NOT NULL DEFAULT '0',
  `count_system` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `mc_scoring` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `score_cutting` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `pass_scoring` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `password` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allowedusers` int(11) DEFAULT NULL,
  `alloweduserstimegap` int(11) DEFAULT NULL,
  `results_presentation` int(11) NOT NULL DEFAULT '3',
  `show_summary` int(11) NOT NULL DEFAULT '0',
  `show_question_titles` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `certificate_visibility` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `show_marker` tinyint(4) NOT NULL DEFAULT '0',
  `kiosk` int(11) NOT NULL DEFAULT '0',
  `resultoutput` int(11) NOT NULL DEFAULT '0',
  `finalstatement` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showfinalstatement` int(11) NOT NULL DEFAULT '0',
  `showinfo` int(11) NOT NULL DEFAULT '1',
  `forcejs` int(11) NOT NULL DEFAULT '0',
  `customstyle` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `mailnotification` tinyint(4) DEFAULT '0',
  `mailnottype` smallint(6) NOT NULL DEFAULT '0',
  `exportsettings` int(11) NOT NULL DEFAULT '0',
  `enabled_view_mode` varchar(20) COLLATE utf8_unicode_ci DEFAULT '0',
  `template_id` int(11) DEFAULT NULL,
  `pool_usage` tinyint(4) DEFAULT NULL,
  `online_status` tinyint(4) NOT NULL DEFAULT '0',
  `print_bs_with_res` tinyint(4) NOT NULL DEFAULT '1',
  `offer_question_hints` tinyint(4) NOT NULL DEFAULT '0',
  `highscore_enabled` int(11) DEFAULT '0',
  `highscore_anon` int(11) DEFAULT '0',
  `highscore_achieved_ts` int(11) DEFAULT '0',
  `highscore_score` int(11) DEFAULT '0',
  `highscore_percentage` int(11) DEFAULT '0',
  `highscore_hints` int(11) DEFAULT '0',
  `highscore_wtime` int(11) DEFAULT '0',
  `highscore_own_table` int(11) DEFAULT '0',
  `highscore_top_table` int(11) DEFAULT '0',
  `highscore_top_num` int(11) DEFAULT '0',
  `specific_feedback` int(11) DEFAULT '0',
  `obligations_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `autosave` tinyint(4) NOT NULL DEFAULT '0',
  `autosave_ival` int(11) NOT NULL DEFAULT '0',
  `pass_deletion_allowed` int(11) NOT NULL DEFAULT '0',
  `redirection_mode` int(11) NOT NULL DEFAULT '0',
  `redirection_url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `examid_in_test_pass` int(11) NOT NULL DEFAULT '0',
  `examid_in_test_res` int(11) NOT NULL DEFAULT '0',
  `enable_examview` tinyint(4) DEFAULT NULL,
  `show_examview_html` tinyint(4) DEFAULT NULL,
  `show_examview_pdf` tinyint(4) DEFAULT NULL,
  `enable_archiving` tinyint(4) DEFAULT NULL,
  `question_set_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FIXED_QUEST_SET',
  `sign_submission` int(11) NOT NULL DEFAULT '0',
  `char_selector_availability` int(11) NOT NULL DEFAULT '0',
  `char_selector_definition` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skill_service` tinyint(4) DEFAULT NULL,
  `result_tax_filters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inst_fb_answer_fixation` tinyint(4) DEFAULT NULL,
  `broken` tinyint(4) DEFAULT NULL,
  `show_grading_status` tinyint(4) DEFAULT '0',
  `show_grading_mark` tinyint(4) DEFAULT '0',
  `intro_enabled` tinyint(4) DEFAULT NULL,
  `starting_time_enabled` tinyint(4) DEFAULT NULL,
  `ending_time_enabled` tinyint(4) DEFAULT NULL,
  `password_enabled` tinyint(4) DEFAULT NULL,
  `limit_users_enabled` tinyint(4) DEFAULT NULL,
  `force_inst_fb` tinyint(4) DEFAULT '0',
  `starting_time` int(11) NOT NULL DEFAULT '0',
  `ending_time` int(11) NOT NULL DEFAULT '0',
  `pass_waiting` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`test_id`),
  KEY `i1_idx` (`obj_fi`),
  FULLTEXT KEY `i2_idx` (`introduction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tests in ILIAS Assessment';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_tests_seq`
--

DROP TABLE IF EXISTS `tst_tests_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_tests_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=1309 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_times`
--

DROP TABLE IF EXISTS `tst_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_times` (
  `times_id` int(11) NOT NULL DEFAULT '0',
  `active_fi` int(11) NOT NULL DEFAULT '0',
  `started` datetime DEFAULT NULL,
  `finished` datetime DEFAULT NULL,
  `pass` smallint(6) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`times_id`),
  KEY `i1_idx` (`active_fi`),
  KEY `i2_idx` (`pass`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Editing times of an assessment test';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tst_times_seq`
--

DROP TABLE IF EXISTS `tst_times_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tst_times_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=443130 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `udf_data`
--

DROP TABLE IF EXISTS `udf_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `udf_data` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `udf_definition`
--

DROP TABLE IF EXISTS `udf_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `udf_definition` (
  `field_id` int(11) NOT NULL DEFAULT '0',
  `field_name` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_type` tinyint(4) NOT NULL DEFAULT '0',
  `field_values` longtext COLLATE utf8_unicode_ci,
  `visible` tinyint(4) NOT NULL DEFAULT '0',
  `changeable` tinyint(4) NOT NULL DEFAULT '0',
  `required` tinyint(4) NOT NULL DEFAULT '0',
  `searchable` tinyint(4) NOT NULL DEFAULT '0',
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `course_export` tinyint(4) NOT NULL DEFAULT '0',
  `registration_visible` tinyint(4) DEFAULT '0',
  `visible_lua` tinyint(4) NOT NULL DEFAULT '0',
  `changeable_lua` tinyint(4) NOT NULL DEFAULT '0',
  `group_export` tinyint(4) DEFAULT '0',
  `certificate` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `udf_definition_seq`
--

DROP TABLE IF EXISTS `udf_definition_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `udf_definition_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `udf_text`
--

DROP TABLE IF EXISTS `udf_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `udf_text` (
  `usr_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `value` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usr_id`,`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ui_uihk_lfmainmenu_it`
--

DROP TABLE IF EXISTS `ui_uihk_lfmainmenu_it`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ui_uihk_lfmainmenu_it` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `nr` smallint(6) NOT NULL,
  `target` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acc_ref_id` int(11) NOT NULL,
  `acc_perm` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pmode` tinyint(4) NOT NULL DEFAULT '0',
  `it_type` tinyint(4) NOT NULL DEFAULT '0',
  `ref_id` int(11) DEFAULT '0',
  `newwin` tinyint(4) DEFAULT '0',
  `feature_id` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submenu_id` int(11) DEFAULT '0',
  `active` tinyint(4) DEFAULT '0',
  `append_last_visited` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ui_uihk_lfmainmenu_it_seq`
--

DROP TABLE IF EXISTS `ui_uihk_lfmainmenu_it_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ui_uihk_lfmainmenu_it_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `ui_uihk_lfmainmenu_mn`
--

DROP TABLE IF EXISTS `ui_uihk_lfmainmenu_mn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ui_uihk_lfmainmenu_mn` (
  `id` int(11) NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `nr` smallint(6) NOT NULL,
  `acc_ref_id` int(11) NOT NULL,
  `acc_perm` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pmode` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `append_last_visited` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ui_uihk_lfmainmenu_mn_seq`
--

DROP TABLE IF EXISTS `ui_uihk_lfmainmenu_mn_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ui_uihk_lfmainmenu_mn_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ui_uihk_lfmainmenu_tl`
--

DROP TABLE IF EXISTS `ui_uihk_lfmainmenu_tl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ui_uihk_lfmainmenu_tl` (
  `id` int(11) NOT NULL,
  `type` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`type`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `user_action_activation`
--

DROP TABLE IF EXISTS `user_action_activation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_action_activation` (
  `context_comp` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `context_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `action_comp` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`context_comp`,`context_id`,`action_comp`,`action_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `usr_data`
--

DROP TABLE IF EXISTS `usr_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_data` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `login` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwd` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` char(1) COLLATE utf8_unicode_ci DEFAULT 'm',
  `email` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `institution` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_office` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `hobby` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_home` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_mobile` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_limit_owner` int(11) DEFAULT '0',
  `time_limit_unlimited` int(11) DEFAULT '0',
  `time_limit_from` int(11) DEFAULT '0',
  `time_limit_until` int(11) DEFAULT '0',
  `time_limit_message` int(11) DEFAULT '0',
  `referral_comment` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matriculation` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `approve_date` datetime DEFAULT NULL,
  `agree_date` datetime DEFAULT NULL,
  `client_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_mode` char(10) COLLATE utf8_unicode_ci DEFAULT 'default',
  `profile_incomplete` int(11) DEFAULT '0',
  `ext_account` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `feed_hash` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loc_zoom` int(11) NOT NULL DEFAULT '0',
  `login_attempts` tinyint(4) NOT NULL DEFAULT '0',
  `last_password_change` int(11) NOT NULL DEFAULT '0',
  `reg_hash` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sel_country` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visited` longtext COLLATE utf8_unicode_ci,
  `inactivation_date` datetime DEFAULT NULL,
  `is_self_registered` tinyint(4) NOT NULL DEFAULT '0',
  `passwd_enc_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwd_salt` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_email` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `uc1_idx` (`login`),
  KEY `i1_idx` (`login`,`passwd`),
  KEY `i2_idx` (`ext_account`,`auth_mode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `usr_def_checks_udf`
--

DROP TABLE IF EXISTS `usr_def_checks_udf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_def_checks_udf` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `parent_id` bigint(20) DEFAULT NULL,
  `field_key` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `check_value` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operator` tinyint(4) DEFAULT NULL,
  `negated` tinyint(4) DEFAULT NULL,
  `owner` bigint(20) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_def_checks_udf_seq`
--

DROP TABLE IF EXISTS `usr_def_checks_udf_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_def_checks_udf_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `usr_def_sets`
--

DROP TABLE IF EXISTS `usr_def_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_def_sets` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `global_role` bigint(20) DEFAULT NULL,
  `owner` bigint(20) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `assigned_courses` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_groupes` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `portfolio_template_id` bigint(20) DEFAULT NULL,
  `portfolio_assigned_to_groups` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `portfolio_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_orgus` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_studyprograms` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_courses_desktop` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_categories_desktop` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unsubscr_from_crs_and_cat` tinyint(4) DEFAULT NULL,
  `assigned_groupes_desktop` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on_create` tinyint(4) DEFAULT NULL,
  `on_update` tinyint(4) DEFAULT NULL,
  `on_manual` tinyint(4) DEFAULT NULL,
  `assigned_groups_option_request` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_def_sets_seq`
--

DROP TABLE IF EXISTS `usr_def_sets_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_def_sets_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `usr_form_settings`
--

DROP TABLE IF EXISTS `usr_form_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_form_settings` (
  `user_id` int(11) NOT NULL,
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `settings` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_portf_acl`
--

DROP TABLE IF EXISTS `usr_portf_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_portf_acl` (
  `node_id` int(11) NOT NULL DEFAULT '0',
  `object_id` int(11) NOT NULL DEFAULT '0',
  `extended_data` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`node_id`,`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_portfolio`
--

DROP TABLE IF EXISTS `usr_portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_portfolio` (
  `id` int(11) NOT NULL,
  `is_online` tinyint(4) DEFAULT NULL,
  `is_default` tinyint(4) DEFAULT NULL,
  `bg_color` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `font_color` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppic` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_portfolio_page`
--

DROP TABLE IF EXISTS `usr_portfolio_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_portfolio_page` (
  `id` int(11) NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `order_nr` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_portfolio_page_seq`
--

DROP TABLE IF EXISTS `usr_portfolio_page_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_portfolio_page_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=8784 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_pref`
--

DROP TABLE IF EXISTS `usr_pref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_pref` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `keyword` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usr_id`,`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_pwassist`
--

DROP TABLE IF EXISTS `usr_pwassist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_pwassist` (
  `pwassist_id` char(180) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `expires` int(11) NOT NULL DEFAULT '0',
  `ctime` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pwassist_id`),
  UNIQUE KEY `c1_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_search`
--

DROP TABLE IF EXISTS `usr_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_search` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `search_result` longtext COLLATE utf8_unicode_ci,
  `checked` longtext COLLATE utf8_unicode_ci,
  `failed` longtext COLLATE utf8_unicode_ci,
  `page` tinyint(4) NOT NULL DEFAULT '0',
  `search_type` tinyint(4) NOT NULL DEFAULT '0',
  `query` longtext COLLATE utf8_unicode_ci,
  `root` int(11) DEFAULT '1',
  `item_filter` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mime_filter` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_filter` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`usr_id`,`search_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `usr_session`
--

DROP TABLE IF EXISTS `usr_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_session` (
  `session_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `expires` int(11) NOT NULL DEFAULT '0',
  `data` longtext COLLATE utf8_unicode_ci,
  `ctime` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `last_remind_ts` int(11) NOT NULL DEFAULT '0',
  `type` int(11) DEFAULT NULL,
  `createtime` int(11) DEFAULT NULL,
  `remote_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`session_id`),
  KEY `i1_idx` (`expires`),
  KEY `i2_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `usr_session_stats`
--

DROP TABLE IF EXISTS `usr_session_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_session_stats` (
  `slot_begin` int(11) NOT NULL,
  `slot_end` int(11) NOT NULL,
  `active_min` int(11) DEFAULT NULL,
  `active_max` int(11) DEFAULT NULL,
  `active_avg` int(11) DEFAULT NULL,
  `active_end` int(11) DEFAULT NULL,
  `opened` int(11) DEFAULT NULL,
  `closed_manual` int(11) DEFAULT NULL,
  `closed_expire` int(11) DEFAULT NULL,
  `closed_idle` int(11) DEFAULT NULL,
  `closed_idle_first` int(11) DEFAULT NULL,
  `closed_limit` int(11) DEFAULT NULL,
  `closed_login` int(11) DEFAULT NULL,
  `max_sessions` int(11) DEFAULT NULL,
  `closed_misc` int(11) DEFAULT '0',
  PRIMARY KEY (`slot_begin`),
  KEY `i1_idx` (`slot_end`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_session_stats_raw`
--

DROP TABLE IF EXISTS `usr_session_stats_raw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_session_stats_raw` (
  `session_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) DEFAULT NULL,
  `end_context` smallint(6) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_starting_point`
--

DROP TABLE IF EXISTS `usr_starting_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_starting_point` (
  `id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) DEFAULT '0',
  `starting_point` int(11) DEFAULT '0',
  `starting_object` int(11) DEFAULT '0',
  `rule_type` int(11) DEFAULT '0',
  `rule_options` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usr_starting_point_seq`
--

DROP TABLE IF EXISTS `usr_starting_point_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr_starting_point_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ut_lp_coll_manual`
--

DROP TABLE IF EXISTS `ut_lp_coll_manual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ut_lp_coll_manual` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `subitem_id` int(11) NOT NULL DEFAULT '0',
  `completed` tinyint(4) NOT NULL DEFAULT '0',
  `last_change` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`,`usr_id`,`subitem_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ut_lp_collections`
--

DROP TABLE IF EXISTS `ut_lp_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ut_lp_collections` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `grouping_id` int(11) NOT NULL DEFAULT '0',
  `num_obligatory` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `lpmode` tinyint(4) DEFAULT '5',
  PRIMARY KEY (`obj_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `ut_lp_marks`
--

DROP TABLE IF EXISTS `ut_lp_marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ut_lp_marks` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `completed` int(11) NOT NULL DEFAULT '0',
  `mark` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_comment` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `status_changed` datetime DEFAULT NULL,
  `status_dirty` tinyint(4) NOT NULL DEFAULT '0',
  `percentage` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`obj_id`,`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ut_lp_settings`
--

DROP TABLE IF EXISTS `ut_lp_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ut_lp_settings` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `obj_type` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_mode` tinyint(4) NOT NULL DEFAULT '0',
  `visits` int(11) DEFAULT '0',
  PRIMARY KEY (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `ut_online`
--

DROP TABLE IF EXISTS `ut_online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ut_online` (
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `online_time` int(11) NOT NULL DEFAULT '0',
  `access_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `webr_items`
--

DROP TABLE IF EXISTS `webr_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webr_items` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `webr_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(127) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `disable_check` tinyint(4) DEFAULT NULL,
  `create_date` int(11) NOT NULL DEFAULT '0',
  `last_update` int(11) NOT NULL DEFAULT '0',
  `last_check` int(11) DEFAULT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '0',
  `internal` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `i1_idx` (`link_id`,`webr_id`),
  FULLTEXT KEY `i2_idx` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `webr_items_seq`
--

DROP TABLE IF EXISTS `webr_items_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webr_items_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=221 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--


--


--


--


--


--


--


--
-- Table structure for table `wiki_page_template`
--

DROP TABLE IF EXISTS `wiki_page_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_page_template` (
  `wiki_id` int(11) NOT NULL,
  `wpage_id` int(11) NOT NULL,
  `new_pages` tinyint(4) NOT NULL DEFAULT '0',
  `add_to_page` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wiki_id`,`wpage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_stat`
--

DROP TABLE IF EXISTS `wiki_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_stat` (
  `wiki_id` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  `num_pages` int(11) NOT NULL,
  `del_pages` int(11) NOT NULL DEFAULT '0',
  `avg_rating` int(11) NOT NULL DEFAULT '0',
  `ts_day` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ts_hour` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`wiki_id`,`ts`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_stat_page`
--

DROP TABLE IF EXISTS `wiki_stat_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_stat_page` (
  `wiki_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  `int_links` int(11) NOT NULL,
  `ext_links` int(11) NOT NULL,
  `footnotes` int(11) NOT NULL,
  `num_ratings` int(11) NOT NULL DEFAULT '0',
  `num_words` int(11) NOT NULL,
  `num_chars` bigint(20) NOT NULL,
  `avg_rating` int(11) NOT NULL DEFAULT '0',
  `ts_day` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ts_hour` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wiki_id`,`page_id`,`ts`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_stat_page_user`
--

DROP TABLE IF EXISTS `wiki_stat_page_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_stat_page_user` (
  `wiki_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  `changes` int(11) NOT NULL DEFAULT '0',
  `read_events` int(11) NOT NULL DEFAULT '0',
  `ts_day` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ts_hour` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`wiki_id`,`page_id`,`ts`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_stat_user`
--

DROP TABLE IF EXISTS `wiki_stat_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_stat_user` (
  `wiki_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  `new_pages` int(11) NOT NULL,
  `ts_day` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ts_hour` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`wiki_id`,`user_id`,`ts`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_user_html_export`
--

DROP TABLE IF EXISTS `wiki_user_html_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_user_html_export` (
  `wiki_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `start_ts` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wiki_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `write_event`
--

DROP TABLE IF EXISTS `write_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `write_event` (
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `parent_obj_id` int(11) NOT NULL DEFAULT '0',
  `usr_id` int(11) NOT NULL DEFAULT '0',
  `action` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `ts` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `write_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`write_id`),
  KEY `i1_idx` (`parent_obj_id`,`ts`),
  KEY `i2_idx` (`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `write_event_seq`
--

DROP TABLE IF EXISTS `write_event_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `write_event_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=19383 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `xmlnestedset`
--

DROP TABLE IF EXISTS `xmlnestedset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xmlnestedset` (
  `ns_id` int(11) NOT NULL,
  `ns_book_fk` int(11) NOT NULL,
  `ns_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ns_tag_fk` int(11) NOT NULL,
  `ns_l` int(11) NOT NULL,
  `ns_r` int(11) NOT NULL,
  PRIMARY KEY (`ns_id`),
  KEY `i1_idx` (`ns_tag_fk`),
  KEY `i2_idx` (`ns_l`),
  KEY `i3_idx` (`ns_r`),
  KEY `i4_idx` (`ns_book_fk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--


--
-- Table structure for table `xmlparam`
--

DROP TABLE IF EXISTS `xmlparam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xmlparam` (
  `tag_fk` int(11) NOT NULL DEFAULT '0',
  `param_name` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `param_value` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tag_fk`,`param_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xmltags`
--

DROP TABLE IF EXISTS `xmltags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xmltags` (
  `tag_pk` int(11) NOT NULL DEFAULT '0',
  `tag_depth` int(11) NOT NULL DEFAULT '0',
  `tag_name` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tag_pk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xmltags_seq`
--

DROP TABLE IF EXISTS `xmltags_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xmltags_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xmlvalue`
--

DROP TABLE IF EXISTS `xmlvalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xmlvalue` (
  `tag_value_pk` int(11) NOT NULL DEFAULT '0',
  `tag_fk` int(11) NOT NULL DEFAULT '0',
  `tag_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`tag_value_pk`),
  KEY `i1_idx` (`tag_fk`),
  FULLTEXT KEY `i2_idx` (`tag_value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xmlvalue_seq`
--

DROP TABLE IF EXISTS `xmlvalue_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xmlvalue_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_cas_cache`
--

DROP TABLE IF EXISTS `xqcas_cas_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_cas_cache` (
  `id` bigint(20) NOT NULL,
  `hash` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `command` longtext COLLATE utf8_unicode_ci NOT NULL,
  `result` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i3_idx` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_cas_cache_seq`
--

DROP TABLE IF EXISTS `xqcas_cas_cache_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_cas_cache_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=650573 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_configuration`
--

DROP TABLE IF EXISTS `xqcas_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_configuration` (
  `parameter_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  `group_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`parameter_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_deployed_seeds`
--

DROP TABLE IF EXISTS `xqcas_deployed_seeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_deployed_seeds` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `seed` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i6_idx` (`question_id`,`seed`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_deployed_seeds_seq`
--

DROP TABLE IF EXISTS `xqcas_deployed_seeds_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_deployed_seeds_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=16648 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_extra_info`
--

DROP TABLE IF EXISTS `xqcas_extra_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_extra_info` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `general_feedback` longtext COLLATE utf8_unicode_ci,
  `penalty` varchar(21) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hidden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_extra_info_seq`
--

DROP TABLE IF EXISTS `xqcas_extra_info_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_extra_info_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=8045 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_inputs`
--

DROP TABLE IF EXISTS `xqcas_inputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_inputs` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tans` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `box_size` bigint(20) DEFAULT '15',
  `strict_syntax` int(11) DEFAULT '1',
  `insert_stars` int(11) DEFAULT '0',
  `syntax_hint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forbid_words` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forbid_float` int(11) DEFAULT '1',
  `require_lowest_terms` int(11) DEFAULT '0',
  `check_answer_type` int(11) DEFAULT '0',
  `must_verify` int(11) DEFAULT '1',
  `show_validation` int(11) DEFAULT '1',
  `options` longtext COLLATE utf8_unicode_ci,
  `allow_words` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`question_id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_inputs_seq`
--

DROP TABLE IF EXISTS `xqcas_inputs_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_inputs_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=16394 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_options`
--

DROP TABLE IF EXISTS `xqcas_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_options` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `question_variables` longtext COLLATE utf8_unicode_ci,
  `specific_feedback` longtext COLLATE utf8_unicode_ci,
  `specific_feedback_format` smallint(6) DEFAULT '0',
  `question_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_simplify` int(11) DEFAULT '1',
  `assume_positive` int(11) DEFAULT '0',
  `prt_correct` longtext COLLATE utf8_unicode_ci,
  `prt_correct_format` smallint(6) DEFAULT '0',
  `prt_partially_correct` longtext COLLATE utf8_unicode_ci,
  `prt_partially_correct_format` smallint(6) DEFAULT '0',
  `prt_incorrect` longtext COLLATE utf8_unicode_ci,
  `prt_incorrect_format` smallint(6) DEFAULT '0',
  `multiplication_sign` varchar(8) COLLATE utf8_unicode_ci DEFAULT 'dot',
  `sqrt_sign` int(11) DEFAULT '1',
  `complex_no` varchar(8) COLLATE utf8_unicode_ci DEFAULT 'i',
  `inverse_trig` varchar(8) COLLATE utf8_unicode_ci DEFAULT 'cos-1',
  `variants_selection_seed` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matrix_parens` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_options_seq`
--

DROP TABLE IF EXISTS `xqcas_options_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_options_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=8109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_prt_nodes`
--

DROP TABLE IF EXISTS `xqcas_prt_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_prt_nodes` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `prt_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `node_name` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `answer_test` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sans` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tans` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test_options` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quiet` int(11) DEFAULT '0',
  `true_score_mode` varchar(4) COLLATE utf8_unicode_ci DEFAULT '=',
  `true_score` varchar(21) COLLATE utf8_unicode_ci DEFAULT NULL,
  `true_penalty` varchar(21) COLLATE utf8_unicode_ci DEFAULT NULL,
  `true_next_node` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `true_answer_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `true_feedback` longtext COLLATE utf8_unicode_ci,
  `true_feedback_format` smallint(6) DEFAULT '0',
  `false_score_mode` varchar(4) COLLATE utf8_unicode_ci DEFAULT '=',
  `false_score` varchar(21) COLLATE utf8_unicode_ci DEFAULT NULL,
  `false_penalty` varchar(21) COLLATE utf8_unicode_ci DEFAULT NULL,
  `false_next_node` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `false_answer_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `false_feedback` longtext COLLATE utf8_unicode_ci,
  `false_feedback_format` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i2_idx` (`question_id`,`prt_name`,`node_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_prt_nodes_seq`
--

DROP TABLE IF EXISTS `xqcas_prt_nodes_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_prt_nodes_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=22039 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_prts`
--

DROP TABLE IF EXISTS `xqcas_prts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_prts` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(21) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto_simplify` int(11) DEFAULT '1',
  `feedback_variables` longtext COLLATE utf8_unicode_ci,
  `first_node_name` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_prts_seq`
--

DROP TABLE IF EXISTS `xqcas_prts_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_prts_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=14616 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_qtest_expected`
--

DROP TABLE IF EXISTS `xqcas_qtest_expected`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_qtest_expected` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `test_case` bigint(20) DEFAULT NULL,
  `prt_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expected_score` varchar(21) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expected_penalty` varchar(21) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expected_answer_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i5_idx` (`question_id`,`test_case`,`prt_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_qtest_expected_seq`
--

DROP TABLE IF EXISTS `xqcas_qtest_expected_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_qtest_expected_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3685 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_qtest_inputs`
--

DROP TABLE IF EXISTS `xqcas_qtest_inputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_qtest_inputs` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `test_case` bigint(20) DEFAULT NULL,
  `input_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i4_idx` (`question_id`,`test_case`,`input_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_qtest_inputs_seq`
--

DROP TABLE IF EXISTS `xqcas_qtest_inputs_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_qtest_inputs_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=3685 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_qtests`
--

DROP TABLE IF EXISTS `xqcas_qtests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_qtests` (
  `id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `test_case` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xqcas_qtests_seq`
--

DROP TABLE IF EXISTS `xqcas_qtests_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xqcas_qtests_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2760 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--


--
-- Table structure for table `xxco_data_types`
--

DROP TABLE IF EXISTS `xxco_data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xxco_data_types` (
  `type_id` int(11) NOT NULL DEFAULT '0',
  `type_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `availability` int(11) NOT NULL DEFAULT '1',
  `remarks` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interface_xml` longtext COLLATE utf8_unicode_ci,
  `time_to_delete` int(11) DEFAULT NULL,
  `use_logs` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use_learning_progress` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xxco_data_types_seq`
--

DROP TABLE IF EXISTS `xxco_data_types_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xxco_data_types_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--


--
-- Table structure for table `xxco_results`
--

DROP TABLE IF EXISTS `xxco_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xxco_results` (
  `id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `result` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i1_idx` (`obj_id`,`usr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xxco_results_seq`
--

DROP TABLE IF EXISTS `xxco_results_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xxco_results_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xxco_type_values`
--

DROP TABLE IF EXISTS `xxco_type_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xxco_type_values` (
  `type_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `field_value` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`type_id`,`field_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-25  9:09:15
