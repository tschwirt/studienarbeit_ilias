import argparse
import numpy as np

from Item import Item
import utils
import mathCalc
import testCorrelation


def analysisForAllQuestions(questionIDs, studentTestIDs, testId, min_ans_questions_in_percent=utils.MIN_ANS_QUEST_IN_PER):
    res = []
    for qID in questionIDs:
        itemObj = Item(qID, testId)
        pointsForQuestion = []
        pointsInWholeTest = []
        bothPointsArr = []
        studentsPoints = utils.get_students_points_for_question(qID, min_ans_questions_in_percent)
        for student in studentsPoints:
            if student[0] in studentTestIDs:
                pointsForQuestion.append(student[1])
                pointsInWholeTest.append(student[2])
                # Needed for item discrimination index
                bothPointsArr.append([student[2], student[1]])
        # Needed for point biserial correlation
        pointsInWholeTestWithoutQuestion = np.subtract(np.array(pointsInWholeTest), np.array(pointsForQuestion))

        pearson = mathCalc.pearson(pointsForQuestion, pointsInWholeTest)
        pointBisCor = mathCalc.pointBiserialCorrelation(pointsForQuestion, pointsInWholeTestWithoutQuestion)
        rightAnswersStudentsHighResults = mathCalc.rightAnswersStudentsHigherThan(0.80, pointsForQuestion,
                                                                                  pointsInWholeTest)
        rightAnswersStudentsLowResults = mathCalc.rightAnswersStudentsLowerThan(0.50, pointsForQuestion,
                                                                                pointsInWholeTest)
        itemDisIdx = mathCalc.itemDiscriminationIndex(bothPointsArr)

        difficultyidx_with_random_correction = None
        if itemObj.getTypeID() == 1:  # Single-Choice Item
            # Calculates difficulty with random correction
            difficultyidx_with_random_correction = mathCalc.difficultyOfSCItem(pointsForQuestion, len(itemObj.getChoices()))

            # Calculates difficulty with plausibility correction
            #correctAnswer = itemObj.getCorrect()
            #answers = itemObj.getStudentsAnswers()
            #if len(answers) > correctAnswer:
            #    del answers[correctAnswer]
            #    mostUsedWrongAnswer = max(answers, key=lambda item: item[2])
            #    difficultyIdx_Corrected = mathCalc.difficultyOfSCItemCorrected(pointsForQuestion,
            #                                                                   mostUsedWrongAnswer[2])

        difficultyIdx = mathCalc.rightAnswers(pointsForQuestion)
        difficulty = mathCalc.asDifficulty(difficultyIdx)

        print(
            "Frage %s (%s): Pearson = %.2f, PBC = %.2f, ID = %.2f, Difficulty = %.2f [%s], Correct(low results) = %.2f, Correct(high results) = %.2f" % (
                qID, itemObj.getTypeTag(), pearson, pointBisCor, itemDisIdx, difficultyIdx, difficulty,
                rightAnswersStudentsLowResults, rightAnswersStudentsHighResults))
        res.append([qID, itemObj.getTypeTag(), pearson, pointBisCor, itemDisIdx, difficultyIdx, difficultyidx_with_random_correction if difficultyidx_with_random_correction else "N/A", difficulty, rightAnswersStudentsLowResults,
                    rightAnswersStudentsHighResults])

    print("Number of analyzed questions: " + str(len(res)))
    print("Number of participants: " + str(len(studentTestIDs)))
    # Analysis to csv file
    utils.to_csv(["Question ID","Type","Pearson", "PBC", "ID", "Difficulty Index", "DI Corrected", "Difficulty", "Correct (low res)",
           "Correct (high res)"], res, 'analysisResult.csv')


# Calculates Cronbachs Alpha for a given Test
def cronbachAlphaforTest(testId, min_ans_questions_in_percent=utils.MIN_ANS_QUEST_IN_PER):
    studentTestIDs = utils.get_users_for_test(testId, min_ans_questions_in_percent)
    questionIDs = utils.get_questions_for_test(testId)
    questionIDs.sort()

    resultArr = np.zeros([len(questionIDs), len(studentTestIDs)], dtype=int)

    studentCounter = 0
    for studentid in studentTestIDs:
        pointPerQuestion = utils.getStudentQuestionPointsForTest(studentid, testId)

        if pointPerQuestion != None:
            questionCounter = 0
            i = 0
            while i < len(pointPerQuestion):
                if pointPerQuestion[i][0] == questionIDs[questionCounter]:
                    resultArr[questionCounter][studentCounter] = pointPerQuestion[i][1]
                    questionCounter += 1
                    i += 1
                else:
                    questionCounter += 1

            studentCounter += 1
        else:
            resultArr = np.delete(resultArr, studentCounter, 1)

    # Error handling
    if not np.array_equal(resultArr, resultArr.astype(bool)):
        return -1

    print(mathCalc.cronbachAlpha(resultArr))



# Setup command-line arguments
parser = argparse.ArgumentParser()

# Analyse whole test
parser.add_argument("-c", "--cronbachAlpha", type=int, help="Calculate Cronbach Alpha for TestID.", nargs=1, metavar=('TestID'))
parser.add_argument("-a", "--all", type=int, help="Execute full item analysis for TestID.", nargs=1, metavar=('TestID'))

parser.add_argument("-o","--outlierConfig", type=int,
              help='Minimum percentage of answered questions [default: 20]', nargs=1, metavar=('Percentage'))

# Analyse specific item
parser.add_argument("-l", "--latex", help="Use latex compiler for plotting.",
                    action="store_true")
parser.add_argument("-p", "--plot", type=int, help="Plot stats for a specific Item from Test.", nargs=2, metavar=('ItemID', 'TestID'))
parser.add_argument("-v", "--view", type=int, help="View specific Item from Test on command-line.", nargs=2, metavar=('ItemID', 'TestID'))

args = parser.parse_args()

# Trigger full item analysis for test
if args.all:
    if args.outlierConfig:
        studentTestIDs = utils.get_users_for_test(args.all[0], args.outlierConfig[0])
    else:
        studentTestIDs = utils.get_users_for_test(args.all[0])
    questionIDs = utils.get_questions_for_test(args.all[0])
    if args.outlierConfig:
        analysisForAllQuestions(questionIDs, studentTestIDs, args.all[0], args.outlierConfig[0])
    else:
        analysisForAllQuestions(questionIDs, studentTestIDs, args.all[0])
    
# Calculate Cronbachs Alpha for test
if args.cronbachAlpha:
    if args.outlierConfig:
        cronbachAlphaforTest(args.cronbachAlpha[0], args.outlierConfig[0])
    else:
        cronbachAlphaforTest(args.cronbachAlpha[0])

# Plot a specific item
if args.plot:
    item = Item(args.plot[0], args.plot[1])
    item.plot(usetex = args.latex)

# View item stats on command-line
if args.view:
    item = Item(args.view[0], args.view[1])
    item.print() # in Progress


#result1 = StudentTestResult(46916)
#result1.print_results()

#question1 = Item(91452) #Single Choice
#question1 = Item(91453) #Cloze
#question1 = Item(91499) #Cloze with choices
#question1 = Item(91504) #Cloze with multiple gaps
#question1.plot(usetex = True) #with latex compilation
#question1.plot() #without latex compilation
#question1.printItem() # in Progress


#studentTestIDs = utils.get_users_for_test(1194)
#testCorrelation.calculateTestVariation(studentTestIDs[0:5])
#testCorrelation.collectDataToCSV(studentTestIDs)
#testCorrelation.regressionApproach()

#questionIDs = misc.getQuestionsForTest(1270)

#misc.analysisForAllQuestions(questionIDs, studentTestIDs)

#count = 0
#countMax = 10
#for testID in studentTestIDs:
#    if count < countMax:
#        student = StudentTestResult(testID)
#        student.print_results()
#    count+=1

# Analyses all questions and print their stochastic values
