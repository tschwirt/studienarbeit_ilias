# Interessante  und speicherwürdige sql-Abfragen

use ilias;

# Zeigt alle in ilias-Schema enthaltenen Tabellen (+ Gesamtanzahl)
SELECT table_schema, table_type, table_name FROM information_schema.tables where TABLE_SCHEMA='ilias';

# Zeigt alle leeren Tabellen von ilias
SELECT table_schema, table_type, table_name  FROM information_schema.tables WHERE table_rows = 0 AND TABLE_SCHEMA='ilias';

# Zeigt alle '_seq' Tabellen von ilias
select table_name from information_schema.tables where table_name LIKE '%_seq' AND TABLE_SCHEMA='ilias';

# Selektiert Suchanfragen von Usern mit Namen
select ud.firstname, ud.lastname, us.query  from usr_search as us JOIN usr_data as ud ON us.usr_id = ud.usr_id;

# Selektiert laufende und abgelaufene Sessions mit User 
select ud.firstname, ud.lastname, ussr.start_time, ussr.end_time from usr_session_stats_raw as ussr JOIN usr_data as ud ON ud.usr_id = ussr.user_id;

# Selektiert User mit zugehöriger Hochschule und Studienrichtung
select ud.firstname, ud.lastname ,ut.value from udf_text as ut JOIN usr_data as ud ON ut.usr_id = ud.usr_id WHERE ut.value <> "";

# Selektiert User und zugehörige Bookings
select ud.firstname, ud.lastname, bo.booking_message from booking_user as bo JOIN usr_data as ud ON ud.usr_id = bo.user_id;

# Selektiert Freunde
select ud.firstname freund1a, ud.lastname freund1b, ud1.firstname freund2a, ud1.lastname freund2b from buddylist as bu JOIN usr_data as ud ON ud.usr_id = bu.usr_id JOIN usr_data as ud1 ON ud1.usr_id = bu.buddy_usr_id;

# Selektiert User mit abgegebener Bewertung
select ud.firstname, ud.lastname ,ra.rating, rac.title from il_rating as ra JOIN usr_data as ud ON ra.user_id = ud.usr_id LEFT JOIN il_rating_cat as rac ON ra.category_id = rac.id;

# Selektiert User und angeklickte Module
SELECT ud.firstname, ud.lastname, om.passed, om.origin_ts, od.title, od.description FROM obj_members as om JOIN usr_data as ud ON ud.usr_id = om.usr_id join object_data od on od.obj_id = om.obj_id;
# Selektiert User und bestandene Module
SELECT ud.firstname, ud.lastname, om.passed, om.origin_ts, od.title, od.description FROM obj_members as om JOIN usr_data as ud ON ud.usr_id = om.usr_id join object_data od on od.obj_id = om.obj_id where om.passed = 1;

# Selektiert User und zugewiesene Skill-Level
SELECT ud.firstname, ud.lastname, l.title, l.description, sl.status_date, sl.trigger_title FROM skl_user_skill_level sl JOIN skl_level l ON sl.level_id = l.id JOIN usr_data as ud ON ud.usr_id = sl.user_id;

# Selektiert User die eine Umfrage abgeschlossen haben
SELECT ud.firstname, ud.lastname, sf.state, sf.tstamp FROM svy_finished sf JOIN usr_data as ud ON ud.usr_id = sf.user_fi;

# Selektiert User mit ihren Testergebnissen
SELECT ud.firstname, ud.lastname, lpm.status, lpm.status_changed, lpm.percentage FROM ut_lp_marks lpm JOIN usr_data as ud ON ud.usr_id = lpm.usr_id where lpm.status >= 2 AND ud.firstname = 'Drop';

# Selektiert User und aufgerufene Wikiseiten
SELECT ud.firstname, ud.lastname, w.wiki_id, w.page_id, w.ts, w.read_events FROM wiki_stat_page_user w JOIN usr_data ud ON ud.usr_id = w.user_id;

# Selektiert Kurse inkl. Inhaltsbeschreibung (teilweise Redundanzen vorhanden)
select  crsO.title, crsO.description from crs_objectives as crsO JOIN crs_objective_tst as crsOT ON crsO.objective_id = crsOT.objective_id order by crsO.title ASC;

# Selektiert ausführliche Defintionen für geg. Wörter (Terme)
select gt.term, gd.short_text from glossary_term as gt JOIN glossary_definition as gd ON gt.id = gd.term_id;

# Anscheinend der Quelltext für selbst erstellte Seiten
select * from page_object where parent_type="cont" LIMIT 30;
	# Änderungshistorie für selbst erstelle Seiten
	select * from page_history where parent_type="cont" LIMIT 30;

# Mögliche Antworten für Single(Multiple)-Choice Frage
select aorder, answertext from qpl_a_sc where question_fi=<QID> ORDER BY aorder ASC;

# Gesamtzahl aller mehrfach angelegten Accounts
select sum(occurence)-count(email) as number_of_multi_acc from (
select count(usr_id) as occurence, email from usr_data
group by email
having count(usr_id) > 1
order by email asc
) a;

# Mehrfach angelegte Accounts, die ausgeschlossen werden müssen (Ergebnis entspricht nicht genau der Liste  von Fr. Derr)
select ud.usr_id, ud.email from usr_data ud JOIN 
(
	select email, min(create_date) as first_acc from usr_data
	group by email
	having count(usr_id) > 1
) subq ON subq.email = ud.email
where subq.first_acc <> ud.create_date
order by ud.email;