import mysqlc
import utils
import re
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.cm as cm
import numpy as np
from pylab import figure, axes, plot, xlabel, ylabel, title, grid, savefig, show, figtext

# Einstiegstest ID
INTRO_TEST_ID = 1270

class Item:

    def __init__(self, id, testId):
        self.id = id
        self.testId = testId
        self.setType()
        self.setQuestionText()
        self.setPoints()
        self.setChoices()
        self.setCorrect()
        self.setStudentsAnswers()

    def print(self):
        htmlRegex = '<[^>]*>'

        print(self.getTypeTag() + " Question " + str(self.id) + " | " + str(self.getPoints()) + " Point(s)")   
        print(self.getQuestionText())
        i = 1
        for choice in self.getChoices():
            humanReadable = re.sub(r'%s' % htmlRegex, "", choice)
            print(str(i) + ". " + humanReadable)
            i+=1

        # Prints aggregated students answers
        print("\nStudents Answers:")
        # Checks if question is single choice
        if self.getTypeID() == 1:
            i = 1
            # Iterates over all single choice answer options and prints number of students who chose this option
            for i in range(1, len(self.getStudentsAnswers())+1):
                print(str(i) + ": " + str(self.getStudentsAnswers()[i-1][2]) + " (Low: " + str(self.getStudentsAnswers()[i-1][3]) + " , High: " + str(self.getStudentsAnswers()[i-1][4]) + ")")
        # No singe choice question
        else:
            # Checks if question has just one gap by comparing value1 (Gap-ID) of first and last answer
            if self.getStudentsAnswers()[0][1] == self.getStudentsAnswers()[len(self.getStudentsAnswers())-1][1]:
                # Iterates over all given answers and gives number of student who took this answer
                for answer in self.getStudentsAnswers():
                    print(str(answer[2]) + ": " + str(answer[3]))
            # Question has multiple gaps
            else:
                # Saves first Gap-ID in variable
                currentGap = self.getStudentsAnswers()[0][1]
                # Iterates over all given answers and prints number of student who took this answer
                for answer in self.getStudentsAnswers():
                    # If Gap-ID differs from current Gap-ID set this to the new one 
                    if answer[1] != currentGap:
                        currentGap = answer[1]
                    print("[Gap " + str(currentGap) + "] " + str(answer[2]) + ": " + str(answer[3]))

    def plot(self, usetex = False, outputFile = 'item.png'): # BETA & in Progress
        escapechar_regex = '[\\\]'

        # configure LateX usage
        self.latexPlotter(usetex)

        if self.getTypeID() == 1:

            N = len(self.getChoices())

            low_scorer = []
            high_scorer = []
            y_axis_max = 0
            for choice in self.getStudentsAnswers():
                low_scorer.append(choice[3])
                high_scorer.append(choice[4])
                if choice[3]+choice[4] > y_axis_max:
                    y_axis_max = choice[3] + choice[4]

            low_scorer = tuple(low_scorer)
            high_scorer = tuple(high_scorer)

            ind = np.arange(N)  # the x locations for the groups
            width = 0.6 # the width of the bars: can also be len(x) sequence

            bar_colors = [ 'green' if choice_iterator == self.getCorrect() else 'none'
                           for choice_iterator in range(N) ]
            p1 = plt.bar(ind, low_scorer, width, edgecolor=bar_colors)
            p2 = plt.bar(ind, high_scorer, width, edgecolor=bar_colors,
                         bottom=low_scorer)

            plt.xlabel('Choices')
            plt.ylabel('Number of Students')
            plt.title("Item " + str(self.id))
            plt.xticks(ind, (f'{i}' for i in range(1, N+1)))
            plt.legend((p1[0], p2[0]), ('Lower 27% of Students', 'Upper 27% of Students'))

            plt.savefig(f'item_{self.id}.png')
        
        elif self.getTypeID() == 3:
            plt.style.use('ggplot')
            plt.gca().set_position((.1, .3, .8, .6))
            # setup plot x-axis
            x = []
            # setup plot y-axis
            y = []
            # Checks if question has just one gap by comparing value1 (Gap-ID) of first and last answer
            if self.getStudentsAnswers()[0][1] == self.getStudentsAnswers()[len(self.getStudentsAnswers())-1][1]:
                # Iterates over all given answers and gives number of student who took this answer
                studentAnswers = self.getStudentsAnswers()
                answerStore = np.zeros([len(studentAnswers),2])
                i = 0
                for answer in studentAnswers:
                    answerStore[i][0] = answer[2]
                    answerStore[i][1] = answer[3]
                    i += 1
                # Sort to get the most popular answers
                answerStoreSorted = answerStore[answerStore[:,1].argsort()]

                # setup plot axis
                for i in range(1,5):
                    x.append(answerStoreSorted[-i][0])
                    y.append(answerStoreSorted[-i][1])

                x_pos = np.arange(len(x))
                plt.bar(x_pos, y, color='#7ed6df')
                plt.xlabel("Choices")
                plt.ylabel("Students")
                if usetex:
                    plt.title(r"%s" % self.getQuestionText())
                else:
                    plt.title(re.sub(r'%s' % escapechar_regex, "", self.getQuestionText()))
                plt.xticks(x_pos, x)

                try:
                    plt.savefig(outputFile,bbox_inches='tight',dpi=100)
                except:
                    print("Latex error. Sorry for any inconvenience.")

        else:
            print("Item " + str(self.id) + " with type %s is not plottable at the moment!" % self.getTypeID())

    def getTypeTag(self):
        return self.typeTag

    def getTypeID(self):
        return self.typeId

    def getQuestionText(self):
        return self.questionText

    def getPoints(self):
        return self.points

    def getChoices(self):
        return self.choices

    def getStudentsAnswers(self):
        return self.studentsAnswers
    
    def getCorrect(self):
        return self.correct

    def setType(self):
        res = mysqlc.query("select qqt.question_type_id, qqt.type_tag from qpl_qst_type qqt JOIN qpl_questions qq ON qqt.question_type_id = qq.question_type_fi where qq.question_id=  %s", [self.id])
        self.typeId = int(res[0][0]) 
        self.typeTag = (res[0][1])[3:] # [3:] removes 'ass'-prefix of typestring

    def setQuestionText(self):
        html_latex_regex = r'<[^>]*>|[ \t]?\[\/?tex]'
        
        res = mysqlc.query("select question_text from qpl_questions where question_id = %s", [self.id])
        
        questionText = self.latexSyntaxHandler(res[0][0])

        # remove html tags and latex marker from string
        humanReadable = re.sub(r'%s' % html_latex_regex, "", questionText) 
        self.questionText = humanReadable

    def setPoints(self):
        res = mysqlc.query("select points from qpl_questions where question_id = %s", [self.id])
        self.points = int(res[0][0])

    def setChoices(self):
        html_latex_regex = '<[^>]*>|[ \t]?\[\/?tex]'

        n = self.getTypeID()
        choiceTable = None

        if n == 1: # Single Choice questions
            choiceTable = "qpl_a_sc"
        #elif n== 1 or n == 9 or n == 4:
        #    print "n is a perfect square\n"
        #elif n == 2:
        #    print "n is an even number\n"
        #elif  n== 3 or n == 5 or n == 7:
        #    print "n is a prime number\n"
        
        self.choices = []
        if choiceTable is None:
            self.choices = []
        else:
            res = mysqlc.query("select answertext from " + choiceTable + " where question_fi = %s ORDER BY aorder ASC;", [self.id])
            for choice in res:
                self.choices.append(self.latexSyntaxHandler(choice[0]))

    def setCorrect(self):
        n = self.getTypeID()
        choiceTable = None

        if n == 1: # Single Choice questions
            choiceTable = "qpl_a_sc"
            self.correct = mysqlc.query("select aorder from " + choiceTable + " where question_fi = %s and points = 1 ORDER BY aorder ASC;", [self.id])[0][0]
        elif n == 3: # Cloze questions
            choiceTable = "qpl_a_cloze"
            try:
                self.correct = mysqlc.query("select answertext from " + choiceTable + " where question_fi = %s and points = 1 ORDER BY aorder ASC;", [self.id])[0][0]
            except IndexError:
                self.correct = None
        else:
            self.correct = None

    def setStudentsAnswers(self):
        test_participant_ids = utils.get_users_for_test(self.testId)

        # Checks if question is Single Choice
        if self.getTypeID() == 1:
            choices_total = len(self.getChoices())

            points_of_testtakers = [testtaker for testtaker in utils.get_students_points_for_question(self.id) if testtaker[0] in test_participant_ids]
            number_of_testtakers = len(points_of_testtakers)
            points_of_testtakers.sort(key=lambda x: x[2])
            choices_of_testtakers = dict(utils.get_students_choices_for_question(self.id, self.testId))

            low_scoring_testtakers = np.zeros(choices_total, dtype=int)
            high_scoring_testtakers = np.zeros(choices_total, dtype=int)
            total_testtakers = np.zeros(choices_total, dtype=int)

            # split in high and low scorers with 27% group size
            splitter = round(number_of_testtakers * 0.27)
            for i in range(number_of_testtakers):
                student_id =  points_of_testtakers[i][0]
                if choices_of_testtakers[student_id]:
                    student_used_choice = int(choices_of_testtakers[student_id])
                    if i < splitter:
                        low_scoring_testtakers[student_used_choice] += 1
                    elif i > number_of_testtakers-splitter:
                        high_scoring_testtakers[student_used_choice] += 1
                    total_testtakers[student_used_choice] += 1

            self.studentsAnswers = []
            for choice_number in range(0, choices_total):
                answers_per_choice = (self.id,  choice_number, total_testtakers[choice_number], low_scoring_testtakers[choice_number], high_scoring_testtakers[choice_number])
                self.studentsAnswers.append(answers_per_choice)

        # Checks if question is Cloze Choice
        elif self.getTypeID() == 3:
            self.studentsAnswers = mysqlc.query("SELECT ts.question_fi, ts.value1, truncate(ts.value2,2) as value_2, count(ts.value2) FROM tst_active ta join tst_solutions ts on ta.active_id = ts.active_fi where ta.test_fi = %s and ts.question_fi = %s group by ts.question_fi, ts.value1, truncate(ts.value2,2) order by ts.value1, value_2;", [self.testId, self.id])
        # Question is not Single Choice
        else:
            self.studentsAnswers = mysqlc.query("SELECT ts.question_fi, ts.value1, ts.value2, count(ts.value2) FROM tst_active ta join tst_solutions ts on ta.active_id = ts.active_fi where ta.test_fi = %s and ts.question_fi = %s group by ts.question_fi, ts.value1, ts.value2 order by ts.value1, ts.value2;", [self.testId, self.id])

    def latexPlotter(self, usetexSwitch):
        rc('text', usetex=usetexSwitch)
        params= {'text.latex.preamble' : [r'\usepackage{amsmath}', r'\everymath{\displaystyle}']}
        plt.rcParams.update(params)

    # prepare math expressions for latex compiler
    def latexSyntaxHandler(self, latexString):
        latex = True

        latexString = latexString.replace("[tex]", "$").replace("[/tex]", "$")

        while(latex):
            latex = False
            latexmarkerIndexStart = latexString.find("<span class=\"latex\">")
            latexmarkerIndexEnd = latexString.find("</span>", latexmarkerIndexStart)

            if(latexmarkerIndexStart >= 0):
                latex = True
                if(latexmarkerIndexEnd - latexmarkerIndexStart > 20):
                    expressionIndicator = "$"
                else:
                    expressionIndicator = ""

                latexPreparedTail = latexString[:latexmarkerIndexEnd] +expressionIndicator+ latexString[latexmarkerIndexEnd+7:] 
                latexString = latexPreparedTail.replace("<span class=\"latex\">", expressionIndicator,1)
            
        return latexString