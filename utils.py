import csv
import mysqlc

# Minimum of answered questions in percent
MIN_ANS_QUEST_IN_PER = 20


# Returns array with end result points and points for a given question for all students
def get_students_points_for_question(questionID, min_ans_questions_in_percent=MIN_ANS_QUEST_IN_PER):
    res = mysqlc.query(
        "select ta.user_fi, ttr.points, tpr.points from tst_active ta join tst_pass_result tpr on ta.active_id = tpr.active_fi join tst_test_result ttr on ta.active_id = ttr.active_fi where ttr.question_fi = %s and tpr.answeredquestions >= tpr.questioncount * %s / 100 order by ta.user_fi;",
        [questionID, min_ans_questions_in_percent])

    # Checks whether db result is empty
    if len(res) > 0:
        return res

    # Prints error for empty result
    else:
        print("No results can be found for question %s!" % questionID)
        exit()


def get_students_choices_for_question(question_id, test_id):
    res = mysqlc.query(
        "SELECT distinct ta.user_fi, ts.value1  FROM tst_active ta join tst_solutions ts on ta.active_id = ts.active_fi where ta.test_fi = %s and ts.question_fi = %s order by ts.value1;",
        [test_id, question_id]
    )

    return res


# Returns list with user IDs from users who commited a given test
def get_users_for_test(testID, min_ans_questions_in_percent=MIN_ANS_QUEST_IN_PER):
    res = mysqlc.query(
        "select distinct user_fi from tst_active ta join tst_pass_result tpr on ta.active_id = tpr.active_fi where ta.test_fi = %s and ta.submitted = 1 and tpr.answeredquestions >= tpr.questioncount * %s / 100 and tpr.points > 0",
        [testID, min_ans_questions_in_percent])
    # Checks whether db result is empty
    if len(res) > 0:
        id_list = []
        for line in res:
            id_list.append(line[0])
        # fetch excluded user ids from file
        try:
            with open('excluded_user_ids.txt') as f:
                excluded = list(map(int, f))
        except:
            excluded = []
            print("No file with excluded users found.")

        return list_diff(id_list, excluded)
    # Prints error for empty result
    else:
        print("No test executions found for this test! Is %s a valid testID? " % testID)
        exit()


# Returns list of question IDs from a specific test
def get_questions_for_test(testID):
    res = mysqlc.query("select question_fi from tst_test_question where test_fi = %s order by sequence", [testID])
    if len(res) > 0:
        qst_ids = []
        for question in res:
            qst_ids.append(question[0])
        return qst_ids
    else:
        print("No questions found for test with ID: " + str(testID))
        exit()


# Calculate difference between two given lists
def list_diff(first, second):
    return list(set(first) - set(second))


def getStudentQuestionPointsForTest(studentid, testid):
    res = mysqlc.query(
        """
        select ttr.question_fi, ttr.points 
        from tst_active ta 
        join tst_pass_result tpr on ta.active_id = tpr.active_fi 
        join tst_test_result ttr on ta.active_id = ttr.active_fi 
        where ta.user_fi = %s and ta.test_fi= %s and ta.submitted = 1
        order by ttr.question_fi;
        """, [studentid, testid])

    # Checks whether db result is empty
    if 0 < len(res) <= 60:
        return res

    # Prints error for empty result
    else:
        print("No or invalid results found for user %s!" % studentid)
        return None


# Format decimal symbol to european standard (, instead of .)
def localize_floats(row):
    return [
        str(el).replace('.', ',') if isinstance(el, float) else el
        for el in row
    ]


# Creates csv output from given header description and row list
def to_csv(header, row_list, filename):
    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerow(header)
        for row in row_list:
            writer.writerow(localize_floats(row))