# (wahrscheinlich) relevante sql-Abfragen für die Auswertungen 

use ilias;

###
#Das Ziel des folgenden Selects ist es, eine Ausgabe zu erzeugen, die möglich nah an der "User_Master.xslx" von Fr. Derr liegt.
#Die entgültig erreichten Prozente eines Users ermittelt die Software dabei, indem sie zuerst die Testdurchführung mit dem höchsten Status (in {2,3}) selektiert. 
#Falls der User mehrere Testdurchführungen mit maximalem Status erzielt hat, wird das Testergebnis des !ersten! Versuchs gewertet.

select usr_id, percentage, status_changed from ut_lp_marks 
WHERE status >= 2 AND YEAR(status_changed) = 2019 AND percentage > 0 
GROUP BY usr_id, percentage, status_changed HAVING max(status_changed) 
order by percentage, usr_id ASC ;

#Zu beachten: Dieser Select beachtet noch nicht die im Dokument "User_Master.xslx" aussortierten User. Weitherhin scheint das von Fr. Derr erzeugte Dokument mehr
#Ergebnisse zu beinhalten. Liegt evtl. daran, dass wir auf einem Testsystem arbeiten, welches inzwischen veraltet ist ...