# Studienarbeit ILIAS
## Verwendung
Auf Basis der ILIAS-Datenbank können mit der main.py verschiedene Item-Analyse-Kennwerte errechnet werden. Die verschiedenen Funktionaliäten werden hierfür durch entsprechende Kommandozeilen-Argumente gesteuert, welche im Folgenden beispielhaft erklärt werden.
Es ist zu berücksichtigen, dass Testdurchführungen nur in die Berechnungen mit einfließen, wenn der entsprechende Teilnehmer mindestens 20% aller Fragen beantwortet hat. Ist dies nicht der Fall, so wird die Durchführung als _unseriös bearbeitet_ betrachtet und außer Acht gelassen. Diese 20% können, wie im Beispiel 3 erläutert, durch den Parameter `-o` temporär überschrieben werden.

### Beispiele
#### Untersuchung ganzer Tests
1. Cronbach-Alpha für Test mit ID 1270
```
python main.py -c 1270
```
2. Volle Item Analyse für alle Fragen des Tests 1270
```
python main.py -a 1270
```
3. Wie 2, aber mindestens 15% beantwortete Fragen nötig (s. o.)
```
python main.py -a 1270 -o 15
```
#### Untersuchung einzelner Items (BETA)
1. Eigenschaften der Frage 91456 im Kontext des Testes 1270 auf Kommandozeile ausgeben
```
python main.py -v 91456 1270
```
2. Frage 91456 aus Test 1270 als Säulendiagramm exportieren
```
python main.py -p 91456 1270
```
3. Wie 2, aber mit Einsatz eines lokal installierten Latex-Compilers (Darstellung von Funktionen, Brüchen, etc.)
```
python main.py -lp 91456 1270
```
## Module
### Purge Factory
Wenn das Python-Skript der purgeFactory ausgeführt wird, löscht es die in den `*.purge` Dateien definierten Tabellen aus der ilias.sql.
Der Output dieses Vorgangs wird in der iliasPurged.sql gespeichert.

## Entwicklung
Zur Verwendung des mysqlc-Moduls müssen die folgenden Einstellungen inform von Umgebungsvariablen eingerichtet sein.

| Variable | Beschreibung |
| -------- | -------- |
| ILIAS_DB_HOST   | DB Server IP-Addr.  |
| ILIAS_DB_USER   | DB Username   |
| ILIAS_DB_PASSWD   | DB Password  |
| ILIAS_DB_SCHEMA   | Schemaname  |

Alternativ können die Konfigurationen auch in der `config.py` eingetragen werden.
 
