import os, glob
list = []

def main():

    file_old_script = "../ilias.sql"
    file_new_script = "iliasPurged.sql"
    script_dir = os.path.dirname(__file__)

    os.chdir(script_dir)
    for file in glob.glob("*.purge"):
        file_path_trash_tables = os.path.join(script_dir, file)
        
        with open(file_path_trash_tables) as l:
            for line in l:
                list.append(line.strip())
        l.close()

    copy = True
    file_path_old_script = os.path.join(script_dir, file_old_script)
    file_path_new_script = os.path.join(script_dir, file_new_script)
    with open(file_path_old_script) as f:
        with open(file_path_new_script, "w") as f1:
            for line in f:
                # set copy flag
                if copy:
                    copy = checkStartTrashTable(line)
                else:
                    if not line.strip():
                        copy = True
                # check copy flag and write to output file
                if copy:
                    f1.write(line)
    f.close()
    f1.close()

def checkStartTrashTable(line):

    tableName = line.split('`')
    if(len(tableName) == 1) or not "TABLE" in tableName[0].upper():
        return True

    return not tableName[1] in list

if __name__ == '__main__':
    main()
