from scipy import stats
import numpy as np

# Returns Pearson Coefficient for two given lists
def pearson(listX, listY):
    return stats.pearsonr(listX, listY)[0]

# Returns point biserial correlation for two given lists
def pointBiserialCorrelation(listX, listY):
    return stats.pointbiserialr(listX, listY)[0]

# Returns percentage of correct answers (difficulty index for cloze items) 
def rightAnswers(listX):
    return sum(listX)/len(listX)

# Returns difficulty index for single choice items
def difficultyOfSCItem(listX, choiceCount):
    N = len(listX)
    N_r = sum(listX)
    N_f = N - N_r
    return (N_r-(N_f/(choiceCount-1)))/N

# Returns corrected difficulty index for single choice items
def difficultyOfSCItemCorrected(listX, N_fmax):
    N = len(listX)
    N_r = sum(listX)
    return (N_r - N_fmax)/N

# Returns percentage of correct answers from students with all-in-all score over given value
def rightAnswersStudentsHigherThan(high, listX, listY):
    maximum = max(listY)
    sumPoints = 0
    count = 0
    # Iterates over all results 
    for i in range(0, len(listX)):
        # Checks if all-in-all score (listY) is higher than 80%
        if listY[i]/maximum > high:
            # Adds points for question to point counter and increase answer counter
            sumPoints+=listX[i]
            count+=1

    return sumPoints/count

# Returns percentage of correct answers from students with all-in-all score under 50%
def rightAnswersStudentsLowerThan(low, listX, listY):
    maximum = max(listY)
    sumPoints = 0
    count = 0
    # Iterates over all results 
    for i in range(0, len(listX)):
        # Checks if all-in-all score (listY) is lower than 50%
        if listY[i]/maximum < low:
            # Adds points for question to point counter and increase answer counter
            sumPoints+=listX[i]
            count+=1

    return sumPoints/count

def cronbachAlpha(itemscores):
    # itemscores = [[ 4,14,3,3,23,4,52,3,33,3], every list corresponds to an item
    #              [ 5,14,4,3,24,5,55,4,15,3]]  numbers are results per user (in our case: 0 or 1)

    itemscores = np.array(itemscores)
    itemvars = itemscores.var(axis=1, ddof=1)
    tscores = itemscores.sum(axis=0)
    nitems = len(itemscores)

    return nitems / (nitems-1.) * (1 - itemvars.sum() / tscores.var(ddof=1))
    
# Returns the item discrimination index
def itemDiscriminationIndex(testResults):
    testResults = np.asarray(testResults)

    # sort by total test score
    testResultsSorted = testResults[testResults[:,0].argsort()]
   
    # Use Truman Kelley's "27% of sample" group size
    testpersonCount = testResultsSorted.shape[0]
    splitter = round(testpersonCount * 0.27)
    lowScorers, remainder, highScorers = np.split(testResultsSorted, [splitter, testpersonCount-splitter])

    correctLowScorers = np.sum(lowScorers, axis=0)[1] # lowScorers correct answers
    correctHighScorers = np.sum(highScorers, axis=0)[1] # highScorers corret answers

    return (correctHighScorers - correctLowScorers) / lowScorers.shape[0]

#a = [[60,0],[50,0],[40,0]]
#print(itemDiscriminationIndex(a))

# Interprets right answers (%) as difficulty and returns a description string
def asDifficulty(P):
    if(P < 0.2):
        return "H"
    elif(P > 0.8):
        return "L"
    else:
        return "M"
