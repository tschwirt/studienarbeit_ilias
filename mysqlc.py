import os
import cymysql
import csv

# db connection settings
from config import *
DB_HOST = ILIAS_DB_HOST
DB_USER = ILIAS_DB_USER
DB_PASSWD = ILIAS_DB_PASSWD
DB_SCHEMA  = ILIAS_DB_SCHEMA

if "ILIAS_DB_HOST" in os.environ:
    DB_HOST = os.environ['ILIAS_DB_HOST']
if "ILIAS_DB_USER" in os.environ:
    DB_USER = os.environ['ILIAS_DB_USER']
if "ILIAS_DB_PASSWD" in os.environ:
    DB_PASSWD = os.environ['ILIAS_DB_PASSWD']
if "ILIAS_DB_SCHEMA" in os.environ:
    DB_SCHEMA = os.environ['ILIAS_DB_SCHEMA']

conn = cymysql.connect(DB_HOST, DB_USER, DB_PASSWD, DB_SCHEMA)
cur = conn.cursor()

# run sql query
def query(queryString, args = None, outputFile = None):
        if outputFile is None:
                if args is None:
                        cur.execute(queryString)
                else:
                        cur.execute(queryString, args)
                return cur.fetchall()                        
        else:
                res = query(queryString)
                with open(outputFile, "w") as f:
                        writer = csv.writer(f, quotechar = "'")
                        writer.writerows(res)

#mysqlc.query("select ud.firstname, ud.lastname, us.query  from usr_search as us JOIN usr_data as ud ON us.usr_id = ud.usr_id LIMIT 3", "test.csv")

# close db connection
def close():
        cur.close()
        conn.close()
