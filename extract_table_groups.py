import os
list = []
dictCount = {}
dictEmpty = {}

def main():

    script_dir = os.path.dirname(__file__)

    file_empty_tables = "emptyTables.txt"
    file_path_empty_tables = os.path.join(script_dir, file_empty_tables)

    with open(file_path_empty_tables) as l:
        for line in l:
            list.append(line.strip())
    l.close()

    file_table_names = "table_names_ilias.txt"
    file_path_table_names = os.path.join(script_dir, file_table_names)

    with open(file_path_table_names) as l:
        for line in l:
            abr = line.strip().split("_")[0]
            if abr in dictCount:
                dictCount[abr] = dictCount[abr] + 1

            else:
                dictCount[abr] = 1
                dictEmpty[abr] = 0

            if line.strip() in list:
                dictEmpty[abr] += 1

    l.close()

    print("Länge:", len(dictCount), "\n")

    for x in dictCount:
        print(x, dictCount[x])

    for x in dictEmpty:
        print(x, dictEmpty[x])

if __name__ == '__main__':
    main()
