import mysqlc

# Constants
INTRO_TEST_ID = 1270
CONTROL_TEST_ID = 1194 # oder 1256, 1292

class StudentTestResult:

    def __init__(self, id):
        self.id = id
        
        self.firstname, self.lastname = self.getStudent()
        self.introExecID = self.getExecID(INTRO_TEST_ID) # requests execution ID for given student for "Einstiegstest" 
        self.controlExecID = self.getExecID(CONTROL_TEST_ID) # requests execution ID for given student for "Einstiegstest" 
        
        if self.introExecID:
            self.intro_result = self.getEndResult(self.introExecID)
            #self.answers = self.getAnswers(self.introExecID) 
        if self.controlExecID:
            self.control_result = self.getEndResult(self.controlExecID)


    def print_results(self):
        print("Test results from %s %s (ID: %d)\n" %(self.firstname, self.lastname, self.id))

        #for answer in self.answers:
        #    if(answer['points'] == 0.0):
        #        print("- %s | Points: %s | Answer: %s | Right: %s" %(answer['questionTitle'], answer['points'], answer['studentAnswer'], answer['rightAnswer']))
        #    else:
        #        print("+ %s | Points: %s | Answer: %s | Right: %s" %(answer['questionTitle'], answer['points'], answer['studentAnswer'], answer['rightAnswer']))

        res = self.intro_result
        print("End result for %s %s<< Points: %s/%s, Percentage: %.2f, Answered questions: %s/%s, Time: %.0f min >>\n" 
            %(self.firstname, self.lastname, res['points'], res['maxPoints'], res['points']/res['maxPoints'], res['questionsAnswered'], res['questionCount'], res['workingTime']/60))
        res = self.control_result
        print("End result for %s %s<< Points: %s/%s, Percentage: %.2f, Answered questions: %s/%s, Time: %.0f min >>\n" 
            %(self.firstname, self.lastname, res['points'], res['maxPoints'], res['points']/res['maxPoints'], res['questionsAnswered'], res['questionCount'], res['workingTime']/60))

    # Creates student object and initializes it with students data
    def getStudent(self):
        res = mysqlc.query("select firstname, lastname from usr_data where usr_id = %s", [self.id])

        # Checks whether db result is empty
        if(len(res) > 0):
            return [res[0][0], res[0][1]]
        
        # Prints error for empty result
        else:
            print("No student can be found with ID %s!" % self.id)
            return [None,None]
    
     # Returns all correct answers to given question
    def getRightAnswer(self, questionID):
        res = mysqlc.query("select question_type_fi, title from qpl_questions where question_id = %s" % questionID)
        
        # Checks whether db result is empty
        if(len(res) > 0):
            # Requests answer for single choice questions
            if(res[0][0] == 1):
                answer = mysqlc.query("select aorder from qpl_a_sc where question_fi = %s and points > 0" % questionID)
            # Requests answers for cloze questions
            elif(res[0][0] == 3):
                answer = mysqlc.query("select answertext, aorder, points from qpl_a_cloze where question_fi = %s" % questionID)
            # Prints error if question is neither type cloze nor type single choice
            else:
                print("Question has unexpected answer type!")

            # If only one line is returned from db return question title and correct answer
            if(len(answer) == 1):
                return [res[0][1] + " (" + str(questionID) + ")", answer[0][0]]
            # If question has multiple correct answers concatenates them and returns it afterwards 
            elif(len(answer) > 1):
                conAnswer = ""
                # Iterates over all answer options
                for part in answer:
                    # Checks if answer is correct
                    if part[2] > 0:
                        # Checks if concatenated answer is empty, if not adds a comma
                        if conAnswer:
                            conAnswer += ","
                        # Adds stringyfied correct answer option to concatenated answer string
                        conAnswer += str(part[1])
                # Return question title and string with all correct answers
                return [res[0][1] + " (" + str(questionID) + ")", conAnswer]
                
            # Prints error for empty result
            else:
                print("Right answer for question with ID %s cannot be found!" % questionID)
                return
        # Prints error for empty result
        else:
            print("Question with ID %s cannot be found in question pool!" % questionID)
            return

    # Collects given and correct answers from db
    def getAnswers(self, execID):
        # Selects all questions from current test execution and orders it by processing sequence
        res = mysqlc.query("select question_fi, points from tst_test_result where active_fi = %s order by question_fi" % execID)
        
        # Declarate empty array for answers for the questions
        answers = []

        # Checks whether db result is empty
        if(len(res) > 0):
            for question in res:
                points = question[1]
                # Fetches question title and right answer and splits it afterwards
                qAndA = self.getRightAnswer(question[0])
                questionTitle = qAndA[0]
                rightAnswer = qAndA[1]
                # Fetches given answer from student
                studentAnswer = self.getStudentsAnswer(question[0], execID)
                # Save title, points and answers in associative array and adds it to answer array
                answers.append({'questionTitle': questionTitle, 'points': points, 'studentAnswer': studentAnswer, 'rightAnswer':rightAnswer})
            
            return answers

        # Prints error for empty result
        else:
            print("No questions can be found for test execution %s!" % execID)
            return

    # Returns all answers from student for a given question during a given execution
    def getStudentsAnswer(self, questionID, execID):
        # Requests students answers ordered by value1 for questions where multiple answers are required
        res = mysqlc.query("select value1, value2 from tst_solutions where question_fi = %s and active_fi = %s order by value1" % (questionID, execID))
        
        # Checks if db query returns one row 
        if(len(res) == 1):
            # If value2 is not given (single choice question) returns value1 otherwise returns value2 (cloze question)
            if(res[0][1] is None):
                return res[0][0]
            else:
                return res[0][1]

        # If a question needs multiple given answers value1 contains gap number and value2 the students answer
        elif(len(res) > 1):
            # Iterates over and concatenates multiple students answers, right order ensured through order by value1 in db query
            conAnswer = res[0][1]
            for i in range(1,len(res)):
                conAnswer += "," + str(res[i][1])
            return conAnswer

        # Prints error for empty result
        else:
            print("Students answer to question with ID %s cannot be found!" % questionID)
            return

    # Returns execution ID for a given student and a given test type
    def getExecID(self, testID):
        res = mysqlc.query("select active_id from tst_active where test_fi = %s and user_fi = %s" % (testID, self.id))

        # Checks whether db result is empty
        if(len(res) > 0):
            return res[0][0]

        # Prints error for empty result
        else:
            print("No test execution can be found for test %s and student %s!" % (testID, self.id))
            return

    # Return associative array with end result data for a given test execution 
    def getEndResult(self, execID):
        res = mysqlc.query("select points, maxpoints, questioncount, answeredquestions, workingtime from tst_pass_result where active_fi = %s" % execID)

        # Checks whether db result is empty and creates associative array with test result data
        if(len(res) > 0):
            return {'points': res[0][0], 'maxPoints': res[0][1], 'questionsAnswered': res[0][3], 'questionCount': res[0][2], 'workingTime': res[0][4]}

        # Prints error for empty result
        else:
            print("No test results can be found for test execution %s!" % execID)
            return