from StudentTestResult import StudentTestResult
import mysqlc
import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.metrics import f1_score, accuracy_score, r2_score
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import SMOTE

def calculateTestVariation(studentIDs):
    for studentID in studentIDs:
        studentResults = StudentTestResult(studentID)
        studentResults.print_results()

        firstName = studentResults.firstname
        lastname = studentResults.lastname

        try:
            punkteET = studentResults.intro_result['points'] / \
                studentResults.intro_result['maxPoints']
            punkteKT = studentResults.control_result['points'] / \
                studentResults.control_result['maxPoints']
            
            if punkteET <= punkteKT:
                print("%s %s (%s):   %.2f(ET) -> %.2f(KT)    +Verbesserung: %.2f\n" %
                    (firstName, lastname, studentID, punkteET, punkteKT, punkteKT-punkteET))
            else:
                print("%s %s (%s):   %.2f(ET) -> %.2f(KT)    -Verschlechterung: %.2f\n" %
                    (firstName, lastname, studentID, punkteET, punkteKT, punkteKT-punkteET))

            #iliasActivity = getIliasActivity(studentID)
            print("\n\n\n")

        except AttributeError:
            continue
        


def getIliasActivity(studentID):
    testResultsOverview = mysqlc.query("select od.type, od.title, ta.user_fi, tpr.pass, tpr.points, tpr.maxpoints, tpr.workingtime, tpr.tstamp from ilias.object_data od join tst_tests tt on od.obj_id = tt.obj_fi join tst_active ta on ta.test_fi = tt.test_id join tst_pass_result tpr on tpr.active_fi = ta.active_id where ta.user_fi = %s", [studentID])
    sumTime = mysqlc.query("select sum(tpr.workingtime) from ilias.object_data od join tst_tests tt on od.obj_id = tt.obj_fi join tst_active ta on ta.test_fi = tt.test_id join tst_pass_result tpr on tpr.active_fi = ta.active_id join usr_data as ud on ud.usr_id = ta.user_fi where user_fi = %s and tt.test_id <> %s and tt.test_id <> %s group by ta.user_fi", [studentID, 1270, 1194])
    bestTestResults = mysqlc.query("select od.title, max(tpr.points/tpr.maxpoints) from ilias.object_data od join tst_tests tt on od.obj_id = tt.obj_fi join tst_active ta on ta.test_fi = tt.test_id join tst_pass_result tpr on tpr.active_fi = ta.active_id join usr_data as ud on ud.usr_id = ta.user_fi where ta.user_fi = %s and ta.submitted = 1 and tt.test_id <> %s and tt.test_id <> %s group by tt.test_id", [studentID, 1270, 1194])
    numTests = len(bestTestResults)
    if numTests > 0:
        avgBestResult = sum([x[1] for x in bestTestResults])/numTests
    else:
        avgBestResult = 0

    # print(testResultsOverview)
    if len(sumTime) > 0:
        print("Investierte Zeit: %.2fh" % (sumTime[0][0]/60/60))
    else:
        print("Investierte Zeit: -")
    print("Abgeschlossene Trainings", bestTestResults)
    print("Durchschnitt beste Testergebnisse", avgBestResult)
    # Tabellengruppen skl, tst

def regressionApproach():
    #read data from csv
    df = pd.read_csv("regressionData.csv")
    print(df.shape)

    #define data and split it to X and Y values
    #feature columns without ET Points
    #feature_columns1 = ['trainingTestTime', 'numberOfTests', 'averageTestResult', 'coursePartizipation', 'forumPosts', 'forumReads', 'numberOfModuls', 'registerTime', 'surveyPartizipation', 'readEvents']
    #feature columns with ET Points
    feature_columns1 = ['trainingTestTime', 'numberOfTests', 'averageTestResult', 'coursePartizipation', 'forumPosts', 'forumReads', 'numberOfModuls', 'registerTime', 'surveyPartizipation', 'readEvents', 'punkteET']
    feature_columns2 = ['trainingTestTime', 'numberOfTests', 'averageTestResult', 'coursePartizipation', 'forumPosts', 'forumReads', 'numberOfModuls', 'registerTime', 'surveyPartizipation', 'readEvents', 'punkteET']
    X1 = df[feature_columns1]
    X2 = df[feature_columns2]
    Y1 = df[['improvement']].values.ravel()
    Y2 = df[['punkteKT']].values.ravel()

    #split data in train and test data
    X1_train, X1_test, y1_train, y1_test = train_test_split(X1, Y1, test_size=0.2, random_state=21)
    X2_train, X2_test, y2_train, y2_test = train_test_split(X2, Y2, test_size=0.2, random_state=21)
    print(X1_test, X2_test)

    #create models, fit models and make predictions
    logisticRegressor1 = LogisticRegression(tol=0.0001, solver='lbfgs', max_iter=1000)
    logisticRegressor1.fit(X1_train, y1_train)
    Y1_pred = logisticRegressor1.predict(X1_test)
    #print(Y1_pred)
    #for student in range(len(Y1_pred)):
    #    print("Student %s:: predicted Improvement: %s | real improvement: %s" %(X1_test.index.values[student], Y1_pred[student], y1_test[student]))
    #    print(X1_test.iloc[student,:])
    #return

    linear_regressor2 = LinearRegression()
    linear_regressor2.fit(X2_train, y2_train)
    Y2_pred = linear_regressor2.predict(X2_test)

    count = 0

    #print predictions and real values
    for row in range(0, len(y1_test)):
        print("Student %s:: ET-Points: %.2f | predicted KT-Points: %.2f | real KT-Points: %.2f || predicted Improvement: %s | real improvement: %s" %(X2_test.index.values[row], X2_test[['punkteET']].iloc[row,0], Y2_pred[row], y2_test[row], Y1_pred[row], y1_test[row]))
        if Y1_pred[row] != y1_test[row]:
            print(X2_test.iloc[row,:])
            count+=1

    #display coefficients
    coefficients1 = logisticRegressor1.coef_[0]
    coefficients2 = linear_regressor2.coef_
    
    print("\n\nCoeffiecients for LR KT-Improvement:")
    for coefIndex in range(0, len(coefficients1)):
        print("%s:: LR improvement: %s" % (feature_columns1[coefIndex], coefficients1[coefIndex]))
    
    print("\n\nCoeffiecients for LR KT-Points:")
    for coefIndex in range(0, len(coefficients2)):
        print("%s:: LR points: %s" % (feature_columns2[coefIndex], coefficients2[coefIndex]))
    
    #print models scores
    print("\n\nTest F1-Score LR improvement: %s" % (f1_score(y1_test, Y1_pred)))
    print("Test Accuracy LR improvement: %s" % (accuracy_score(y1_test, Y1_pred, normalize=True)))
    print("Manual Test Accuracy LR improvement: %s" % (1-count/len(y2_test)))
    print("Train R2_Score:: LR points: %s | LR improvement: %s" % (linear_regressor2.score(X2_train, y2_train), logisticRegressor1.score(X1_train, y1_train)))
    print("Test R2_Score:: LR points: %s | LR improvement: %s" % (linear_regressor2.score(X2_test, y2_test), logisticRegressor1.score(X1_test, y1_test)))

def collectDataToCSV(studentIDs):
    #data lists for students data both with data for whole test and for single modul analysis
    whole_test_analysis = []
    training_moduls_analysis = []

    #definitions of moduls names, creating dictionaries for lists of question-IDs belonging to each category and maximum of reachable points in each category for control test and intro test
    categories = ['Arithmetik', 'Gleichungen', 'Potenzen, Wurzeln, Logarithmen', 'Funktionen', 'Geometrie', 'Trigonometrie', 'Lineare Algebra', 'Folgen, Grenzwerte, Stetigkeit', 'Differential-, Integralrechnung', 'Logik']
    categories_questions_ET = dict()
    categories_questions_KT = dict()
    cat_maxpoints_ET = dict()
    cat_maxpoints_KT = dict()
    #definition of object-ID of modul and final test for each category
    cat_course_test_ids = { 'Arithmetik':[39034,1198], 'Gleichungen':[39207,1208], 'Potenzen, Wurzeln, Logarithmen':[39393,1213], 'Funktionen':[39502,1218], 'Geometrie':[39838,1224], 'Trigonometrie':[40142,1228], 'Lineare Algebra':[44168,1294], 'Folgen, Grenzwerte, Stetigkeit':[44379,1308], 'Differential-, Integralrechnung':[44609,1306], 'Logik':[43465,None] }

    #for each category getting all IDs of associated questions
    for cat in categories:
        #for intro test
        cat_qst_ET = mysqlc.query("select question_id from qpl_questions where obj_fi = %s and (title like %s or title like %s)", (43403, '%{}'.format(cat), '%{} '.format(cat)))
        categories_questions_ET[cat] = cat_qst_ET
        #each question in test has a maximum of one point to reach
        cat_maxpoints_ET[cat] = len(cat_qst_ET)
        #for control test
        cat_qst_KT = mysqlc.query("select question_id from qpl_questions where obj_fi = %s and (title like %s or title like %s)", (37318, '%{}'.format(cat), '%{} '.format(cat)))
        categories_questions_KT[cat] = cat_qst_KT
        #each question in test has a maximum of one point to reach
        cat_maxpoints_KT[cat] = len(cat_qst_KT)

    #get data for each student from studentID list
    for studentID in studentIDs:
        print(studentID)
        studentResults = StudentTestResult(studentID)

        #if error occurs by querying database for a user continue with next user
        try:
            #extract data from StudentTestResult object
            firstName = studentResults.firstname
            lastname = studentResults.lastname
            punkteET = studentResults.intro_result['points'] / studentResults.intro_result['maxPoints']
            punkteKT = studentResults.control_result['points'] / studentResults.control_result['maxPoints']
            if punkteKT > punkteET:
                improvement = 1
            else:
                improvement = 0

            #queries the whole time a user spends on ilias platform 
            online_time = mysqlc.query("SELECT sum(spent_seconds) FROM read_event re join object_data od on od.obj_id = re.obj_id where usr_id = %s group by usr_id", studentID)
            if len(online_time) > 0:
                online_time = online_time[0][0]
            else:
                online_time = 0

            #queries number of guided courses where a user took part 
            courses = mysqlc.query(
                "select od.title from obj_members om join object_data od on om.obj_id = od.obj_id where od.type = %s and om.usr_id = %s", ["grp", studentID])
            if len(courses) > 0:
                coursePartizipation = 1
            else:
                coursePartizipation = 0

            # Achtung: nachfolgende Abfrage kann die Performanz stark beeinträchtigen, da hier über Strings gejoint werden muss!
            #queries the number of forum activities of a user
            forumPosts = len(mysqlc.query(
                "select fp.pos_pk from frm_posts fp join usr_data ud on ud.login = fp.pos_usr_alias where ud.usr_id = %s", studentID))
            forumReads = len(mysqlc.query(
                "select fr.post_id from frm_user_read fr join usr_data ud on ud.usr_id = fr.usr_id where fr.usr_id = %s", studentID))
            
            #queries all moduls a user registrated for
            moduls = mysqlc.query("select od.title, od.description, od.type, od.obj_id, om.passed, om.origin, om.origin_ts from obj_members om join usr_data ud on om.usr_id = ud.usr_id join object_data od on om.obj_id = od.obj_id where od.type=%s and od.obj_id <> %s and od.obj_id <> %s and ud.usr_id = %s", [
                                "crs", 43985, 35171, studentID])
            numOfModuls = len(moduls)

            #queries the data when a user registrated on ilias and calculates a time span from unix start time
            registerDate = mysqlc.query(
                "select create_date from usr_data where usr_id = %s", studentID)[0][0]
            registerTime = (
                registerDate - datetime.datetime.utcfromtimestamp(0)).total_seconds()

            #queries wheather a user has submitted a survey
            #nicht 100% validierbar, ob sinnvoll und korrekt
            surveyFinished = mysqlc.query(
                "select finished_id from svy_finished where survey_fi <> %s and user_fi = %s", [112, studentID])
            if len(surveyFinished) > 0:
                surveyPartizipation = 1
            else:
                surveyPartizipation = 0

            #results for ET/KT depending on categories
            #initializes dictionaries for points in categories and training activities (time, final test result) for each category
            points_cat_ET = dict()
            points_cat_KT = dict()
            course_time_cat = dict()
            final_test_cat = dict()
            
            #iterates over each category and query data
            for cat in categories:
                #extract list of question-IDs for both tests for the current category
                cat_qst_ids_ET = categories_questions_ET[cat]
                cat_qst_ids_KT = categories_questions_KT[cat]
                #queries the received points in the current category
                points_cat_ET[cat] = mysqlc.query("SELECT sum(points) FROM tst_test_result ttr join tst_active ta on ta.active_id = ttr.active_fi where user_fi = %s and ttr.question_fi in %s group by ttr.active_fi", [studentID, cat_qst_ids_ET])[0][0]
                points_cat_KT[cat] = mysqlc.query("SELECT sum(points) FROM tst_test_result ttr join tst_active ta on ta.active_id = ttr.active_fi where user_fi = %s and ttr.question_fi in %s group by ttr.active_fi", [studentID, cat_qst_ids_KT])[0][0]
                
                #extracts course object-ID and test-ID
                cat_course_id = cat_course_test_ids[cat][0]
                cat_final_test_id = cat_course_test_ids[cat][1]
                #queries the training time in the current category and the final test result for the current category
                course_time = mysqlc.query("select spent_seconds+childs_spent_seconds from read_event where obj_id = %s and usr_id = %s", [cat_course_id, studentID])
                course_final_test_result = mysqlc.query("SELECT max(points)/maxpoints FROM tst_pass_result tpr join tst_active ta on tpr.active_fi = ta.active_id where ta.user_fi = %s and ta.test_fi = %s group by ta.user_fi, tpr.maxpoints", [studentID, cat_final_test_id])
                
                if len(course_time)>0: 
                    course_time_cat[cat] = course_time[0][0]
                else:
                    course_time_cat[cat] = 0

                if len(course_final_test_result)>0: 
                    final_test_cat[cat] = course_final_test_result[0][0]
                else:
                    final_test_cat[cat] = 0

            #creates a dictionary for all modul data for the current student
            student_test_analysis = dict()
            #iterates over each category and adds data to dictionary
            for cat in categories:
                student_test_analysis['studentID'] = studentID
                student_test_analysis['ET_'+cat] = points_cat_ET[cat]/cat_maxpoints_ET[cat]
                student_test_analysis['KT_'+cat] = points_cat_KT[cat]/cat_maxpoints_KT[cat]
                student_test_analysis['Time_'+cat] = course_time_cat[cat]
                student_test_analysis['Test-Score_'+cat] = final_test_cat[cat]

            #adds student modul data to modul analysis data list
            training_moduls_analysis.append(student_test_analysis)

            #adds student test and uncategorized training data to test analysis list
            whole_test_analysis.append({"studentID": studentID,  "onlineTime": online_time, "coursePartizipation": coursePartizipation, "forumPosts": forumPosts, "forumReads": forumReads, "numberOfModuls": numOfModuls, "registerTime": registerTime,
                    "surveyPartizipation": surveyPartizipation, "punkteET": punkteET, "punkteKT": punkteKT, "improvement": improvement})

            #print("studentID", studentID,  "onlineTime", online_time, "numberOfTests", "coursePartizipation", coursePartizipation, "forumPosts", forumPosts, "forumReads", forumReads, "numberOfModuls", numOfModuls, "registerTime", registerTime,
            #    "surveyPartizipation", surveyPartizipation, "punkteET", punkteET, "punkteKT", punkteKT, "improvement", improvement)
            #print(student_test_analysis)
        
        #if error in query occurs continue with next student
        except:
            continue
    
    #transforms test data to a pandas data frame
    df = pd.DataFrame(whole_test_analysis)
    print(df.shape)
    print(df)
    #extract data which should not be normalized
    studentIDColumn = df[['studentID']]
    punkteETColumn = df[['punkteET']]
    punkteKTColumn = df[['punkteKT']]

    #normalizes to values between 0 and 1 for each column, for a better comparability and analyzation of influence of each feature
    df1 = df.apply(lambda x: (x - x.min()) / (x.max() - x.min())).fillna(0)
    #overwrites normalized data with unnormalized data for chosen columns 
    df1[['studentID']] = studentIDColumn
    df1[['punkteET']] = punkteETColumn
    df1[['punkteKT']] = punkteKTColumn
    print(df1.shape)
    print(df1)

    #convert test analysis dataframe to csv file
    df1.to_csv('wholeTestAnalysis.csv', index=False)

    #transforms modul analysis data to pandas dataframe and further to csv file
    df_training_moduls = pd.DataFrame(training_moduls_analysis)
    df_training_moduls.to_csv('trainingModulsAnalysis.csv', index=False)