# DHBW-PA-Preset

A LaTeX preset for the "Praxisarbeit" at the Duale Hochschule Baden-Württemberg Mannheim, with my interpretation of the guidelines of the Technical Committee, 2016.

## Compiling

```sh
$ latexmk PA.tex
```

with XeLaTeX (already specified in the `.latexmkrc`). Target document is `PA.pdf`.

## Content

### Settings

You will have to fill in title, author, time range you worked on it, "Matrikelnummer" and course you're attending (comma in between), date you are turning it in, location, employer, mentor, and DHBW reviewer if applicable (in this case you must uncomment the line in `hilfe/Deckblatt.tex`); in `hilfe/Einstellungen.tex`.

### Restriction

If your thesis must contain a "Sperrvermerk", you have to uncomment the line in `PA.tex`.

### Registration

Save your registration (the sheet you turned in where you stated what your Praxisarbeit would contain) as `pdfs/anmeldung.pdf`.

### Abbrevations, bibliography, and glossary

Examples provided (please remove before turning in) in `inhalt/Abkuerzungen.tex`, `PA.bib`, and `inhalt/Glossar.tex`, respectively.

### Abstract

Abstract is `inhalt/Abstract.tex`.